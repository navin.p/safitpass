//
//  UINavigation+NavBar.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 5/19/17.
//  Copyright © 2017 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface UINavigationController (NavBar)

- (void)configureBar;

@end
