//
//  UIViewController+NavigationBack.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/25/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (NavigationBack)

- (void) setCustomBackButton;
- (void) back;

@end
