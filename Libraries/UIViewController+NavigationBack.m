//
//  UIViewController+NavigationBack.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/25/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import "UIViewController+NavigationBack.h"

@implementation UIViewController (NavigationBack)

- (void) setCustomBackButton
{
    UIImage *buttonImage = [UIImage imageNamed:@"white_arrow_left"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;
}

- (void) back
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
