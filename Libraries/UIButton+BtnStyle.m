//
//  UIButton+BtnStyle.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/10/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import "UIButton+BtnStyle.h"

@implementation UIButton (BtnStyle)

- (void)configureBtn:(UIButton*)button
{
    
    self.layer.cornerRadius = 5;
    self.layer.borderColor = [UIColor whiteColor].CGColor;
    self.layer.backgroundColor = [UIColor colorWithRed:229/255.0 green:92/255.0 blue:51/255.0 alpha:1].CGColor;
}


@end
