//
//  UINavigation+NavBar.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 5/19/17.
//  Copyright © 2017 Ping-jung Tsai. All rights reserved.
//

#import "UINavigation+NavBar.h"


@implementation UINavigationController (NavBar)

- (void)configureBar
{
    
    //add logo to navigation bar
    UIImageView *image=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,105,36)] ;
    //set your image logo replace to the main-logo
    [image setImage:[UIImage imageNamed:@"fp-newlogo"]];
    [self.navigationController.navigationBar.topItem setTitleView:image];
    
      // Updated by Navin
//    UIColor *navColor =  [UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:1.0];
//    self.navigationController.navigationBar.barTintColor = navColor;
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
