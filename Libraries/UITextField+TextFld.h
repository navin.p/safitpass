//
//  UITextField+TextFld.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/10/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (TextFld)

- (void)configureTextFieldWithFrame:(CGRect)frame;

@end
