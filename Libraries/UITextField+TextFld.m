//
//  UITextField+TextFld.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/10/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import "UITextField+TextFld.h"

@implementation UITextField (TextFld)

- (void)configureTextFieldWithFrame:(CGRect)frame
{
    self.frame = frame;
    self.backgroundColor= [UIColor clearColor];
    self.layer.cornerRadius = 5.0f;
    [self.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    [self.layer setBorderWidth: 1.0f];
     self.clipsToBounds = YES;
}
- (CGRect)textRectForBounds:(CGRect)bounds
{
    return CGRectInset(bounds, 10.0f, 0);
}

- (CGRect)editingRectForBounds:(CGRect)bounds
{
    return [self textRectForBounds:bounds];
}

@end

