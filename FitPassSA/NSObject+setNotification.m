//
//  NSObject+setNotification.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/31/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import "NSObject+setNotification.h"
#import "Event.h"
#import <EventKit/EventKit.h>
#import <EventKit/EKReminder.h>
#import "AFNetworking.h"
#import "Header.h"
@implementation NSObject (setNotification)



- (NSString *)getNotified:(Event*)eventDetail userID:(NSString *)uID{
    
    NSString *postSuccess;
    
    EKEventStore *store = [[EKEventStore alloc] init];
    [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        if (!granted) { return; }
        EKEvent *event = [EKEvent eventWithEventStore:store];
        event.title = eventDetail.eventTitle;
        event.location = eventDetail.address;
        event.notes = eventDetail.eventDescription;
        
        
        //are we in daylight saving time?
        //convert date
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        NSLocale *enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [dateFormatter setLocale:enUSPOSIXLocale];
        NSTimeZone *tz=[NSTimeZone timeZoneWithAbbreviation:@"CST"];
        
        NSString *startDateTime = [NSString stringWithFormat:@"%@ %@ CST",eventDetail.startDate, eventDetail.startTime];
        NSString *endDateTime= [NSString stringWithFormat:@"%@ %@ CST",eventDetail.startDate, eventDetail.endTime];
        
        //setting yourlocal time
        [dateFormatter setTimeZone:tz];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss vvv"];
        NSDate *dateFromString = [dateFormatter dateFromString:startDateTime];
        
        // NSLog(@"%@", timeZone);
        event.startDate = dateFromString; //today
        NSDate *dateFromString2 = [[NSDate alloc] init];
        dateFromString2 = [dateFormatter dateFromString:endDateTime];
        event.endDate = dateFromString2;  //set 1 hour meeting
        
        //daylight saving adjust
        if ([tz isDaylightSavingTimeForDate:event.startDate]) {
            event.startDate = [event.startDate dateByAddingTimeInterval:-[tz daylightSavingTimeOffsetForDate: event.startDate]];
        }
        if ([tz isDaylightSavingTimeForDate:event.endDate]) {
            event.endDate = [event.endDate dateByAddingTimeInterval:-[tz daylightSavingTimeOffsetForDate:event.endDate]];
        }

        //create a date variable
        if ([eventDetail.startTime isEqualToString: eventDetail.endTime]){
            event.allDay = YES;
            
            //set reminder
            NSTimeInterval interval = -(60 * 60 * 15);
            EKAlarm *alarm = [EKAlarm alarmWithRelativeOffset:interval]; //Create object of alarm
            [event addAlarm:alarm]; //Add alarm to your event
        }else{
            
            //set reminder
            NSTimeInterval interval = -(60 * 60);
            EKAlarm *alarm = [EKAlarm alarmWithRelativeOffset:interval]; //Create object of alarm
            [event addAlarm:alarm]; //Add alarm to your event
        }
        
        NSLog(@"%@", event.startDate);
        
        [event setCalendar:[store defaultCalendarForNewEvents]];
        NSError *err = nil;
        [store saveEvent:event span:EKSpanThisEvent commit:YES error:&err];
        NSString *savedEventId = event.eventIdentifier;  //this is so you can access this event later
        
        //STORE INTO DATABASE
        [self saveNotification:eventDetail.eventID userID:uID icalID: savedEventId];
    }];
    postSuccess = @"Added";
    return postSuccess;
}


- (void)saveNotification:(NSString *)eventID userID:(NSString *)uID icalID:(NSString *)iCalEvent{
    
    //compile the URL
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *params =  [[NSDictionary alloc] initWithObjectsAndKeys:
                             eventID, @"eid",
                             uID, @"uid",
                             @"Add", @"func",
                             iCalEvent, @"ical",
                             nil] ;
    //PROCESS IT
    
    
    [manager POST:[NSString stringWithFormat:@"%@saveNotification.php",BaseURL] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //is this successful or is it a duplicate entries
        NSString *returnvalue = [NSString stringWithFormat:@"%@",[responseObject objectAtIndex:0]];
        if (![returnvalue isEqualToString:@"Added"]){
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Warning" message:@"This event notification can not be added at this time. Please try again." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
        }
        return;
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error.localizedDescription);
    }];
}


- (NSString *)removeNotified:(Event*)eventDetail userID:(NSString *)uID icalID:(NSString *)iCalEvent{
    
    
    NSString *postSuccess;
    
    EKEventStore* store = [EKEventStore new];
    [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        if (!granted) { return; }
        EKEvent* eventToRemove = [store eventWithIdentifier:iCalEvent];
        if (eventToRemove) {
            NSError* error = nil;
            [store removeEvent:eventToRemove span:EKSpanThisEvent commit:YES error:&error];
        }
        [self removeNotification:eventDetail.eventID userID:uID];
        
    }];
    postSuccess = @"Removed";
    return postSuccess;

}
- (void)removeNotification:(NSString *)eventID userID:(NSString *)uID{
    
    //compile the URL
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    // NSURL *url = [NSURL URLWithString:@"http://www.safitpass.com.php53-22.ord1-1.websitetestlink.com/json/signup.php"];
     NSDictionary *params =  [[NSDictionary alloc] initWithObjectsAndKeys:
                             eventID, @"eid",
                             uID, @"uid",
                             @"Remove", @"func",
                             nil] ;
    
    //PROCESS IT
    [manager POST:[NSString stringWithFormat:@"%@saveNotification.php",BaseURL] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //is this successful or is it a duplicate entries
        NSString *returnvalue = [NSString stringWithFormat:@"%@",[responseObject objectAtIndex:0]];
        if (![returnvalue isEqualToString:@"Removed"]){
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Warning" message:@"This event notification can not be removed at this time. Please try again." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
        }
        
        return;
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error.localizedDescription);
    }];
}


@end
