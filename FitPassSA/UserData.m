//
//  UserData.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 5/6/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import "UserData.h"
#import <UIKit/UIKit.h>
#import "SignUp.h"
#import <QuartzCore/QuartzCore.h>
#import "UITextField+TextFld.h"
#import "UIButton+BtnStyle.h"
#import "DropDownListView.h"
#import "MBProgressHUD.h"
#import "CommonCrypto.h"
#import "AFNetworking.h"
#import "URBAlertView.h"
#import "AppSetting.h"
#import "SWRevealViewController.h"
#import "EventList.h"
#import "Login.h"
#import "GAIDictionaryBuilder.h"
#import "GAI.h"
#import "GAIFields.h"
#import "Header.h"

#define kOFFSET_FOR_KEYBOARD 80.0

@interface UserData (){
    
    NSArray *genderArray;
    NSArray *booleanArray;
    NSArray *activityArray;
    NSUserDefaults *userDefaults;
}

@end

@implementation UserData

@synthesize scrollView;
@synthesize contentView;
@synthesize lblSelectedNames;
@synthesize firstName;
@synthesize lastName;
@synthesize emailAddress;
@synthesize password;
@synthesize age;
@synthesize textOK;
@synthesize fitLevel;
@synthesize gender;
@synthesize zipCode;
@synthesize weight;
@synthesize teamName;

-(void)viewDidAppear:(BOOL)animated
{
    //GA Log
    NSString *str = @"User Profile";
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:str];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    tracker.allowIDFACollection = YES;
    
    
    //add logo to navigation bar
   // UIImageView *image=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,105,36)] ;
    //set your image logo replace to the main-logo
    //[image setImage:[UIImage imageNamed:@"fp-newlogo"]];
    //[self.navigationController.navigationBar.topItem setTitleView:image];
    
    self.navigationController.topViewController.title = @"Update Profile";
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"< Back" style: UIBarButtonItemStylePlain target:self action:@selector(BackClicked)];
    self.navigationItem.leftBarButtonItem = backButton;
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor whiteColor];
    
    // Updated by Navin
//    UIColor *navColor =  [UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:1.0];
//    self.navigationController.navigationBar.barTintColor = navColor;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     
    //** attach scroll view     self.scrollView.delegate = self;
    [self.scrollView setScrollEnabled:YES];
    self.scrollView.frame = CGRectMake(0, 80, self.view.frame.size.width, self.scrollView.frame.size.height);
    [self.scrollView setContentSize:CGSizeMake(self.view.frame.size.width,2300)];
    self.contentView.frame = CGRectMake(0, 80, self.view.frame.size.width, self.contentView.frame.size.height);
    
    
    userDefaults = [[NSUserDefaults alloc] init];
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    //self.firstName.frame=CGRectMake(0, 0, 400, 44);
    //NSString *userID = [userDefaults objectForKey:kUserID];
    
    [self formatFields];
    //[self createMenu];
    
    //set picker options
    genderArray = @[@"Male", @"Female"];
    booleanArray = @[@"Yes", @"No"];
    activityArray = @[@"Light", @"Moderate", @"Advanced"];
    
    self.gender.delegate = self;
    self.textOK.delegate = self;
    self.fitLevel.delegate = self;
    
    //show progress bar
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self loadData];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)formatFields{
    [self roundUIiew:self.nameBox];
    [self roundUIiew:self.lnameBox];
    [self roundUIiew:self.emailBox];
    [self roundUIiew:self.pwBox];
    [self roundUIiew:self.ageBox];
    [self roundUIiew:self.genderBox];
    [self roundUIiew:self.cantextBox];
    [self roundUIiew:self.fitlevelBox];
    [self roundUIiew:self.teamBox];
    [self roundUIiew:self.zipBox];
    [self roundUIiew:self.weightBox];
    //style button
  //  [self.registerBtn configureBtn:self.registerBtn];
    
}
- (void)roundUIiew:(UIView*)layerBox {
    layerBox.layer.cornerRadius = 5;
    layerBox.layer.masksToBounds = YES;
}
- (void)loadData{
    NSString *userID = [userDefaults objectForKey:kUserID];
    //then get the data
    NSString *url = [NSString stringWithFormat: @"%@userData.php?uid=%@",BaseURL, userID];
    NSURL *eventURL = [NSURL URLWithString: url];
    NSLog(@"%@", eventURL);
    NSData *jsonData = [NSData dataWithContentsOfURL: eventURL];
    NSError *error = nil;
    NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error: &error];
    
    NSDictionary *userInfo = [dataDictionary objectForKey:@"fitpass"];
    NSLog(@"%@", userInfo);
    //see if there is facebook login
    NSString *fbID = [userDefaults objectForKey:kFBID];
    if ([[userInfo objectForKey:@"photo"] isEqualToString:@""] &&  [fbID isEqualToString:@""]){
        //create round image
        self.profileImg.image = [UIImage imageNamed:@"add_photo"];
        self.profileImg.layer.cornerRadius = self.profileImg.frame.size.width / 2;
        self.profileImg.clipsToBounds = YES;
        //create round image border
        self.profileImg.layer.borderWidth = 3.0f;
        self.profileImg.layer.borderColor = [UIColor whiteColor].CGColor;
    }else{
        if ([[userInfo objectForKey:@"photo"] isEqualToString:@""]){
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=normal", fbID]];
            NSLog(@"%@", url);
            NSData  *data = [NSData dataWithContentsOfURL:url];
            self.profileImg.image = [UIImage imageWithData:data];
            self.image = [UIImage imageWithData:data];
        }else{
            NSString *userphoto = [NSString stringWithFormat: @"%@users/%@",BaseURLFOR_IMAGE, [userInfo objectForKey:@"photo"]];
            NSURL *photoURL = [NSURL URLWithString:userphoto];
            NSData *imageData = [NSData dataWithContentsOfURL:photoURL];
            
            self.image = [UIImage imageWithData:imageData];
            self.profileImg.image = [UIImage imageWithData:imageData];
        }
        //blur image
        [self updateImage];
        self.addPhotoLabel.text=@"Change Photo";
        //create round image
        self.profileImg.layer.cornerRadius = self.profileImg.frame.size.width / 2;
        self.profileImg.clipsToBounds = YES;
        self.profileImg.layer.borderWidth = 3.0f;
        self.profileImg.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    //change placeholder of the fieldsw.
    UIColor *color = [UIColor colorWithRed:36/255.0f green:121/255.0f blue:110/255.0f alpha:1.0];
    NSString *firstname = [userInfo objectForKey:@"fname"];
    if ([firstname isEqualToString:@""]){
        self.firstName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"First Name" attributes:@{NSForegroundColorAttributeName: color}];
    }else{
        self.firstName.text = firstname;
    }
    
    NSString *lastname = [userInfo objectForKey:@"lname"];
    if ([lastname isEqualToString:@""]){
        self.lastName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Last Name" attributes:@{NSForegroundColorAttributeName: color}];
    }else{
        self.lastName.text = lastname;
    }
    
    NSString *userEmail = [userInfo objectForKey:@"email"];
    if ([userEmail isEqualToString:@""]){
        self.emailAddress.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email Address" attributes:@{NSForegroundColorAttributeName: color}];
    }else{
        self.emailAddress.text = userEmail;
    }
    
    self.password.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"" attributes:@{NSForegroundColorAttributeName: color}];
    
    NSString *userage = [userInfo objectForKey:@"age"];
    if ([[userInfo objectForKey:@"age"] isEqualToString:@""]){
        self.age.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Age" attributes:@{NSForegroundColorAttributeName: color}];
    }else{
        self.age.text = userage;
    }
    
    NSString *contactOK = [userInfo objectForKey:@"contact_ok"];
    if ([[userInfo objectForKey:@"contact_ok"] isEqualToString:@""]){
        self.textOK.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Yes/No" attributes:@{NSForegroundColorAttributeName: color}];
    }else{
        self.textOK.text = contactOK;
    }
    
    NSString *userFitLvl = [userInfo objectForKey:@"act_lvl"];
    if ([[userInfo objectForKey:@"act_lvl"] isEqualToString:@""]){
        self.fitLevel.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Light/Moderate/Advanced" attributes:@{NSForegroundColorAttributeName: color}];
    }else{
        self.fitLevel.text = userFitLvl;
    }
    
    NSString *userGender = [userInfo objectForKey:@"gender"];
    if ([[userInfo objectForKey:@"gender"] isEqualToString:@""]){
        self.gender.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Gender" attributes:@{NSForegroundColorAttributeName: color}];
    }else{
        self.gender.text = userGender;
    }
    
    NSString *userZip = [userInfo objectForKey:@"zip"];
    if ([userGender isEqualToString:@""]){
        self.zipCode.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Zip Code" attributes:@{NSForegroundColorAttributeName: color}];
    }else{
        self.zipCode.text = userZip;
    }
    
    NSString *userTeam = [userInfo objectForKey:@"team"];
    if ([[userInfo objectForKey:@"team"] isEqualToString:@""]){
        self.teamName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"" attributes:@{NSForegroundColorAttributeName: color}];
    }else{
        self.teamName.text = userTeam;
    }
    NSString *weightInfo = [userInfo objectForKey:@"current_weight"];
    if ([userEmail isEqualToString:@""]){
        self.weight.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Weight" attributes:@{NSForegroundColorAttributeName: color}];
    }else{
        self.weight.text = weightInfo;
    }

}
- (void)updateImage{
    UIImage *effectImage = nil;
    effectImage = [self.image applyDarkEffect];
    self.blurImage.image = effectImage;
}

#pragma mark - SlideNavigationController Methods -
- (void)createMenu{
    
    //create menu
    SWRevealViewController *revealController = [self revealViewController];
    //create the left menu button
    UIButton *button  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [button setImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
    [button addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
    
    UITapGestureRecognizer *viewTapped=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewTapped:)];
    [self.view addGestureRecognizer:viewTapped];
    viewTapped.delegate = self;
    [viewTapped setCancelsTouchesInView:NO];
    
    self.revealViewController.delegate = self;
    [revealController.frontViewController.revealViewController tapGestureRecognizer];
    
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
       shouldReceiveTouch:(UITouch *)touch
{
    
    [self dismissKeyboard];
    if(touch.view == self.addPictureBtn){
        return NO;
    }
    return YES;
}
-(void)viewTapped:(UITapGestureRecognizer*)gestureRecognizer
{
    
    [self dismissKeyboard];
    //Do what you want here
    SWRevealViewController *revealController = self.revealViewController;
    if (revealController.frontViewPosition == FrontViewPositionRight) {
        [revealController revealToggleAnimated:YES];
    }
    
}
-(void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position {
    
    [self dismissKeyboard];
    if(position == FrontViewPositionLeft) {
        self.view.userInteractionEnabled = YES;
    } else {
        self.view.userInteractionEnabled = NO;
    }
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position {
    
    [self dismissKeyboard];
    if(position == FrontViewPositionLeft) {
        self.view.userInteractionEnabled = YES;
    } else {
        self.view.userInteractionEnabled = NO;
    }
}


#pragma mark - Change Picture
- (IBAction)addPicture:(id)sender {
    
    [self dismissKeyboard];
    
    NSString *actionSheetTitle = @"Change Photo";
    NSString *other1 = @"Take Photo";
    NSString *other2 = @"Choose Photo";
    NSString *cancelTitle = @"Cancel Button";
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:actionSheetTitle
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:other1, other2, nil];
    
    [actionSheet showInView:self.view];
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //Get the name of the current pressed button
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    
    if ([buttonTitle isEqualToString:@"Take Photo"])
    {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:NULL];
    }
    
    if ([buttonTitle isEqualToString:@"Choose Photo"])
    {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:NULL];
    }
    
    if ([buttonTitle isEqualToString:@"Cancel"])
    {
        [actionSheet dismissWithClickedButtonIndex:3 animated:YES];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //[userDefaults setObject:NULL forKey:kProfilePic];
    //[userDefaults synchronize];
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    
    self.profileImg.image = chosenImage;
    
    UIImage *image = chosenImage;
    UIImage *tempImage = nil;
    CGSize targetSize = CGSizeMake(230,230);
    UIGraphicsBeginImageContext(targetSize);
    
    CGRect thumbnailRect = CGRectMake(0, 0, 0, 0);
    thumbnailRect.origin = CGPointMake(0.0,0.0);
    thumbnailRect.size.width  = targetSize.width;
    thumbnailRect.size.height = targetSize.height;
    
    [image drawInRect:thumbnailRect];
    
    tempImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    NSData *imageData = UIImageJPEGRepresentation(tempImage, 50);
    NSString *urlString = [NSString stringWithFormat:@"%@photo.php?ID=%@",BaseURL, [userDefaults objectForKey:kUserID]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    //  NSLog(@"%@", urlString);
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"userfile\"; filename=\".jpg\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:imageData]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    NSLog(@"Return String %@", returnString);
    
    NSLog(@"UPLOADING");
    
    [picker dismissViewControllerAnimated:YES completion:^{
        //update background image
        self.image =chosenImage;
        [self updateImage];
    }];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

#pragma mark - Profile Update Submitting
- (IBAction)signUp:(id)sender
{
    NSLog(@"Sign Up");
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    progressHUD.labelText = @"Updating...";
    progressHUD.mode = MBProgressHUDAnimationFade;
    
    
    //check for required fields
    if ([firstName.text isEqualToString:@""] ||
        [lastName.text isEqualToString:@""]) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention!" message:@"First name and last name can not be empty!" delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alertView show];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        return;
    }else{
        
        NSString *userID = [userDefaults objectForKey:kUserID];
        //compile the URL
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
       
        NSDictionary *params =  [[NSDictionary alloc] initWithObjectsAndKeys:
                                 firstName.text, @"first_name",
                                 lastName.text, @"last_name",
                                 fitLevel.text, @"fit_level",
                                 zipCode.text, @"zip_code",
                                 gender.text, @"gender",
                                 age.text, @"age",
                                 emailAddress.text, @"email",
                                 password.text, @"password",
                                 textOK.text, @"text_ok",
                                 teamName.text, @"team",
                                 weight.text, @"weight",
                                 userID, @"uID",
                                 nil] ;
        //PROCESS IT
        [manager POST:[NSString stringWithFormat:@"%@profileUpdate.php",BaseURL] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            
            //assign value to system default
            [userDefaults setObject:@"1" forKey:kLoggedIn];
            [userDefaults setObject:userID forKey:kUserID];
            [userDefaults setObject:emailAddress.text forKey:kEmail];
            [userDefaults setObject:password.text forKey:kPassword];
            [userDefaults setObject:firstName.text forKey:kFirstName];
            [userDefaults synchronize];
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Confirmed!" message:@"Your profile has been updated!" delegate: nil cancelButtonTitle:@"OK"  otherButtonTitles:nil, nil];
            
            [alertView show];

            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
            
            SWRevealViewController *revealController = [self revealViewController];
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                     bundle: nil];
            
            UIViewController *newFrontController = nil;
            UIViewController *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"accountViewController"];
            newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
            [revealController pushFrontViewController:newFrontController animated:YES];
            return;
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error.localizedDescription);
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        }];
    }
}


#pragma mark - Text Field Editing
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Text Field Drop Down
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    //see if I can get the y position of the text field checked
    //double positionX = textField.frame.origin.x;
    //double positionY = textField.frame.origin.y + 100;
   // double positionX= 0;
    //double fieldwidth = textField.frame.size.width;
    double fieldwidth = self.view.frame.size.width;
    [self.lblSelectedNames setHidden:YES];
    
    [self.gender resignFirstResponder];
    [self.textOK resignFirstResponder];
    [self.fitLevel resignFirstResponder];
    
    if (textField == self.gender) {
        int optionCt = [genderArray count];
        double viewHeight = optionCt * 88;
        double positionY = (self.view.frame.size.height - viewHeight)/2;
        /*[self showPopUpWithTitle:@"" withOption:genderArray xy:CGPointMake(positionX, positionY) size:CGSizeMake(fieldwidth, viewHeight) isMultiple:NO];*/
        [self showPopUpWithTitle:@"" withOption:genderArray xy:CGPointMake(0, positionY) size:CGSizeMake(fieldwidth, viewHeight) isMultiple:NO];
        //set the label market to gender so we know what we are clicking
        self.lblSelectedNames.text = @"Gender";
    }else if (textField == self.textOK){        int optionCt = [booleanArray count];
        double viewHeight = optionCt * 88;
        double positionY = (self.view.frame.size.height - viewHeight)/2;
        /*[self showPopUpWithTitle:@"" withOption:booleanArray xy:CGPointMake(positionX, positionY) size:CGSizeMake(fieldwidth, viewHeight) isMultiple:NO];*/
        [self showPopUpWithTitle:@"" withOption:booleanArray xy:CGPointMake(0, positionY) size:CGSizeMake(fieldwidth, viewHeight) isMultiple:NO];
        self.lblSelectedNames.text = @"Text";
    }else if (textField == self.fitLevel) {
        int optionCt = [activityArray count];
        double viewHeight = optionCt * 88;
        double positionY = (self.view.frame.size.height - viewHeight)/2;
        /*[self showPopUpWithTitle:@"" withOption:activityArray xy:CGPointMake(positionX, positionY) size:CGSizeMake(fieldwidth, viewHeight) isMultiple:NO];*/
        [self showPopUpWithTitle:@"" withOption:activityArray xy:CGPointMake(0, positionY) size:CGSizeMake(fieldwidth, viewHeight) isMultiple:NO];
        self.lblSelectedNames.text = @"Level";
    }
}

-(void)showPopUpWithTitle:(NSString*)popupTitle withOption:(NSArray*)arrOptions xy:(CGPoint)point size:(CGSize)size isMultiple:(BOOL)isMultiple{
    
    
    Dropobj = [[DropDownListView alloc] initWithTitle:popupTitle options:arrOptions xy:point size:size isMultiple:isMultiple];
    Dropobj.delegate = self;
    [Dropobj showInView:self.view animated:YES];
    
    /*----------------Set DropDown backGroundColor-----------------*/
    [Dropobj SetBackGroundDropDwon_R:0.0 G:108.0 B:194.0 alpha:0.70];
    
}

- (void)DropDownListView:(DropDownListView *)dropdownListView Datalist:(NSMutableArray*)ArryData{
    
    if (ArryData.count>0) {
        lblSelectedNames.text=[ArryData componentsJoinedByString:@"\n"];
        CGSize size=[self GetHeightDyanamic:lblSelectedNames];
        lblSelectedNames.frame=CGRectMake(0, 100, size.width, size.height);
    }
    else{
        lblSelectedNames.text=@"";
    }
    
}
- (void)DropDownListViewDidCancel{
    
}


-(CGSize)GetHeightDyanamic:(UILabel*)lbl
{
    NSRange range = NSMakeRange(0, [lbl.text length]);
    CGSize constraint;
    constraint= CGSizeMake(288 ,MAXFLOAT);
    CGSize size;
    
    if (([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)) {
        NSDictionary *attributes = [lbl.attributedText attributesAtIndex:0 effectiveRange:&range];
        CGSize boundingBox = [lbl.text boundingRectWithSize:constraint options: NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size;
        
        size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    }
    else{
        size = [lbl.text sizeWithFont:[UIFont fontWithName:@"Helvetica" size:14] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    }
    return size;
}

-(void)DropDownListView:(DropDownListView *)dropdownListView didSelectedIndex:(NSInteger)anIndex{
    NSLog(@"Index: %ld", (long)anIndex);
    if (self.gender && [self.lblSelectedNames.text isEqual: @"Gender"]) {
        self.gender.text = [genderArray objectAtIndex:anIndex];
    }
    if (self.textOK && [self.lblSelectedNames.text  isEqual: @"Text"]) {
        self.textOK.text = [booleanArray objectAtIndex:anIndex];
    }
    if (self.fitLevel && [self.lblSelectedNames.text  isEqual: @"Level"]) {
        self.fitLevel.text = [activityArray objectAtIndex:anIndex];
    }
    [Dropobj fadeOut];
}

-(void)dismissKeyboard {
    NSLog(@"HIDE KEYBOARD");
    
    [Dropobj fadeOut];
    [self.firstName resignFirstResponder];
    [self.lastName resignFirstResponder];
    [self.emailAddress resignFirstResponder];
    [self.password resignFirstResponder];
    [self.gender resignFirstResponder];
    [self.age resignFirstResponder];
    [self.textOK resignFirstResponder];
    [self.fitLevel resignFirstResponder];
    [self.zipCode resignFirstResponder];
    [self.teamName resignFirstResponder];
    [self.weight resignFirstResponder];
}
- (IBAction)BackClicked
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    SWRevealViewController *revealController = [self revealViewController];
    UIViewController *newFrontController = nil;
    UIViewController *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"accountViewController"];
    newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    
    [revealController pushFrontViewController:newFrontController animated:YES];
}
@end
