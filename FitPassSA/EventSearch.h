//
//  EventSearch.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 4/7/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"
#import "SWTableViewCell.h"

@interface EventSearch : UIViewController<UITableViewDelegate, UITableViewDataSource, SWTableViewCellDelegate>

@property (strong, nonatomic) IBOutlet UITextField *searchFld;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UILabel *instructionTxt;
@property (strong, nonatomic) IBOutlet UILabel *noResultTxt;


@property (nonatomic, strong) NSMutableArray *eventList;
@property(strong, nonatomic) NSIndexPath *savedSelectedIndexPath;


- (IBAction)goHome:(id)sender;
- (IBAction)submitSearch:(id)sender;

@end
