//
//  ViewController.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/9/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import "ViewController.h"
#import "UIButton+BtnStyle.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "AFNetworking.h"
#import "AppSetting.h"
#import "SWRevealViewController.h"
#import "SignUp.h"
#import "Login.h"
#import "GAIDictionaryBuilder.h"
#import "GAI.h"
#import "GAIFields.h"
#import "Header.h"
@interface ViewController (){
    
    NSUserDefaults *userDefaults;
}

@end

@implementation ViewController

#pragma mark - Object lifecycle
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self.slideOutAnimationEnabled = YES;
    
    return [super initWithCoder:aDecoder];
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // We wire up the FBSDKLoginButton using the interface builder
        // but we could have also explicitly wired its delegate here.
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear{
    
    NSString *str = @"Home View";
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:str];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    tracker.allowIDFACollection = YES;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"appbkgd.png"]]];
    
    //load button Style
    [self.SignUp configureBtn:self.SignUp];
    
    userDefaults = [[NSUserDefaults alloc] init];
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    
    //facebook button
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(observeProfileChange:) name:FBSDKProfileDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(observeTokenChange:) name:FBSDKAccessTokenDidChangeNotification object:nil];

    self.loginButton.readPermissions = @[@"public_profile", @"email", @"user_friends"];
   // FBSDKLoginButton *loginButton = [[FBSDKLoginButton alloc] init];
    if ([FBSDKAccessToken currentAccessToken]) {
        // User is logged in, do work such as go to next view controller.
        NSLog(@"Token created");
        [self observeProfileChange:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)application:(UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    [FBSDKLoginButton class];
    return YES;
}


- (void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error {
    if (error) {
        NSLog(@"Unexpected login error: %@", error);
        NSString *alertMessage = error.userInfo[FBSDKErrorLocalizedDescriptionKey] ?: @"There was a problem logging in. Please try again later.";
        NSString *alertTitle = error.userInfo[FBSDKErrorLocalizedTitleKey] ?: @"Oops";
        [[[UIAlertView alloc] initWithTitle:alertTitle
                                    message:alertMessage
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    } else {
        //if (_viewIsVisible) {
       //   [self performSegueWithIdentifier:@"showMain" sender:self];
        //}
        NSLog(@"Result: %@", result);
    }
}

- (void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton {
   // if (_viewIsVisible) {
     //   [self performSegueWithIdentifier:@"continue" sender:self];
//    }
}

- (void)observeProfileChange:(NSNotification *)notfication {
    if ([FBSDKProfile currentProfile]) {
      //  NSString *title = [NSString stringWithFormat:@"continue as %@", [FBSDKProfile currentProfile].name];
        
      
        //[self.continueButton setTitle:title forState:UIControlStateNormal];
    }
    //get user information
    if ([FBSDKAccessToken currentAccessToken]) {
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error) {
                 NSLog(@"fetched user:%@", result);
                 NSString *fbemail = result[@"email"];
                 NSString *fbfname = result[@"first_name"];
                 NSString *fblname = result[@"last_name"];
                 [userDefaults setObject:result[@"id"] forKey:kFBID];
                 [userDefaults synchronize];
                 [self checkUser:fbemail checkFname:fbfname checkLname:fblname];
             }
         }];
    }
}
- (void)observeTokenChange:(NSNotification *)notfication {
    if (![FBSDKAccessToken currentAccessToken]) {
       // [self.continueButton setTitle:@"continue as a guest" forState:UIControlStateNormal];
    } else {
        [self observeProfileChange:nil];
    }
}


//get user id based on email
- (void)checkUser:(NSString*)fbemail checkFname:(NSString*)fbfname checkLname:(NSString *)fblname{
    
    //compose the url parameters
    NSString *url = [NSString stringWithFormat: @"%@fblogin.php?email=%@&fname=%@&lname=%@",BaseURL, fbemail, fbfname, fblname];
    NSURL *eventURL = [NSURL URLWithString: url];
    NSLog(@"%@", eventURL);
    NSData *jsonData = [NSData dataWithContentsOfURL: eventURL];
    NSError *error = nil;
    NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error: &error];
  
    //get data
    NSDictionary *userDictionary = [dataDictionary objectForKey:@"user"];
        
    [userDefaults setObject:[userDictionary objectForKey:@"id"] forKey:kUserID];
    [userDefaults setObject:[userDictionary objectForKey:@"fname"] forKey:kFirstName];
    [userDefaults setObject:[userDictionary objectForKey:@"lname"] forKey:kLastName];
    [userDefaults setObject:[userDictionary objectForKey:@"email"] forKey:kEmail];
    [userDefaults setObject:@"1" forKey:kLoggedIn];
    [userDefaults setObject:nil forKey:kEventCat];
    [userDefaults setObject:nil forKey:kEventDate];
    
    [userDefaults synchronize];
    
    UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SWRevealViewController *revealVC = [main instantiateViewControllerWithIdentifier:@"swreveal"];
    
    [self presentViewController:revealVC animated:YES completion:nil];
    
}
- (IBAction)signUpAction:(id)sender {
   
    UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //assign instruction to new uiviewcontroller
    UIViewController *instructionViewController = [mainstoryboard instantiateViewControllerWithIdentifier:@"SignUpViewContorller"];
    [self presentViewController:instructionViewController animated:YES completion:nil];
    
    NSLog(@"sign up");
}

- (IBAction)loginAction:(id)sender {
    
    UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //assign instruction to new uiviewcontroller
    UIViewController *instructionViewController = [mainstoryboard instantiateViewControllerWithIdentifier:@"loginViewController"];
    [self presentViewController:instructionViewController animated:YES completion:nil];
    
}
@end
