//
//  Sponsor.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 4/16/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Sponsor : NSObject

@property (nonatomic, strong) NSString *sponsorID;
@property (nonatomic, strong) NSString *sponsorTitle;
@property (nonatomic, strong) NSString *sponsorURL;
@property (nonatomic, strong) NSString *sponsorPhoto;
@property (nonatomic, strong) NSString *sponsorOrder;

- (id) initWithTitle:(NSString *)title;
+(id) blogPostWithTitle:(NSString *)title; //convenient constructor

//implementation to conver to URL
- (NSURL *) siteURL;
- (NSURL *) thumbnailURL;
@end
