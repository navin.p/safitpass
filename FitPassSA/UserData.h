//
//  UserData.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 5/6/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "UITextField+TextFld.h"
#import "UIButton+BtnStyle.h"
#import "DropDownListView.h"
#import "UIImage+ImageEffects.h"
#import "UserInfo.h"

@interface UserData : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate, UITextFieldDelegate, kDropDownListViewDelegate, UIGestureRecognizerDelegate, UITextViewDelegate, UIScrollViewDelegate>
{
    DropDownListView *Dropobj;
    NSArray *arryList;
    NSArray *booleanList;
    NSArray *activityList;
}

@property (strong, nonatomic) IBOutlet UITextField *firstName;
@property (strong, nonatomic) IBOutlet UITextField *lastName;
@property (strong, nonatomic) IBOutlet UITextField *emailAddress;
@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UITextField *age;
@property (strong, nonatomic) IBOutlet UITextField *textOK;
@property (strong, nonatomic) IBOutlet UITextField *fitLevel;
@property (strong, nonatomic) IBOutlet UITextField *gender;
@property (strong, nonatomic) IBOutlet UITextField *zipCode;
@property (strong, nonatomic) IBOutlet UITextField *teamName;
@property (strong, nonatomic) IBOutlet UIButton *registerBtn;
@property (strong, nonatomic) IBOutlet UITextField *weight;

@property (strong, nonatomic) IBOutlet UILabel *lblSelectedNames;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (nonatomic, assign) BOOL slideOutAnimationEnabled;

@property (strong, nonatomic) IBOutlet UILabel *addPhotoLabel;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *viewTapped;
@property (strong, nonatomic) IBOutlet UIImageView *blurImage;
@property (strong, nonatomic) IBOutlet UIImageView *profileImg;

#pragma event outlets
@property (strong, nonatomic) IBOutlet UIButton *addPictureBtn;
@property (strong, nonatomic) IBOutlet UIView *nameBox;
@property (strong, nonatomic) IBOutlet UIView *lnameBox;
@property (strong, nonatomic) IBOutlet UIView *emailBox;
@property (strong, nonatomic) IBOutlet UIView *pwBox;
@property (strong, nonatomic) IBOutlet UIView *ageBox;
@property (strong, nonatomic) IBOutlet UIView *genderBox;
@property (strong, nonatomic) IBOutlet UIView *cantextBox;
@property (strong, nonatomic) IBOutlet UIView *fitlevelBox;
@property (strong, nonatomic) IBOutlet UIView *zipBox;
@property (strong, nonatomic) IBOutlet UIView *teamBox;
@property (weak, nonatomic) IBOutlet UIView *weightBox;

#pragma Variables
@property (nonatomic) UIImage *image;
@property (nonatomic) int imageIndex;

#pragma actions
- (IBAction)addPicture:(id)sender;
- (IBAction)signUp:(id)sender;

@end
