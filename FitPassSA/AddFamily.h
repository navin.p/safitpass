//
//  AddFamily.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 5/8/17.
//  Copyright © 2017 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "UITextField+TextFld.h"
#import "UIButton+BtnStyle.h"
#import "DropDownListView.h"
#import "UIImage+ImageEffects.h"
#import "UserInfo.h"
#import "Family.h"
#import "SWRevealViewController.h"

@interface AddFamily : UIViewController<UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate, UITextFieldDelegate, kDropDownListViewDelegate, UIGestureRecognizerDelegate, UITextViewDelegate, UIScrollViewDelegate, SWRevealViewControllerDelegate>
{
    DropDownListView *Dropobj;
    NSArray *arryList;
    NSArray *booleanList;
    NSArray *activityList;
}


@property (strong, nonatomic) IBOutlet UITextField *firstName;
@property (strong, nonatomic) IBOutlet UITextField *lastName;
@property (strong, nonatomic) IBOutlet UITextField *weight;
@property (strong, nonatomic) IBOutlet UITextField *age;
@property (strong, nonatomic) IBOutlet UITextField *fitLevel;
@property (strong, nonatomic) IBOutlet UITextField *gender;
@property (strong, nonatomic) IBOutlet UITextField *zipCode;
@property (strong, nonatomic) IBOutlet UIButton *registerBtn;
@property (weak, nonatomic) IBOutlet UITextField *teamName;
@property (weak, nonatomic) IBOutlet UILabel *parentID;
@property (weak, nonatomic) IBOutlet UILabel *memberIDFD;

@property (strong, nonatomic) IBOutlet UILabel *lblSelectedNames;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (nonatomic, assign) BOOL slideOutAnimationEnabled;

@property (strong, nonatomic) IBOutlet UILabel *addPhotoLabel;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *viewTapped;
@property (strong, nonatomic) IBOutlet UIImageView *blurImage;
@property (strong, nonatomic) IBOutlet UIImageView *profileImg;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *profileImgHeight;

#pragma event outlets
@property (strong, nonatomic) IBOutlet UIButton *addPictureBtn;
@property (strong, nonatomic) IBOutlet UIView *nameBox;
@property (strong, nonatomic) IBOutlet UIView *lnameBox;
@property (strong, nonatomic) IBOutlet UIView *weightBox;
@property (strong, nonatomic) IBOutlet UIView *ageBox;
@property (strong, nonatomic) IBOutlet UIView *genderBox;
@property (strong, nonatomic) IBOutlet UIView *fitlevelBox;
@property (strong, nonatomic) IBOutlet UIView *zipBox;
@property (weak, nonatomic) IBOutlet UIView *teamBox;


#pragma Variables
@property (nonatomic) UIImage *image;
@property (nonatomic) int imageIndex;
@property (nonatomic, strong) Family *userInfo;

#pragma actions
- (IBAction)addPicture:(id)sender;
- (IBAction)signUp:(id)sender;


@end
