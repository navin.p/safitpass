//
//  SwitchAcct.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 5/15/17.
//  Copyright © 2017 Ping-jung Tsai. All rights reserved.
//

#import "SwitchAcct.h"
#import "SwitchAcctCell.h"
#import "Family.h"

#import "MBProgressHUD.h"
#import "SWRevealViewController.h"
#import "AppSetting.h"

#import "GAIDictionaryBuilder.h"
#import "GAI.h"
#import "GAIFields.h"

#import "EventLists.h"
#import "Header.h"
@interface SwitchAcct (){
    NSUserDefaults *userDefaults;
}
@end

@implementation SwitchAcct

@synthesize memberCollection = _collectionView;
static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidAppear{
    
    //GA Log
    NSString *str = @"Switch Account";
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:str];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    tracker.allowIDFACollection = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    userDefaults = [[NSUserDefaults alloc] init];
    userDefaults = [NSUserDefaults standardUserDefaults];    //testing to see if the collection view is loading
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
 
    self.savedSelectedIndexPath = nil;
    
    self.memberCollectionLayout = [[UICollectionViewFlowLayout alloc] init];
    [self.memberCollectionLayout setItemSize:CGSizeMake(120, 200)];
    [self.memberCollectionLayout setSectionInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    self.memberCollectionLayout.minimumLineSpacing = 0;
    self.memberCollectionLayout.minimumInteritemSpacing = 0;
    
    [self.memberCollection setCollectionViewLayout:self.memberCollectionLayout];
    [self loadFamily];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) loadFamily{
    //construct URL
    NSString *userID = [userDefaults objectForKey:kCurrentUser];
    
    //show progress bar
    NSString *url = [NSString stringWithFormat: @"%@switchAcct.php?uid=%@",BaseURL, userID];
        NSURL *eventURL = [NSURL URLWithString: url];
        NSData *jsonData = [NSData dataWithContentsOfURL: eventURL];
        NSError *error = nil;
        NSDictionary *dataDictionary  = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error: &error];
        self.memberList = [NSMutableArray array];
        // NSString *eventFound = [dataDictionary objectForKey:@"notfound"];
        //if nothing is found
        NSArray *familyArray = [dataDictionary objectForKey:@"fitpass"];

        for(NSDictionary *familyDictionary in familyArray){
            Family *family =  [Family blogPostWithID:[familyDictionary objectForKey:@"ID"]];
            family.userID = [familyDictionary objectForKey:@"ID"];
            family.userFirstName = [familyDictionary objectForKey:@"fname"];
            family.userLastName = [familyDictionary objectForKey:@"lname"];
            family.userFullName = [familyDictionary objectForKey:@"full_name"];
            family.userPhoto = [familyDictionary objectForKey:@"photo"];
            [self.memberList addObject:family];
            [_collectionView reloadData];
        }
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.memberList count];
    //return 5;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    SwitchAcctCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    // Configure the cell
    Family *member = [self.memberList objectAtIndex:indexPath.item];
    cell.userName.text = member.userFullName;
    //get image
    NSData *imageData = [NSData dataWithContentsOfURL:member.thumbnailURL];
    cell.userPhoto.image = [UIImage imageWithData:imageData];
    
    cell.userPhoto.layer.cornerRadius = cell.userPhoto.frame.size.width / 2;
    cell.userPhoto.clipsToBounds = YES;
    cell.userPhoto.layer.borderWidth = 3.0f;
    cell.userPhoto.layer.borderColor = [UIColor whiteColor].CGColor;
    
    
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    //self.savedSelectedIndexPath = [self.memberCollection indexPathsForSelectedItems];
    //get the index value of what's clicked
    int itemNo = (int)[indexPath row];
    //SwitchAcctCell *selectedCell =(SwitchAcctCell*)[collectionView cellForItemAtIndexPath:indexPath];
    //NSLog(@"%d", itemNo);
    
    //refresh the current user ID
    Family *family = [self.memberList objectAtIndex:itemNo];
    NSString *memberID = family.userID;
    [userDefaults setObject:memberID forKey:kCurrentUser];
    [userDefaults synchronize];
    
    //switch back to previous screen
    UINavigationController *navigationController = self.navigationController;
    [navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
