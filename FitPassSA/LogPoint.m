//
//  LogPoint.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/26/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import "LogPoint.h"
#import "EventDetail.h"
#import "URBAlertView.h"
#import "MBProgressHUD.h"
#import "AFNetworking.h"
#import "AppSetting.h"
#import <QuartzCore/QuartzCore.h>
#import "SWRevealViewController.h"
#import "SharePoint.h"
#import "BonusShare.h"
#import "EventList.h"
#import "GAIDictionaryBuilder.h"
#import "GAI.h"
#import "GAIFields.h"
#import "Header.h"
@interface LogPoint (){
    NSUserDefaults *userDefaults;
}
@end

@implementation LogPoint
@synthesize eventDetail;
@synthesize codeField;


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    //GA Log
    NSString *str = [NSString stringWithFormat:@"%@ - %@", @"Log Points", self.eventTitleV];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:str];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    tracker.allowIDFACollection = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    userDefaults = [[NSUserDefaults alloc] init];
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    [[self navigationController] popToRootViewControllerAnimated:YES];
    self.navigationController.navigationBarHidden = YES;
    
    //style the text field
    [codeField.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    [codeField.layer setBorderWidth: 1.0f];
    self.codeField.clipsToBounds = YES;
    UIColor *color = [UIColor whiteColor];
    self.codeField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter Code" attributes:@{NSForegroundColorAttributeName: color}];
    self.codeField.delegate = self;
    [self.codeField becomeFirstResponder];
    
    self.scrowView.delegate = self;
    [self.scrowView setScrollEnabled:YES];
    /*
    ** attach scroll view *
     
    self.scrowView.frame = CGRectMake(0, 80, self.view.frame.size.width, self.scrowView.frame.size.height);
    [self.scrowView setContentSize:CGSizeMake(self.view.frame.size.width, 1050)];
    self.contentView.frame = CGRectMake(0, 80, self.view.frame.size.width, self.contentView.frame.size.height);
    */
    //show progress bar
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    progressHUD.labelText = @"Loading...";
    progressHUD.mode = MBProgressHUDAnimationFade;
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        NSString *eID = [userDefaults objectForKey:kEventID];
        //then get the data
        NSString *url = [NSString stringWithFormat: @"%@eventDetail.php?eid=%@",BaseURL, eID];
        NSURL *eventURL = [NSURL URLWithString: url];
        NSLog(@"%@", eventURL);
        NSData *jsonData = [NSData dataWithContentsOfURL: eventURL];
        NSError *error = nil;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error: &error];
    
        NSDictionary *userInfo = [dataDictionary objectForKey:@"fitpass"];
    
        NSLog(@"%@", userInfo);
        self.eventTitleV =[userInfo objectForKey:@"title"];
        self.eventPointV = [userInfo objectForKey:@"point"];
        self.photoNameV = [userInfo objectForKey:@"photo"];
        self.socialBonus = [userInfo objectForKey:@"shareBonus"];
        
        self.eventTitle.text = self.eventTitleV;
        self.eventPoint.text = self.eventPointV;
    
        if ([self.photoNameV isEqualToString:@""]){
            self.photoHeightLimit.constant = 0;
        }else{
            NSString *imageurl = [NSString stringWithFormat: @"%@events/%@",BaseURLFOR_IMAGE, self.photoNameV];
            NSURL *thumbnailURL = [NSURL URLWithString: imageurl];
            NSData *imageData = [NSData dataWithContentsOfURL: thumbnailURL];
            UIImage *image = [UIImage imageWithData:imageData];
            self.eventPhoto.image = image;
        
            self.eventPhoto.layer.cornerRadius = self.eventPhoto.frame.size.width / 2;
            self.eventPhoto.clipsToBounds = YES;
            //create round image border
            self.eventPhoto.layer.borderWidth = 3.0f;
            self.eventPhoto.layer.borderColor = [UIColor whiteColor].CGColor;
        }
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    });
        
    //load data
    [codeField setDelegate:self];

}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [codeField resignFirstResponder];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma IBActions
- (IBAction)closeWindow:(id)sender {
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    SWRevealViewController *revealController = [self revealViewController];
    UIViewController *newFrontController = nil;
    EventList *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"eventListViewController"];
    newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    [revealController pushFrontViewController:newFrontController animated:YES];
}

- (IBAction)logPoints:(id)sender {
    //confirm fields are in
    if ([codeField.text isEqualToString:@""]) {
        
        URBAlertView *alertView = [[URBAlertView alloc] initWithTitle:@"Attention!" message:@"Please enter the event code!" cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alertView setHandlerBlock:^(NSInteger buttonIndex, URBAlertView *alertView) {
            [alertView hideWithCompletionBlock:^{
                
                if (buttonIndex == 0) {
                    [alertView hideWithAnimation:URBAlertAnimationFade];
                }
                
            }];
        }];
        [alertView show];
        return;
    }else{
        //show progress bar
        MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        progressHUD.labelText = @"Logging Point...";
        progressHUD.mode = MBProgressHUDAnimationFade;
        
     //   NSString *uID = [userDefaults objectForKey:kUserID];
        NSString *eID =   [userDefaults objectForKey:kEventID];
        NSString *uID = [userDefaults objectForKey:kCurrentUser];
        
        //compose the url parameters
        //compile the URL
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
      //  NSURL *url = [NSURL URLWithString:@"http://www.safitpass.com.php53-22.ord1-1.websitetestlink.com/json/logPoint.php"];
      
        NSDictionary *params =  [[NSDictionary alloc] initWithObjectsAndKeys:
                                 uID, @"uid",
                                 codeField.text, @"code",
                                 eID, @"eid",
                                 nil] ;
        NSLog(@"%@", params);
        [manager POST:[NSString stringWithFormat:@"%@logPoint.php",BaseURL] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            
            //is this successful or is it a duplicate entries
            NSString *dupUser = [NSString stringWithFormat:@"%@",[responseObject objectAtIndex:0]];
            NSString *userPT = [NSString stringWithFormat:@"%@",[responseObject objectAtIndex:1]];
            if ([dupUser isEqualToString:@"Invalid"]){
                // invalid information
                [self killHUD];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops!" message:@"You have entered the wrong event code. Please try again." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                
                [alertView show];
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            }else{
                //update total point in userDefault
                [userDefaults setObject:userPT forKey:kUserPoints];
                [userDefaults synchronize];
                
                //NSLog(@"%@", [userDefaults objectForKey:kUserPoints]);
                [self killHUD];
                //switch view
                UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UIViewController *newFrontController = nil;
                SWRevealViewController *revealController = [self revealViewController];
                NSLog(@"Share Bonus: %@", self.socialBonus);
                if ([self.socialBonus isEqualToString:@"0"]){
                    SharePoint *detailVC = [main    instantiateViewControllerWithIdentifier:@"sharePointViewController"];
                    newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
                    [revealController pushFrontViewController:newFrontController animated:YES];
                }else{
                    BonusShare *detailVC = [main    instantiateViewControllerWithIdentifier:@"shareBonusViewController"];
                    newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
                    [revealController pushFrontViewController:newFrontController animated:YES];
                }
                
                //[self performSegueWithIdentifier:@"sharePoint" sender:self];
                
                return;
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error.localizedDescription);
            [self killHUD];
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"No Connection!" message:@"It seems there is no internet connection. Please try again later." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
             [alertView show];
            return;
        }];
    }
}

- (IBAction)hideKeyBoard:(id)sender {
    [codeField resignFirstResponder];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    NSLog(@"Finished Typing");
   // [self.view endEditing:YES];
    [self.codeField resignFirstResponder];
    
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSLog(@"Hit Return");
    
    [codeField resignFirstResponder];
    //if([textField isEqual:self.codeField])
   // {
      //  [self scrollViewToCenterOfScreen:self.codeField];
        
    //}
      return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSLog(@"Start Typing");
    [self scrollViewToCenterOfScreen:self.codeField];
}
- (void)scrollViewToCenterOfScreen:(UIView *)theView
{
    CGFloat viewCenterY = theView.center.y;
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    
    CGFloat availableHeight = applicationFrame.size.height + 200;            // Remove area covered by keyboard
    
    CGFloat y = viewCenterY - availableHeight / 2.0;
    if (y < 0)
    {
        y = 0;
    }
    [self.scrowView setContentOffset:CGPointMake(0, y+30) animated:YES];
}
/*
#pragma mark - Segue Process Navigation
-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"sharePoint"]){
        SharePoint *eventShared = (SharePoint *)segue.destinationViewController;
        eventShared.eventDetail = eventDetail;
    }
}*/
#pragma mark - Misc
- (void)killHUD
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

@end
