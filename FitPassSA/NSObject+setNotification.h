//
//  NSObject+setNotification.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/31/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Event.h"
#import "AFNetworking.h"
@interface NSObject (setNotification)

- (NSString *)getNotified:(Event*)eventDetail userID:(NSString *)userID;
- (void)saveNotification:(NSString *)eventID userID:(NSString *)uID icalID:(NSString *)iCalEvent;
- (NSString *)removeNotified:(Event*)eventDetail userID:(NSString *)uID icalID:(NSString *)iCalEvent;
- (void)removeNotification:(NSString *)eventID userID:(NSString *)uID;
@end
