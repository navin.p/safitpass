//
//  Login.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/10/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import "Login.h"
#import <QuartzCore/QuartzCore.h>
#import "UITextField+TextFld.h"
#import "UIButton+BtnStyle.h"
#import "MBProgressHUD.h"
#import "AppSetting.h"
#import "SWRevealViewController.h"
#import "URBAlertView.h"
#import "AFNetworking.h"
#import "FitPassInfo.h"

#import "GAIDictionaryBuilder.h"
#import "GAI.h"
#import "GAIFields.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "Header.h"
@interface Login (){
    NSUserDefaults *userDefaults;
}
@end

@implementation Login

@synthesize emailAddressFd;
@synthesize passwordFd;


-(void)viewDidAppear:(BOOL)animated
{
    //GA Log
    NSString *str = @"Account Login";
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:str];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    tracker.allowIDFACollection = YES;

    
    if ([userDefaults objectForKey:kLoggedIn] != NULL) {
        //Pass Through
        UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SWRevealViewController *revealVC = [main instantiateViewControllerWithIdentifier:@"swreveal"];
        [self presentViewController:revealVC animated:YES completion:nil];
    } else {
        NSLog(@"Not Logged In");
    }
}


- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // We wire up the FBSDKLoginButton using the interface builder
        // but we could have also explicitly wired its delegate here.
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view, typically from a nib.
    [[self navigationController] setNavigationBarHidden:YES animated:YES];

    
    // Do any additional setup after loading the vie
    
    //** attach scroll view **/
    self.scrollView.delegate = self;
    [self.scrollView setScrollEnabled:YES];
    self.scrollView.frame = CGRectMake(0, 80, self.view.frame.size.width, self.scrollView.frame.size.height);
    [self.scrollView setContentSize:CGSizeMake(self.view.frame.size.width, 400)];
    self.contentView.frame = CGRectMake(0, 80, self.view.frame.size.width, self.contentView.frame.size.height);

    //add tap for keybord dismiss
    UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:viewTap];
    
    //change style of the fields
    [self.emailAddressFd configureTextFieldWithFrame:self.emailAddressFd.frame];
    [self.passwordFd configureTextFieldWithFrame:self.passwordFd.frame];
    
    //change placeholder of the fieldsw.
    UIColor *color = [UIColor whiteColor];
    self.emailAddressFd.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email Address" attributes:@{NSForegroundColorAttributeName: color}];
    self.passwordFd.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];
    
    //style button
    //[self.loginBtn configureBtn:self.loginBtn];
    
    //load data
    userDefaults = [[NSUserDefaults alloc] init];
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    [self.emailAddressFd setDelegate:self];
    [self.passwordFd setDelegate:self];
    
    self.emailAddressFd.text = [userDefaults objectForKey:kUsername];
    
    
    //facebook button
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(observeProfileChange:) name:FBSDKProfileDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(observeTokenChange:) name:FBSDKAccessTokenDidChangeNotification object:nil];
    
    self.fbLoginBtn.readPermissions = @[@"public_profile", @"email", @"user_friends"];
    FBSDKLoginButton *fbLoginBtn = [[FBSDKLoginButton alloc] init];
    if ([FBSDKAccessToken currentAccessToken]) {
        // User is logged in, do work such as go to next view controller.
        NSLog(@"Token created");
        [self observeProfileChange:nil];
    }
}
- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    
    // NSString *token = [[NSString alloc] initWithData:deviceToken encoding:NSUTF8StringEncoding];
    //NSLog(@"My token is: %@", deviceToken);
    NSString *tokenStr = [NSString stringWithFormat:@"%@", deviceToken];
    //NSLog(@"Token String %@", tokenStr);
    NSString *lessThan = [tokenStr stringByReplacingOccurrencesOfString:@"<" withString:@""];
    //NSLog(@"Less Than String %@", lessThan);
    NSString *greaterThan = [lessThan stringByReplacingOccurrencesOfString:@">" withString:@""];
    //NSLog(@"Greater Than String %@", greaterThan);
    NSString *token = [greaterThan stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSLog(@"My token is: %@", token);
    //store it to database
    //compile the URL
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    // manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *params =  [[NSDictionary alloc] initWithObjectsAndKeys:
                             token, @"token",
                              [userDefaults objectForKey:kUserID], @"uid",
                             @"iOS", @"platform",
                             nil] ;
    [manager POST:[NSString stringWithFormat:@"%@token.php",BaseURL] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error.localizedDescription);
        
    }];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
 
}
- (BOOL)application:(UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    [FBSDKLoginButton class];
    return YES;
}

- (void)fbLoginBtn:(FBSDKLoginButton *)fbLoginBtn didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error {
    if (error) {
        NSLog(@"Unexpected login error: %@", error);
        NSString *alertMessage = error.userInfo[FBSDKErrorLocalizedDescriptionKey] ?: @"There was a problem logging in. Please try again later.";
        NSString *alertTitle = error.userInfo[FBSDKErrorLocalizedTitleKey] ?: @"Oops";
        [[[UIAlertView alloc] initWithTitle:alertTitle
                                    message:alertMessage
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    } else {
        //if (_viewIsVisible) {
        //   [self performSegueWithIdentifier:@"showMain" sender:self];
        //}
        NSLog(@"Result: %@", result);
    }
}
- (void)observeProfileChange:(NSNotification *)notfication {
    if ([FBSDKProfile currentProfile]) {
        NSString *title = [NSString stringWithFormat:@"continue as %@", [FBSDKProfile currentProfile].name];
        
        
       //[self.continueButton setTitle:title forState:UIControlStateNormal];
    }
    //get user information
    if ([FBSDKAccessToken currentAccessToken]) {
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, first_name, email, last_name"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error) {
                 //NSLog(@"fetched user:%@", result);
                 NSString *fbemail = result[@"email"];
                 NSString *fbfname = [result[@"first_name"] stringByReplacingOccurrencesOfString:@" " withString:@"_"];
                 NSString *fblname = [result[@"last_name"] stringByReplacingOccurrencesOfString:@" " withString:@"_"];
                 NSLog(@"fetched user:%@", fblname);
                 NSLog(@"now sync data");
                 [userDefaults setObject:result[@"id"] forKey:kFBID];
                 [userDefaults synchronize];
                 [self checkUser:fbemail checkFname:fbfname checkLname:fblname];
             }
         }];
    }
}
- (void)observeTokenChange:(NSNotification *)notfication {
    if (![FBSDKAccessToken currentAccessToken]) {
        //  [self.continueButton setTitle:@"continue as a guest" forState:UIControlStateNormal];
    } else {
        [self observeProfileChange:nil];
    }
}


//get user id based on email
- (void)checkUser:(NSString*)fbemail checkFname:(NSString*)fbfname checkLname:(NSString *)fblname{
    NSLog(@"check emil: %@", fbemail);
    NSLog(@"check fbfname: %@", fbfname);
    NSLog(@"check fblname: %@", fblname);
    NSDictionary *params =  [[NSDictionary alloc] initWithObjectsAndKeys:
                             fbemail, @"email",
                             fbfname, @"fname",
                             fblname, @"lname",
                             nil] ;
    
    NSLog(@"Parameter %@", params);
    //compile the URL
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    //compose the url parameters
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    //PROCESS IT
    [manager POST:[NSString stringWithFormat:@"%@fblogin.php",BaseURL] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", [responseObject objectForKey:@"user"]);
        
        //is this successful or is it a duplicate entries
        NSDictionary *userDictionary = [responseObject objectForKey:@"user"];
            [userDefaults setObject:[userDictionary objectForKey:@"id"] forKey:kUserID];
            [userDefaults setObject:[userDictionary objectForKey:@"id"] forKey:kCurrentUser];
            [userDefaults setObject:[userDictionary objectForKey:@"fname"] forKey:kFirstName];
            [userDefaults setObject:[userDictionary objectForKey:@"lname"] forKey:kLastName];
            [userDefaults setObject:[userDictionary objectForKey:@"email"] forKey:kEmail];
            [userDefaults setObject:@"1" forKey:kLoggedIn];
            [userDefaults setObject:nil forKey:kEventCat];
            [userDefaults setObject:nil forKey:kEventDate];
            
            [userDefaults synchronize];
            
            UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SWRevealViewController *revealVC = [main instantiateViewControllerWithIdentifier:@"swreveal"];
            
            [self presentViewController:revealVC animated:YES completion:nil];
        
        return;
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error.localizedDescription);
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];
    
}

/*
 #pragma mark - Login
 */
- (IBAction)login:(id)sender {
    //confirm fields are in
    if ([emailAddressFd.text isEqualToString:@""] || [passwordFd.text isEqualToString:@""]) {
        
        URBAlertView *alertView = [[URBAlertView alloc] initWithTitle:@"Attention!" message:@"Please fill in all the fields!" cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alertView setHandlerBlock:^(NSInteger buttonIndex, URBAlertView *alertView) {
            [alertView hideWithCompletionBlock:^{
                
                if (buttonIndex == 0) {
                    [alertView hideWithAnimation:URBAlertAnimationFade];
                }
                
            }];
        }];
        [alertView show];
        return;
    }else{
        //show progress bar
        MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        progressHUD.labelText = @"Signing In";
        progressHUD.mode = MBProgressHUDAnimationFade;

        //compose the url parameters
        //compile the URL
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        //NSURL *url = [NSURL URLWithString:@"https://www.safitpass.com/json/login.php"];
        NSDictionary *params =  [[NSDictionary alloc] initWithObjectsAndKeys:
                                 emailAddressFd.text, @"username",
                                 passwordFd.text, @"password",
                                 nil] ;
        [manager POST:[NSString stringWithFormat:@"%@login.php",BaseURL]  parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            //is this successful or is it a duplicate entries
            NSString *dupUser = [NSString stringWithFormat:@"%@",[responseObject objectAtIndex:0]];
            NSString *userID = [NSString stringWithFormat:@"%@",[responseObject objectAtIndex:1]];
            if ([dupUser isEqualToString:@"1"]){
                // invalid information
                [self killHUD];
                URBAlertView *alertView = [[URBAlertView alloc] initWithTitle:@"Oops!" message:@"You must have entered something wrong. Please try again." cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                
                [alertView setHandlerBlock:^(NSInteger buttonIndex, URBAlertView *alertView) {
                    [alertView hideWithCompletionBlock:^{
                        
                        if (buttonIndex == 0) {
                            [alertView hideWithAnimation:URBAlertAnimationFade];
                        }
                        
                    }];
                }];
                [alertView show];
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            }else{
                //assign value to system default
                //[userDefaults setObject:@"1" forKey:kLoggedIn];
                [userDefaults setObject:userID forKey:kUserID];
                [userDefaults setObject:userID forKey:kCurrentUser];
                //[userDefaults setObject:strResult forKey:kUserData];
                [userDefaults synchronize];
                
                //switch view
                UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                SWRevealViewController *revealVC = [main instantiateViewControllerWithIdentifier:@"swreveal"];
                [self presentViewController:revealVC animated:YES completion:nil];
                
                
                
                return;
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error.localizedDescription);
            [self killHUD];
            URBAlertView *alertView = [[URBAlertView alloc] initWithTitle:@"No Connection!" message:@"It seems there is no internet connection. Please try again later." cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [alertView setHandlerBlock:^(NSInteger buttonIndex, URBAlertView *alertView) {
                [alertView hideWithCompletionBlock:^{
                    
                    if (buttonIndex == 0) {
                        [alertView hideWithAnimation:URBAlertAnimationFade];
                    }
                }];
            }];
            [alertView show];
            return;
        }];
    }
}

- (IBAction)needAcctAction:(id)sender {
    
    UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //assign instruction to new uiviewcontroller
    UIViewController *instructionViewController = [mainstoryboard instantiateViewControllerWithIdentifier:@"SignUpViewContorller"];
    [self presentViewController:instructionViewController animated:YES completion:nil];
    
}

 #pragma mark - textfield_functions
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if([textField isEqual:self.emailAddressFd])
    {
        [self.emailAddressFd becomeFirstResponder];
        [self scrollViewToCenterOfScreen:self.emailAddressFd];
        
    }
    if([textField isEqual:self.passwordFd])
    {
        
        [self.passwordFd becomeFirstResponder];
        [self scrollViewToCenterOfScreen:self.passwordFd];
        
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    [self.view endEditing:YES];
    [self.emailAddressFd resignFirstResponder];
    [self.passwordFd resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([textField isEqual:self.emailAddressFd])
    {
        [self.emailAddressFd becomeFirstResponder];
        [self scrollViewToCenterOfScreen:self.emailAddressFd];
        
    }
    if([textField isEqual:self.passwordFd])
    {
        
        [self.passwordFd resignFirstResponder];
        [self scrollViewToCenterOfScreen:self.passwordFd];
        
    }
    return YES;
}

- (void)scrollViewToCenterOfScreen:(UIView *)theView
{
    CGFloat viewCenterY = theView.center.y;
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    
    CGFloat availableHeight = applicationFrame.size.height - 200;            // Remove area covered by keyboard
    
    CGFloat y = viewCenterY - availableHeight / 2.0;
    if (y < 0)
    {
        y = 0;
    }
    [self.scrollView setContentOffset:CGPointMake(0, y+30) animated:YES];
}
/*
 #pragma mark - Misc
*/
- (void)killHUD
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)passwordReset:(id)sender {
    
    
    UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //assign instruction to new uiviewcontroller
    UIViewController *instructionViewController = [mainstoryboard instantiateViewControllerWithIdentifier:@"passwordViewController"];
    [self presentViewController:instructionViewController animated:YES completion:nil];
    
}
-(void)dismissKeyboard {
    [self.emailAddressFd resignFirstResponder];
    [self.passwordFd resignFirstResponder];
}

@end
