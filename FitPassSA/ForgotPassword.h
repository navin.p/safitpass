//
//  ForgotPassword.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 4/16/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPassword : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *emailField;
@property (strong, nonatomic) IBOutlet UIButton *loginBtn;
@property (strong, nonatomic) IBOutlet UIButton *needAcctBtn;
@property (strong, nonatomic) IBOutlet UIButton *resetBtn;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *viewTap;


@property (nonatomic, assign) BOOL slideOutAnimationEnabled;

- (IBAction)loginAction:(id)sender;
- (IBAction)signUpAction:(id)sender;
- (IBAction)resetAction:(id)sender;


@end
