//
//  LeftMenu.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/23/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import "LeftMenu.h"
#import "LeftMenuCell.h"
#import "UITextField+TextFld.h"
#import "AppSetting.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "MBProgressHUD.h"
#import "FitPassInfo.h"
#import "EventList.h"
#import "Sponsor.h"
#import "SAParknRec.h"
#import "Profile.h"
#import "FIPInfo.h"
#import "NotificationLists.h"
#import "ViewController.h"
#import "SWRevealViewController.h"
#import "GAIDictionaryBuilder.h"
#import "GAI.h"
#import "GAIFields.h"
#import "EventSearch.h"
#import "MyAccount.h"

@interface LeftMenu (){
    NSUserDefaults *userDefaults;
}

@end

@implementation LeftMenu

#pragma mark - UIViewController Methods -

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self.slideOutAnimationEnabled = YES;
    
  
    return [super initWithCoder:aDecoder];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
     [self killHUD];
    //GA Log
    NSString *str = @"Side Menu";
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:str];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    tracker.allowIDFACollection = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //sync user data
    userDefaults = [[NSUserDefaults alloc] init];
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    // Do any additional setup after loading the view.
    self.tableView.separatorColor = [UIColor whiteColor];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    //style search field
    UIColor *color = [UIColor whiteColor];
    self.searchFld.layer.cornerRadius = 5.0f;
    [self.searchFld.layer setBorderColor: [[UIColor clearColor] CGColor]];
    [self.searchFld.layer setBorderWidth: 1.0f];
    self.searchFld.layer.masksToBounds = YES;
    self.searchFld.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search Event or Activity" attributes:@{NSForegroundColorAttributeName: color}];
    
    [self.searchFld setLeftViewMode:UITextFieldViewModeAlways];
    self.searchFld.leftView= [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"telescope"]];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 6;
    //return 8;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 20)];
   view.backgroundColor = [UIColor whiteColor];
    return view;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   // UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"leftMenuCell"];
   
    LeftMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    switch (indexPath.row)
    {
        case 0:
        cell.leftMenuLabel.text = @"Home";
        cell.leftMenuIcon.image = [UIImage imageNamed:@"homeicon"];
        cell.leftMenuArrow.image = [UIImage imageNamed:@"right_arrow-white"];
        break;
//        case 1:
//            cell.leftMenuLabel.text = @"Play Fit Pass";
//            cell.leftMenuIcon.image = [UIImage imageNamed:@"playFitPass"];
//            cell.leftMenuArrow.image = [UIImage imageNamed:@"right_arrow-white"];
//            break;
        case 1:
            cell.leftMenuLabel.text = @"Fitness In The Park";
            cell.leftMenuIcon.image = [UIImage imageNamed:@"fip-icon"];
            cell.leftMenuArrow.image = [UIImage imageNamed:@"right_arrow-white"];
            break;
        case 2:
            cell.leftMenuLabel.text = @"Notifications";
            cell.leftMenuIcon.image = [UIImage imageNamed:@"bell"];
            cell.leftMenuArrow.image = [UIImage imageNamed:@"right_arrow-white"];
            break;
//        case 3:
//            cell.leftMenuLabel.text = @"Account Profiles";
//            cell.leftMenuIcon.image = [UIImage imageNamed:@"profile"];
//            cell.leftMenuArrow.image = [UIImage imageNamed:@"right_arrow-white"];
//            break;
        case 3:
            cell.leftMenuLabel.text = @"Partners";
            cell.leftMenuIcon.image = [UIImage imageNamed:@"supporter"];
            cell.leftMenuArrow.image = [UIImage imageNamed:@"right_arrow-white"];
            break;
        case 4:
            cell.leftMenuLabel.text = @"San Antonio Parks & Rec.";
            cell.leftMenuIcon.image = [UIImage imageNamed:@"sapr_logosmall"];
            cell.leftMenuArrow.image = [UIImage imageNamed:@"right_arrow-white"];
            break;
        case 5:
            cell.leftMenuLabel.text = @"Log Out";
            cell.leftMenuIcon.hidden = YES;
            cell.leftMenuArrow.image = [UIImage imageNamed:@"logout_arrow"];
            cell.separatorInset =  UIEdgeInsetsMake(0.f, cell.bounds.size.width, 0.f, 0.f);
            NSLog(@"%f", cell.leftMenuLabel.frame.origin.x);
            [cell.leftMenuLabel setFrame:CGRectMake(15.0f, cell.leftMenuLabel.frame.origin.y, cell.leftMenuLabel.frame.size.width, cell.leftMenuLabel.frame.size.height)];
            
            break;
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    SWRevealViewController *revealController = [self revealViewController];
    if (indexPath.row == 0) {
        
        UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SWRevealViewController *revealVC = [main instantiateViewControllerWithIdentifier:@"swreveal"];
        [self presentViewController:revealVC animated:NO completion:nil];
        
    }
  /* else if (indexPath.row ==1) {
        
        UIViewController *newFrontController = nil;
        FitPassInfo *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"eventListViewController"];
        newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
        
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        [revealController pushFrontViewController:newFrontController animated:YES];

        
    }*/
   else if (indexPath.row == 1) {
       UIViewController *newFrontController = nil;
       FIPInfo *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"fipInfoViewController"];
       newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
       [revealController pushFrontViewController:newFrontController animated:YES];
       [self.tableView deselectRowAtIndexPath:indexPath animated:YES];

   
        
    }else if (indexPath.row == 2) {
        
        UIViewController *newFrontController = nil;
        NotificationLists *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"NotificationViewController"];
        newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
        
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        [revealController pushFrontViewController:newFrontController animated:YES];
        
    }
    
//    else if (indexPath.row == 3) {
//        UIViewController *newFrontController = nil;
//        MyAccount *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"accountViewController"];
//        newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
//
//        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
//
//        [revealController pushFrontViewController:newFrontController animated:YES];
//
//    }
    
    else if (indexPath.row == 3) {
        UIViewController *newFrontController = nil;
        Sponsor *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"SponsorViewController"];
      
        newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
            
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
            
        [revealController pushFrontViewController:newFrontController animated:YES];
    }else if (indexPath.row == 4) {
        
        UIViewController *newFrontController = nil;
        SAParknRec *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"saparkViewController"];
        newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
        
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        [revealController pushFrontViewController:newFrontController animated:YES];
   
    }else if (indexPath.row == 5) {
        
        //UIViewController *newFrontController = nil;
        if ([FBSDKAccessToken currentAccessToken]) {
            FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
            [login logOut];
        }
            
        //app logout
        [userDefaults setObject:NULL forKey:kLoggedIn];
        [userDefaults setObject:NULL forKey:kUserID];
        [userDefaults setObject:NULL forKey:kEventCat];
        [userDefaults setObject:NULL forKey:kUserPoints];
        [[NSUserDefaults standardUserDefaults] setPersistentDomain:[NSDictionary dictionary] forName:[[NSBundle mainBundle] bundleIdentifier]];

        [userDefaults synchronize];
           
        [self.loginManager logOut];
        
        ViewController *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"loginViewController"];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:detailVC];
        
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        [self presentViewController:nav animated:YES completion:nil];
        
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == self.searchFld) {
        NSLog(@"form submitted");
        //Your submit code goes here
        return NO;
    }
    
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)search:(id)sender {
    
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    SWRevealViewController *revealController = [self revealViewController];
    
    UIViewController *newFrontController = nil;
    EventSearch *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"searchEventViewController"];
    newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    
    
    [revealController pushFrontViewController:newFrontController animated:YES];

}
- (void)killHUD
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}
@end
