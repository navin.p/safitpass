//
//  EventLists.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/19/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import "EventLists.h"
#import "Event.h"
#import "EventCellView.h"
#import "AppSetting.h"
#import "CoreLocation/CLLocationManagerDelegate.h"
#import "MBProgressHUD.h"
#import "RMDateSelectionViewController.h"
#import "EventDetail.h"
#import "DXPopover.h"
#import "SWRevealViewController.h"
#import "GAIDictionaryBuilder.h"
#import "GAI.h"
#import "GAIFields.h"
#import "FitPassInfo.h"
#import "Profile.h"
#import "SwitchAcct.h"
#import "Header.h"
static NSString *UYLCellIdentifier = @"Cell";

@interface EventLists (){
    NSString *dateSelected;
    NSString *eventCat;
    NSUserDefaults *userDefaults;
    BOOL sidebarMenuOpen;
}

@property (nonatomic) BOOL useCustomCells;
@property (nonatomic, weak) UIRefreshControl *refreshControl;
@property (nonatomic, strong) NSArray *configs;

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)
#define IS_IOS8_OR_ABOVE                            (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))

@end

@implementation EventLists
@synthesize noEvent;
@synthesize tableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated{
    //load current user
    NSString *currentUser = [userDefaults objectForKey:kCurrentUser];
    [self loadCurrentUser:currentUser];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    
    //GA Log
    NSString *str = @"Fit Pass Event List";
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:str];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    tracker.allowIDFACollection = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTableWithNotification:) name:@"RefreshTable" object:nil];
    
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
      // Updated by Navin
//     UIColor *navColor =  [UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:1.0];
//    self.navigationController.navigationBar.barTintColor = navColor;
//    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    userDefaults = [[NSUserDefaults alloc] init];
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString *userID = [userDefaults objectForKey:kUserID];
    if ([userDefaults objectForKey:kCurrentUser] == nil){
        [userDefaults setObject:userID forKey:kCurrentUser];
        [userDefaults synchronize];
    }
    [userDefaults setObject:@"FitPass" forKey:kCalendar];
    [userDefaults setObject:nil forKey:kEventCat];
    [userDefaults synchronize];
    //load current user
    NSString *currentUser = [userDefaults objectForKey:kCurrentUser];
    [self loadCurrentUser:currentUser];
    
    self.edgesForExtendedLayout=UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars=NO;
    self.automaticallyAdjustsScrollViewInsets=NO;
    
    [self createMenu];
    /*UITapGestureRecognizer *viewTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewTapped:)];
    [self.view addGestureRecognizer:viewTap];
    [viewTap setCancelsTouchesInView:NO];*/
    
    //default hide everything
    tableView.hidden = YES;
    noEvent.hidden = YES;
    
    //define a saved selected IndexPath so we can pass the index between swipe button and direct row press
    self.savedSelectedIndexPath = nil;
    
    //get current location
    self.locationManager = [[CLLocationManager alloc] init];
    [self.locationManager setDelegate:self];
    self.locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
    [self.locationManager startUpdatingLocation];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        [self.locationManager requestAlwaysAuthorization];
    }
    //CLLocation *currentLoc = self.locationManager.location;
    
    //get the category
    eventCat = [userDefaults objectForKey:kEventCat];
    dateSelected = [userDefaults objectForKey:kEventDate];
    
    //store the current date selected;
    if (dateSelected == nil){
        dateSelected = self.currentDate;
        [_dateSelection setTitle:@"Today" forState:UIControlStateNormal];
    }
    
    [self loadEvents:eventCat sortEvent:nil searchDate:dateSelected];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didChangePreferredContentSize:)
                                                 name:UIContentSizeCategoryDidChangeNotification object:nil];
    self.tableView.estimatedRowHeight = 100.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;

    //add swipe gesture to navigate between dates
   
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeleft];
    
    UISwipeGestureRecognizer * swiperight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
    swiperight.direction=UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swiperight];
    
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIContentSizeCategoryDidChangeNotification
                                                  object:nil];
}

- (void)didChangePreferredContentSize:(NSNotification *)notification
{
    [self.tableView reloadData];
}
- (void)refreshTableWithNotification:(NSNotification *)notification
{
    eventCat = [userDefaults objectForKey:kEventCat];
    [self loadEvents:[userDefaults objectForKey:kEventCat] sortEvent:nil searchDate:dateSelected];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadCurrentUser:(NSString*)uid{
    //then get the data
    NSString *url = [NSString stringWithFormat: @"%@userBrief.php?uid=%@",BaseURL, uid];
    NSLog(@"User URL: %@", url);
    NSURL *eventURL = [NSURL URLWithString: url];
    NSData *jsonData = [NSData dataWithContentsOfURL: eventURL];
    NSError *error = nil;
    NSDictionary *userInfo = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error: &error];
    
    NSLog(@"%@", userInfo);
    self.currentUserName.text = [userInfo objectForKey:@"full_name"];
    NSString *userphoto = [NSString stringWithFormat: @"%@users/%@",BaseURLFOR_IMAGE, [userInfo objectForKey:@"photo"]];
    NSURL *photoURL = [NSURL URLWithString:userphoto];
    NSData *imageData = [NSData dataWithContentsOfURL:photoURL];
    self.currentUserPhoto.image = [UIImage imageWithData:imageData];
    
    self.currentUserPhoto.layer.cornerRadius = self.currentUserPhoto.frame.size.width / 2;
    self.currentUserPhoto.clipsToBounds = YES;
    self.currentUserPhoto.layer.borderWidth = 3.0f;
    self.currentUserPhoto.layer.borderColor = [UIColor whiteColor].CGColor;
    
    [self loadEvents:eventCat sortEvent:nil searchDate:dateSelected];
    
}
#pragma mark - Event Gathering
//load events
- (void)loadEvents:(NSString*)searchType sortEvent:(NSString*)sortType searchDate:(NSString *)dateSelect{
    
    //convert date format
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM.dd.yy"];
    NSDate *date = [dateFormatter dateFromString:dateSelected];
    [dateFormatter setDateFormat:@"EEEE, MMMM dd"];
    NSString *newDateString = [dateFormatter stringFromDate:date];
    
    //update the date display on the button
    if ([self.currentDate isEqualToString: dateSelected]){
        [_dateSelection setTitle:@"Today" forState:UIControlStateNormal];
    }else{
        [_dateSelection setTitle:newDateString forState:UIControlStateNormal];
    }
    
    NSString *userID = [userDefaults objectForKey:kCurrentUser];
    NSLog(@"%@", userID);
    if ([userID isEqualToString:@""]){
        NSString *userID = [userDefaults objectForKey:kUserID];
    }
  //
    /*
    //change navigation title
    if ([eventCat isEqualToString:@"1"]){
        self.navigationController.topViewController.title = @"Fitness";
    }else if ([eventCat isEqualToString:@"2"]){
        self.navigationController.topViewController.title = @"Nutrition";
    }else if ([eventCat isEqualToString:@"3"]){
        self.navigationController.topViewController.title = @"Wellness";
    }else if ([eventCat isEqualToString:@"4"]){
        self.navigationController.topViewController.title = @"Physical Activity";
    }else if ([eventCat isEqualToString:@"5"]){
        self.navigationController.topViewController.title = @"Fitness in Park";
    }else if ([eventCat isEqualToString:@"6"]){
        self.navigationController.topViewController.title = @"Featured";
    }else{
        self.navigationController.topViewController.title = @"All Events";
    }*/
    
     //change navigation title
     if ([eventCat isEqualToString:@"1"]){
         self.navigationController.topViewController.title = @"Featured";
     }else if ([eventCat isEqualToString:@"2"]){
         self.navigationController.topViewController.title = @"Fitness";
     }else if ([eventCat isEqualToString:@"3"]){
         self.navigationController.topViewController.title = @"Wellness/Nutrition";
     }else if ([eventCat isEqualToString:@"4"]){
         self.navigationController.topViewController.title = @"Volunteering";
     }else{
         self.navigationController.topViewController.title = @"All Events";
     }
    //show progress bar
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    progressHUD.labelText = @"Updating...";
    progressHUD.mode = MBProgressHUDAnimationFade;
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        //construct URL
        NSString *url = [NSString stringWithFormat: @"%@eventList.php?cat=%@&sortby=%@&date=%@&lat=%f&long=%f&uID=%@",BaseURL, searchType, sortType, dateSelected, self.locationManager.location.coordinate.latitude, self.locationManager.location.coordinate.longitude, userID];
        NSURL *eventURL = [NSURL URLWithString: url];
        NSLog(@"%@", eventURL);
        NSData *jsonData = [NSData dataWithContentsOfURL: eventURL];
        NSError *error = nil;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error: &error];
        self.eventList = [NSMutableArray array];
        NSArray *eventArray = [dataDictionary objectForKey:@"fitpass"];
        // NSString *eventFound = [dataDictionary objectForKey:@"notfound"];
        //if nothing is found
        if ([dataDictionary objectForKey:@"notfound"]){
            tableView.hidden = YES;
            noEvent.hidden = NO;
        }else{
            noEvent.hidden = YES;
            tableView.hidden = NO;
            for(NSDictionary *eventDictionary in eventArray){
                Event *event = [Event blogPostWithTitle:[eventDictionary objectForKey:@"name"]];
                event.eventID = [eventDictionary objectForKey:@"ID"];
                event.eventTitle = [eventDictionary objectForKey:@"title"];
                event.startDate = [eventDictionary objectForKey:@"sdate"];
                event.startTime = [eventDictionary objectForKey:@"stime"];
                event.endTime = [eventDictionary objectForKey:@"etime"];
                event.ckTime = [eventDictionary objectForKey:@"ctime"];
                event.eventDescription = [eventDictionary objectForKey:@"description"];
                event.points = [eventDictionary objectForKey:@"point"];
                event.fitLevel = [eventDictionary objectForKey:@"fitLevel"];
                event.eventPhoto = [eventDictionary objectForKey:@"photo"];
                event.categories = [eventDictionary objectForKey:@"category"];
                event.locationInfo = [eventDictionary objectForKey:@"location"];
                event.address = [eventDictionary objectForKey:@"address"];
                event.eventTime = [eventDictionary objectForKey:@"event_time"];
                event.loclat = [eventDictionary objectForKey:@"loc_lat"];
                event.loclong = [eventDictionary objectForKey:@"loc_long"];
                event.locName = [eventDictionary objectForKey:@"loc_name"];
                event.iCalID = [eventDictionary objectForKey:@"iCalID"];
                event.userPt = [eventDictionary objectForKey:@"user_pt"];
                event.badge = [eventDictionary objectForKey:@"pillar"];
                
                [self.eventList addObject:event];
                [self.tableView reloadData];
            }
        }
        [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    });
}


#pragma mark - GeoLocation
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError");
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch(status) {
        case kCLAuthorizationStatusNotDetermined:
            // User has not yet made a choice with regards to this application
            NSLog(@"Not Determined");
            break;
        case kCLAuthorizationStatusRestricted:
            // This application is not authorized to use location services.  Due
            // to active restrictions on location services, the user cannot change
            // this status, and may not have personally denied authorization
            NSLog(@"Restricted");
            break;
        case kCLAuthorizationStatusDenied:
            // User has explicitly denied authorization for this application, or
            // location services are disabled in Settings
            NSLog(@"Denied");
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            NSLog(@"Allowed to location Access 1");
            //[self loadEvents:nil sortEvent:nil limitEvent:nil];
            break;
        default:
            NSLog(@"Allowed to location Access 2");
            //[self loadEvents:nil sortEvent:nil limitEvent:nil];
    }
 //   NSLog(@"Change in authorization status%@");
}


#pragma mark - Table View Setup
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
   if (self.eventList){
         return [self.eventList count];
    }else{
        return 1;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    EventCellView *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
  
    if (self.eventList){
        //use custom class
        Event *event = [self.eventList objectAtIndex:indexPath.row];
        cell.event_points.text = event.points;
        cell.event_address.text = [NSString stringWithFormat:@"%@, %@", event.locName, event.address];
        
        cell.event_title.text = event.eventTitle;
        cell.event_time.text = event.eventTime;
        
        cell.event_address.numberOfLines = 0;
        [cell.event_address sizeToFit];
        
        cell.event_time.numberOfLines = 0;
        [cell.event_time sizeToFit];
        
        NSLog(@"%@", event.badge);
        if ([event.badge isEqualToString:@""]){
           cell.badgeIcon.hidden = YES;
            
        }else if ([event.badge isEqualToString:@"1"]){
            cell.badgeIcon.hidden = NO;
            cell.badgeIcon.image = [UIImage imageNamed:@"badge_featured_circle"];
            
        }else if ([event.badge isEqualToString:@"2"]){
            cell.badgeIcon.hidden = NO;
             cell.badgeIcon.image = [UIImage imageNamed:@"badge_nutrition_circle"];
            
        }else if ([event.badge isEqualToString:@"3"]){
            cell.badgeIcon.hidden = NO;
            cell.badgeIcon.image = [UIImage imageNamed:@"badge_fitness_circle"];
            
        }else if ([event.badge isEqualToString:@"4"]){
            cell.badgeIcon.hidden = NO;
            cell.badgeIcon.image = [UIImage imageNamed:@"badge_volunteer_circle"];
            
        }else{
            cell.badgeIcon.hidden = YES;
        }
        
       // cell.functionBtnView.hidden = YES;
        //get image
        //cell.imageHeight.constant = 50;
        if ([event.eventPhoto isEqualToString:@""]){
            cell.event_photo.hidden = YES;
            cell.imgHeight.constant = 0;
        }else{
            NSData *imageData = [NSData dataWithContentsOfURL:event.thumbnailURL];
            cell.event_photo.image = [UIImage imageWithData:imageData];
            cell.event_photo.hidden = NO;
            cell.imgHeight.constant = 150;
            NSLog(@"%@", event.eventPhoto);
        }
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }
    //cell.hospital_address.text = [NSString stringWithFormat:@"%@",hospital.address];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    Event *event2 = [self.eventList objectAtIndex:indexPath.row];
    
    if ([event2.eventPhoto isEqualToString:@""]){
        return 200.0f;
    }else{
        return 380.0f;
    }
}

//on direct press, trigger segue
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  /*  MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    progressHUD.labelText = @"Loading...";
    progressHUD.mode = MBProgressHUDAnimationFade;
   */

    self.savedSelectedIndexPath = [self.tableView indexPathForSelectedRow];
    
    NSLog(@"Saved Path %@", _savedSelectedIndexPath);
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
   //[self performSegueWithIdentifier:@"showDetail" sender:self];
}

#pragma mark - Segue Process Navigation
-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    
    if ([segue.identifier isEqualToString:@"showDetail"]){
        
        [self killHUD];
        
        self.savedSelectedIndexPath = [self.tableView indexPathForSelectedRow];
        
        NSLog(@"SavedPath %@", _savedSelectedIndexPath);
        //self.savedSelectedIndexPath = [self.tableView indexPathForSelectedRow];
        NSIndexPath *indexPath =  self.savedSelectedIndexPath; //get the index path of the row selected
        
        Event *event = [self.eventList objectAtIndex:indexPath.row]; // now get the content of that row
        EventDetail *eventDetailView = (EventDetail *)segue.destinationViewController;
        
        eventDetailView.eventDetail = event;
       
    }
}


- (void)killHUD
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}
 #pragma mark - Sort By Time, Location or Point


- (IBAction)swipeRightAction:(id)sender {
}

- (IBAction)sortByPt:(id)sender {
    [self loadEvents:eventCat sortEvent:@"point" searchDate:nil];
    [self.byPoints setTitleColor:[UIColor colorWithRed:4/255.0f green:121/255.0f blue:144/255.0f alpha:1.0] forState:UIControlStateNormal];
    [self.byLocation setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.byTime setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
}

- (IBAction)sortByLoc:(id)sender {
    [self loadEvents:eventCat sortEvent:@"location" searchDate:nil];
    [self.byLocation setTitleColor:[UIColor colorWithRed:4/255.0f green:121/255.0f blue:144/255.0f alpha:1.0] forState:UIControlStateNormal];
    [self.byPoints setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.byTime setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
}

- (IBAction)sortByTime:(id)sender {
    [self loadEvents:eventCat sortEvent:@"time" searchDate:nil];
    [self.byTime setTitleColor:[UIColor colorWithRed:4/255.0f green:121/255.0f blue:144/255.0f alpha:1.0] forState:UIControlStateNormal];
    [self.byLocation setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.byPoints setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
}

#pragma mark - Date Creation and Filtering
- (IBAction)viewNextDay:(id)sender {
    
  // self.currentDate = self.nextDate;
    dateSelected = self.nextDate;
    
    [userDefaults setObject:dateSelected forKey:kEventDate];
    [userDefaults synchronize];
    [self loadEvents:eventCat sortEvent:@"time" searchDate:dateSelected];
}

- (IBAction)viewPrevDay:(id)sender {
    
    dateSelected = self.previousDate;
    [userDefaults setObject:dateSelected forKey:kEventDate];
    [userDefaults synchronize];
    [self loadEvents:eventCat sortEvent:@"time" searchDate:dateSelected];
}


#pragma mark - Declaring date variables
- (NSString *) currentDate{
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // display in 12HR/24HR (i.e. 11:25PM or 23:25) format according to User Settings
    [dateFormatter setDateFormat:@"MM.dd.yy"];
    //[dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    NSString *currentTime = [dateFormatter stringFromDate:today];
    
    return currentTime;
}
- (NSString *) nextDate{
    //convert current date from NSString to NSDate
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM.dd.yy"];
    //create a date variable
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateFormatter dateFromString:dateSelected];
    //set calendar
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    //date difference
    components.day = 1;
    //adjust teh date difference
    NSDate *newDate = [calendar dateByAddingComponents:components toDate:dateFromString options:0];
    NSString *nextDay = [dateFormatter stringFromDate:newDate];
    NSLog(@"%@", nextDay);
    return nextDay;
}
//this is pretty much the same as next date but change date difference to minus 1
- (NSString *) previousDate{
    //convert current date from NSString to NSDate
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM.dd.yy"];
    //create a date variable
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateFormatter dateFromString:dateSelected];
    //set calendar
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    //date difference
    components.day = -1;
    //adjust teh date difference
    NSDate *newDate = [calendar dateByAddingComponents:components toDate:dateFromString options:0];
    NSString *prevDay = [dateFormatter stringFromDate:newDate];
    return prevDay;
}

#pragma mark - RMDateSelectionViewController Delegates
/*
- (void)dateSelectionViewController:(RMDateSelectionViewController *)vc didSelectDate:(NSDate *)aDate {
    //Do something
    [vc dismiss];
    NSLog(@"123");
    //convert current date from NSString to NSDate
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM.dd.yy"];
    dateSelected = [dateFormatter stringFromDate:aDate];
    
    [userDefaults setObject:dateSelected forKey:kEventDate];
    [userDefaults synchronize];
   [self loadEvents:eventCat sortEvent:@"time" searchDate:dateSelected];
}
*/
- (void)dateSelectionViewControllerDidCancel:(RMDateSelectionViewController *)vc {
    //Do something else
}
- (IBAction)pickDate:(id)sender {
    RMAction<UIDatePicker *> *selectAction = [RMAction<UIDatePicker *> actionWithTitle:@"Select" style:RMActionStyleDone andHandler:^(RMActionController<UIDatePicker *> *controller) {
       
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MM.dd.yy"];
        dateSelected = [dateFormatter stringFromDate: controller.contentView.date];
        [userDefaults setObject:dateSelected forKey:kEventDate];
        [userDefaults synchronize];
        NSLog(@"Successfully selected date: %@", dateSelected);
        [self loadEvents:eventCat sortEvent:@"time" searchDate:dateSelected];
        
    }];
    
    RMAction<UIDatePicker *> *cancelAction = [RMAction<UIDatePicker *> actionWithTitle:@"Cancel" style:RMActionStyleCancel andHandler:^(RMActionController<UIDatePicker *> *controller) {
        NSLog(@"Date selection was canceled");
    }];
    
    RMDateSelectionViewController *dateSelectionController = [RMDateSelectionViewController actionControllerWithStyle:RMActionControllerStyleWhite title:@"Select Date" message:nil selectAction:selectAction andCancelAction:cancelAction];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM.dd.YYYY"];
    NSDate *dateFromString = [[NSDate alloc] init];
    //create a date variable
    dateFromString = [dateFormatter dateFromString:dateSelected];
    dateSelectionController.datePicker.datePickerMode = UIDatePickerModeDate;
    dateSelectionController.datePicker.date = dateFromString;
    [self presentViewController:dateSelectionController animated:YES completion:nil];

}
#pragma mark - Swipe for date navigation -
-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
    //Do what you want here
    // self.currentDate = self.nextDate;
    NSLog(@"%@abc", self.nextDate);
    dateSelected = self.nextDate;
    [self loadEvents:eventCat sortEvent:@"time" searchDate:dateSelected];
}

-(void)swiperight:(UISwipeGestureRecognizer*)gestureRecognizer
{
    NSLog(@"%@abc", self.previousDate);
    dateSelected = self.previousDate;
    [self loadEvents:eventCat sortEvent:@"time" searchDate:dateSelected];
}


#pragma mark - SlideNavigationController Methods -
- (void)createMenu{
    
    //create menu
    SWRevealViewController *revealController = [self revealViewController];
    //create the left menu button
    UIButton *button  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [button setImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
    [button addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
    //create the right menu button
    UIButton *button_right  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [button_right setImage:[UIImage imageNamed:@"filter"] forState:UIControlStateNormal];
    [button_right addTarget:revealController action:@selector(rightRevealToggle:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button_right];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    
    
    UITapGestureRecognizer *viewTapped=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewTapped:)];
    [self.view addGestureRecognizer:viewTapped];
    viewTapped.delegate = self;
    [viewTapped setCancelsTouchesInView:NO];
    
    self.revealViewController.delegate = self;
    [revealController.frontViewController.revealViewController tapGestureRecognizer];
    
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
       shouldReceiveTouch:(UITouch *)touch
{
    if([touch.view class] == self.tableView.class || [touch.view class] ==[UITableViewCell class]){
        return NO;
    }
    return YES;
}
-(void)viewTapped:(UITapGestureRecognizer*)gestureRecognizer
{
    
    //Do what you want here
    SWRevealViewController *revealController = self.revealViewController;
    if (revealController.frontViewPosition == FrontViewPositionRight) {
        [revealController revealToggleAnimated:YES];
    }
    
}
-(void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position {
    if(position == FrontViewPositionLeft) {
        self.view.userInteractionEnabled = YES;
    } else {
        self.view.userInteractionEnabled = NO;
    }
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position {
    if(position == FrontViewPositionLeft) {
        self.view.userInteractionEnabled = YES;
    } else {
        self.view.userInteractionEnabled = NO;
    }
}



#pragma mark - Tab Bar
-(void)tabBarController:(UITabBarController *)tabBar didSelectViewController:(UIViewController *)viewController
{
    NSLog(@"Selected index: %lu", (unsigned long)tabBar.selectedIndex);
}
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    SWRevealViewController *revealController = [self revealViewController];
    if(item.tag == 1){
        UIViewController *newFrontController = nil;
        Profile *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
        newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
        [revealController pushFrontViewController:newFrontController animated:YES];
    }else if (item.tag == 0){
        UIViewController *newFrontController = nil;
        FitPassInfo *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"fitpassInfoViewController"];
        newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
        [revealController pushFrontViewController:newFrontController animated:YES];
    }else if (item.tag == 3){
       
        UIViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"SwitchAcctViewController"];
        [self presentViewController:vc animated:YES completion:nil];
        
    }
    
   // NSLog(@"Selected index: %lu", item.tag);
}

@end
