//
//  Header.h
//  FitPassSA
//
//  Created by Navin Patidar on 5/8/18.
//  Copyright © 2018 Ping-jung Tsai. All rights reserved.
//

#ifndef Header_h
#define Header_h

//#define BaseURL @"https://www.safitpass.com/json/"
//#define BaseURL1 @"http://www.safitpass.com/siteInclude/"
//#define BaseURLFOR_IMAGE @"https://www.safitpass.com/images/"
// Staging
//#define BaseURL @"http://www.safitpass.com/staging/json/"
//#define BaseURL1 @"http://www.safitpass.com/staging/siteInclude/"
//#define BaseURLFOR_IMAGE @"http://www.safitpass.com/staging/images/"
//Live
#define BaseURL @"http://www.safitpass.com/json/"
#define BaseURL1 @"http://www.safitpass.com/siteInclude/"
#define BaseURLFOR_IMAGE @"http://www.safitpass.com/images/"
#endif /* Header_h */
