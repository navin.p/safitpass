//
//  AddFamily.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 5/8/17.
//  Copyright © 2017 Ping-jung Tsai. All rights reserved.
//

#import "AddFamily.h"

#import <UIKit/UIKit.h>
#import "SignUp.h"
#import "MyAccount.h"
#import <QuartzCore/QuartzCore.h>
#import "UITextField+TextFld.h"
#import "UIButton+BtnStyle.h"
#import "DropDownListView.h"
#import "MBProgressHUD.h"
#import "CommonCrypto.h"
#import "AFNetworking.h"
#import "URBAlertView.h"
#import "AppSetting.h"
#import "SWRevealViewController.h"
#import "GAIDictionaryBuilder.h"
#import "GAI.h"
#import "GAIFields.h"
#import "Header.h"
#define kOFFSET_FOR_KEYBOARD 80.0

@interface AddFamily (){
    NSArray *genderArray;
    NSArray *booleanArray;
    NSArray *activityArray;
    NSUserDefaults *userDefaults;
    NSString *memberID;
}

@end

@implementation AddFamily

@synthesize scrollView;
@synthesize contentView;
@synthesize lblSelectedNames;
@synthesize firstName;
@synthesize lastName;
@synthesize weight;
@synthesize age;
@synthesize fitLevel;
@synthesize gender;
@synthesize zipCode;
@synthesize teamName;
@synthesize userInfo;

-(void)viewDidAppear:(BOOL)animated
{
    //GA Log
    NSString *str = @"Family Member Management";
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:str];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    tracker.allowIDFACollection = YES;
    
    
    //add logo to navigation bar
    /*UIImageView *image=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,105,36)] ;
    //set your image logo replace to the main-logo
    [image setImage:[UIImage imageNamed:@"fp-newlogo"]];
    [self.navigationController.navigationBar.topItem setTitleView:image];*/
 
    // Updated by Navin
    //  UIColor *navColor =  [UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:1.0];
   // self.navigationController.navigationBar.barTintColor = navColor;
   // self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    
   
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    userDefaults = [[NSUserDefaults alloc] init];
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    [self formatFields];
    
    //set picker options
    genderArray = @[@"Male", @"Female"];
    activityArray = @[@"Light", @"Moderate", @"Advanced"];
    
    self.gender.delegate = self;
    self.fitLevel.delegate = self;
    
    //show progress bar
    /**[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self loadData];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });*/
    
    //if this is an update, load data
    NSLog(@"Member ID: %@", memberID);
    memberID = userInfo.userID;
    if (memberID){
        [self loadProfile:memberID];
        NSLog(@"Entries Update");
        self.navigationController.topViewController.title = @"Profile Update";
    }else{
        NSLog(@"New Entries");
        self.addPhotoLabel.hidden = YES;
        self.addPictureBtn.hidden = YES;
        self.profileImg.hidden = YES;
        self.blurImage.hidden = YES;
        self.profileImgHeight.constant = 0;
        
        self.navigationController.topViewController.title = @"Add Family Member";
        
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"< Back" style: UIBarButtonItemStylePlain target:self action:@selector(BackClicked)];
        self.navigationItem.leftBarButtonItem = backButton;
    }
}

#pragma mark - Format Text Fields

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)formatFields{
    [self roundUIiew:self.nameBox];
    [self roundUIiew:self.lnameBox];
    [self roundUIiew:self.weightBox];
    [self roundUIiew:self.ageBox];
    [self roundUIiew:self.genderBox];
    [self roundUIiew:self.fitlevelBox];
    [self roundUIiew:self.zipBox];
    [self roundUIiew:self.teamBox];
    //style button
   // [self.registerBtn configureBtn:self.registerBtn];
    
}
- (void)roundUIiew:(UIView*)layerBox {
    layerBox.layer.cornerRadius = 5;
    layerBox.layer.masksToBounds = YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
//load profile
- (void)loadProfile:(NSString*)memberID{
    //then get the data
    NSString *url = [NSString stringWithFormat:@"%@userData.php?uid=%@",BaseURL,memberID];
    NSURL *eventURL = [NSURL URLWithString: url];
    NSLog(@"%@", url);
    NSData *jsonData = [NSData dataWithContentsOfURL: eventURL];
    NSError *error = nil;
    NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error: &error];
    
    NSDictionary *memberInfo = [dataDictionary objectForKey:@"fitpass"];
    
    NSLog(@"%@",memberInfo);
    if ([[memberInfo objectForKey:@"photo"] isEqualToString:@""]){
        //create round image
        self.profileImg.image = [UIImage imageNamed:@"add_photo"];
        self.profileImg.layer.cornerRadius = self.profileImg.frame.size.width / 2;
        self.profileImg.clipsToBounds = YES;
        //create round image border
        self.profileImg.layer.borderWidth = 3.0f;
        self.profileImg.layer.borderColor = [UIColor whiteColor].CGColor;
    }else{
        NSString *userphoto = [NSString stringWithFormat: @"%@users/%@",BaseURLFOR_IMAGE, [memberInfo objectForKey:@"photo"]];
        NSURL *photoURL = [NSURL URLWithString:userphoto];
        NSData *imageData = [NSData dataWithContentsOfURL:photoURL];
            
        self.image = [UIImage imageWithData:imageData];
        self.profileImg.image = [UIImage imageWithData:imageData];
        //blur image
        [self updateImage];
        self.addPhotoLabel.text=@"Change Photo";
        //create round image
        self.profileImg.layer.cornerRadius = self.profileImg.frame.size.width / 2;
        self.profileImg.clipsToBounds = YES;
        self.profileImg.layer.borderWidth = 3.0f;
        self.profileImg.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    //change placeholder of the fieldsw.
    self.firstName.text = [memberInfo objectForKey:@"fname"];
    self.lastName.text = [memberInfo objectForKey:@"lname"];
    self.age.text = [memberInfo objectForKey:@"age"];
    self.fitLevel.text = [memberInfo objectForKey:@"act_lvl"];
    self.gender.text = [memberInfo objectForKey:@"gender"];
    self.zipCode.text = [memberInfo objectForKey:@"zip"];
    self.weight.text = [memberInfo objectForKey:@"current_weight"];
    self.teamName.text = [memberInfo objectForKey:@"team"];
}


- (void)updateImage{
    
    UIImage *effectImage = nil;
    
    effectImage = [self.image applyDarkEffect];
    self.blurImage.image = effectImage;
    
}
#pragma mark - Profile Update Submitting
- (IBAction)signUp:(id)sender
{
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    progressHUD.labelText = @"Registering...";
    progressHUD.mode = MBProgressHUDAnimationFade;
    //check for required fields
    if ([firstName.text isEqualToString:@""] ||
        [lastName.text isEqualToString:@""]) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention!" message:@"Please enter first name and last name!" delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alertView show];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        return;
    }else{
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        
        _parentID.text = [userDefaults objectForKey:kUserID];
        _memberIDFD.text = memberID;
        NSDictionary *params =  [[NSDictionary alloc] initWithObjectsAndKeys:
                                 firstName.text, @"first_name",
                                 lastName.text, @"last_name",
                                 fitLevel.text, @"fit_level",
                                 zipCode.text, @"zip_code",
                                 gender.text, @"gender",
                                 age.text, @"age",
                                 weight.text, @"weight",
                                 teamName.text, @"team",
                                 _parentID.text, @"parentID",
                                 _memberIDFD.text, @"uID",
                                 nil] ;
        NSString *postURL = @"";
        NSLog(@"INSERT Param: %@", params);
        if (memberID){
            postURL = [NSString stringWithFormat:@"%@profileUpdate.php",BaseURL];
        }else{
            postURL = [NSString stringWithFormat:@"%@addFamily.php",BaseURL];
        }
        //PROCESS IT
        [manager POST:postURL parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON URL: %@", responseObject);
            NSString *confirmMsg = @"";
            if (memberID){
                confirmMsg = @"Family Member Information Updated";
            }else{
                confirmMsg = @"Family Member Added";
            }
            //is this successful or is it a duplicate entries
            UIAlertView *confirmAlert = [[UIAlertView alloc] initWithTitle:confirmMsg message:@"" delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [confirmAlert show];
          
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                     bundle: nil];
            SWRevealViewController *revealController = [self revealViewController];
            UIViewController *newFrontController = nil;
            MyAccount *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"accountViewController"];
            newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
            
            [revealController pushFrontViewController:newFrontController animated:YES];
            return;
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error.localizedDescription);
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        }];
    }
}
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        SWRevealViewController *revealController = [self revealViewController];
        UIViewController *newFrontController = nil;
        MyAccount *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"accountViewController"];
        newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
        
        [revealController pushFrontViewController:newFrontController animated:YES];
        
    }
}
#pragma mark - Change Picture
- (IBAction)addPicture:(id)sender {
    
    [self dismissKeyboard];
    
    NSString *actionSheetTitle = @"Change Photo";
    NSString *other1 = @"Take Photo";
    NSString *other2 = @"Choose Photo";
    NSString *cancelTitle = @"Cancel Button";
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:actionSheetTitle
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:other1, other2, nil];
    
    [actionSheet showInView:self.view];
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //Get the name of the current pressed button
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    
    if ([buttonTitle isEqualToString:@"Take Photo"])
    {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:NULL];
    }
    
    if ([buttonTitle isEqualToString:@"Choose Photo"])
    {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:NULL];
    }
    
    if ([buttonTitle isEqualToString:@"Cancel"])
    {
        [actionSheet dismissWithClickedButtonIndex:3 animated:YES];
    }
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //[userDefaults setObject:NULL forKey:kProfilePic];
    //[userDefaults synchronize];
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    
    //
    NSLog(@"Resize");
    UIImage *image = chosenImage;
    UIImage *tempImage = nil;
    CGSize targetSize = CGSizeMake(230,230);
    UIGraphicsBeginImageContext(targetSize);
    
    CGRect thumbnailRect = CGRectMake(0, 0, 0, 0);
    thumbnailRect.origin = CGPointMake(0.0,0.0);
    thumbnailRect.size.width  = targetSize.width;
    thumbnailRect.size.height = targetSize.height;
    
    [image drawInRect:thumbnailRect];
    tempImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    NSLog(@"Start Upload");
    
    NSData *imageData = UIImageJPEGRepresentation(tempImage, 50);
    NSString *urlString = [NSString stringWithFormat:@"%@photo.php?ID=%@",BaseURL, memberID];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    //  NSLog(@"%@", urlString);
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"userfile\"; filename=\".jpg\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:imageData]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    NSLog(@"Return String %@", returnString);
    
    NSLog(@"UPLOADING");
    
    
    [picker dismissViewControllerAnimated:YES completion:^{
        //update background image
        self.image = chosenImage;
        self.profileImg.image = chosenImage;
        
        [self updateImage];
    }];
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

#pragma mark - Text Field Editing
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
    
}

#pragma mark - Text Field Drop Down
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    //see if I can get the y position of the text field checked
    //double positionX = textField.frame.origin.x;
   // double positionY = textField.frame.origin.y;
    //double positionX= 0;
    //double fieldwidth = textField.frame.size.width;
    double fieldwidth = self.view.frame.size.width;
    [self.lblSelectedNames setHidden:YES];
    double viewHeight = self.view.frame.size.height;
    double positionY = 180;
    
    [self.gender resignFirstResponder];
    [self.fitLevel resignFirstResponder];
    
    if (textField == self.gender) {
        //int optionCt = [genderArray count];
        //double viewHeight = optionCt * 44;
        //double positionY = self.view.frame.size.height - viewHeight;
        /*[self showPopUpWithTitle:@"" withOption:genderArray xy:CGPointMake(positionX, positionY) size:CGSizeMake(fieldwidth, viewHeight) isMultiple:NO];*/
        [self showPopUpWithTitle:@"" withOption:genderArray xy:CGPointMake(0, positionY) size:CGSizeMake(fieldwidth, viewHeight) isMultiple:NO];
        //set the label market to gender so we know what we are clicking
        self.lblSelectedNames.text = @"Gender";
    }else if (textField == self.fitLevel) {
       // int optionCt = [activityArray count];
        //double viewHeight = optionCt * 44;
        //double positionY = self.fitLevel.frame.origin.y;
       // double positionY = self.view.frame.size.height - viewHeight;
        /*[self showPopUpWithTitle:@"" withOption:activityArray xy:CGPointMake(positionX, positionY) size:CGSizeMake(fieldwidth, viewHeight) isMultiple:NO];*/
        [self showPopUpWithTitle:@"" withOption:activityArray xy:CGPointMake(0, positionY) size:CGSizeMake(fieldwidth, viewHeight) isMultiple:NO];
        self.lblSelectedNames.text = @"Level";
    }
}

-(void)showPopUpWithTitle:(NSString*)popupTitle withOption:(NSArray*)arrOptions xy:(CGPoint)point size:(CGSize)size isMultiple:(BOOL)isMultiple{
    
    
    Dropobj = [[DropDownListView alloc] initWithTitle:popupTitle options:arrOptions xy:point size:size isMultiple:isMultiple];
    Dropobj.delegate = self;
    [Dropobj showInView:self.view animated:YES];
    
    /*----------------Set DropDown backGroundColor-----------------*/
    [Dropobj SetBackGroundDropDwon_R:0.0 G:0.0 B:0.0 alpha:0.70];
    
}

- (void)DropDownListView:(DropDownListView *)dropdownListView Datalist:(NSMutableArray*)ArryData{
    
    if (ArryData.count>0) {
        lblSelectedNames.text=[ArryData componentsJoinedByString:@"\n"];
        CGSize size=[self GetHeightDyanamic:lblSelectedNames];
        lblSelectedNames.frame=CGRectMake(0, 100, size.width, size.height);
    }
    else{
        lblSelectedNames.text=@"";
    }
    
}
- (void)DropDownListViewDidCancel{
    
}


-(CGSize)GetHeightDyanamic:(UILabel*)lbl
{
    NSRange range = NSMakeRange(0, [lbl.text length]);
    CGSize constraint;
    constraint= CGSizeMake(288 ,MAXFLOAT);
    CGSize size;
    
    if (([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)) {
        NSDictionary *attributes = [lbl.attributedText attributesAtIndex:0 effectiveRange:&range];
        CGSize boundingBox = [lbl.text boundingRectWithSize:constraint options: NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size;
        
        size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    }
    else{
        size = [lbl.text sizeWithFont:[UIFont fontWithName:@"Helvetica" size:14] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    }
    return size;
}

-(void)DropDownListView:(DropDownListView *)dropdownListView didSelectedIndex:(NSInteger)anIndex{
    NSLog(@"Index: %ld", (long)anIndex);
    if (self.gender && [self.lblSelectedNames.text isEqual: @"Gender"]) {
        NSLog(@"%@", [genderArray objectAtIndex:anIndex]);
        self.gender.text = [genderArray objectAtIndex:anIndex];
    }
    if (self.fitLevel && [self.lblSelectedNames.text  isEqual: @"Level"]) {
        self.fitLevel.text = [activityArray objectAtIndex:anIndex];
    }
    [Dropobj fadeOut];
}

#pragma mark - Keyboard control -
-(void)dismissKeyboard {
    
    NSLog(@"HIDE KEYBOARD");
    
    [Dropobj fadeOut];
    [self.firstName resignFirstResponder];
    [self.lastName resignFirstResponder];
    [self.weight resignFirstResponder];
    [self.gender resignFirstResponder];
    [self.age resignFirstResponder];
    [self.fitLevel resignFirstResponder];
    [self.zipCode resignFirstResponder];
    [self.teamName resignFirstResponder];
}
- (IBAction)BackClicked
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    SWRevealViewController *revealController = [self revealViewController];
    UIViewController *newFrontController = nil;
    MyAccount *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"accountViewController"];
    newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    
    [revealController pushFrontViewController:newFrontController animated:YES];
}
@end
