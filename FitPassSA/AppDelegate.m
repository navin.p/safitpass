//
//  AppDelegate.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/9/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import "AppDelegate.h"
#import "AppSetting.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "AFNetworking.h"
#import "Instruction.h"
#import "SWRevealViewController.h"
#import "GAI.h"
#import "Header.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
@import Firebase;
@import HockeySDK;

static NSString *const kTrackingId = @"UA-60394307-2";
static NSString *const kAllowTracking = @"allowTracking";

@interface AppDelegate ()<SWRevealViewControllerDelegate>

@end

@implementation AppDelegate
+ (void)initialize
{
    // Nib files require the type to have been loaded before they can do the wireup successfully.
   [FBSDKLoginButton class];
  
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [UIApplication sharedApplication].applicationIconBadgeNumber=0;
    if ([AVCaptureDevice respondsToSelector:@selector(requestAccessForMediaType: completionHandler:)]) {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            // Will get here on both iOS 7 & 8 even though camera permissions weren't required
            // until iOS 8. So for iOS 7 permission will always be granted.
            if (granted) {
                // Permission has been granted. Use dispatch_async for any UI updating
                // code because this block may be executed in a thread.
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    // Permission has been Granted.
                    
                });
            } else {
                // Permission has been denied.
            }
        }];
    } else {
        
        // Permission has been Granted.
        
    }
    

    
    ALAssetsLibrary *lib = [[ALAssetsLibrary alloc] init];
    [lib enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        NSLog(@"%zd", [group numberOfAssets]);
    } failureBlock:^(NSError *error) {
        if (error.code == ALAssetsLibraryAccessUserDeniedError) {
            NSLog(@"user denied access, code: %zd", error.code);
        } else {
            NSLog(@"Other error code: %zd", error.code);
        }
    }];
    [FIRApp configure];
    [self connectToFCM];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:) name:kFIRInstanceIDTokenRefreshNotification object:nil];
    
    NSString *fireBaseToken = [[FIRInstanceID instanceID] token];
    NSLog(@"InstanceID token: %@", fireBaseToken);
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:fireBaseToken forKey:@"FirebaseToken"];
    [userDefaults synchronize];
    
    
    
    
    [[BITHockeyManager sharedHockeyManager] configureWithIdentifier:@"adf1a3b30357d48eac8f85d3b0f41198"];
    // Do some additional configuration if needed here
    [[BITHockeyManager sharedHockeyManager] startManager];
    [[BITHockeyManager sharedHockeyManager].authenticator
     authenticateInstallation];

    // Override point for customization after application launch.
    
    //AppSee tracking
    //[Appsee start:@"d862acbdd7b3476ea7c1b06eef84e91b"];
    
    // Optional: automatically send uncaught exceptions to Google Analytics.
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 20;
    
    // Optional: set Logger to VERBOSE for debug information.
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    
    // Initialize tracker. Replace with your tracking ID.
    [[GAI sharedInstance] trackerWithTrackingId: kTrackingId];
    
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId: kTrackingId];
    tracker.allowIDFACollection = YES;

    
    //load custom background
    [self.window setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"appbkgd.png"]]];
    
    userDefaults = [[NSUserDefaults alloc] init];
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    
    [userDefaults setObject:nil forKey:kEventCat];
    [userDefaults setObject:nil forKey:kEventDate];
    //Globally set userID = 14 for testing    
    //[userDefaults setObject:@"14" forKey:kUserID];
    [userDefaults synchronize];
    
    //set custom back button
  //  UIImage *backBtnIcon = [UIImage imageNamed:@"white_arrow_left"];
   // [UINavigationBar appearance].backgroundColor = [UIColor colorWithRed:41/255.0 green:138/255.0 blue:130/255.0 alpha:1.0];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
      //  [UINavigationBar appearance].tintColor = [UIColor whiteColor];
      
    }else{
        
     
    }
 
    
    //FACEBOOK Integration returns
    [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
    
    //set a variable to determine if this i the first time we launch this app
    
    //------ Remove Tutorial Screen by Navin PAtidar On 12/09/2019 - Refrence Kapil Sir (Android developer)
    
//    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"hasSeenTutorial"]){
//        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"hasSeenTutorial"];
//        self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
//        // Override point for customization after application launch.
//        self.viewController = [[Instruction alloc] initWithNibName:@"Instruction" bundle:nil];
//        self.window.rootViewController = self.viewController;
//        [self.window makeKeyAndVisible];
//    }else{
    
        NSLog(@"Second Time Visit");
        NSLog(@"UID %@",[userDefaults objectForKey:kUserID]);
        //get storyboard
        UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        //get attach to main window
        self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        //assign instruction to new uiviewcontroller
        if ([userDefaults objectForKey:kUserID]){            
            UIViewController *navView = [mainstoryboard instantiateViewControllerWithIdentifier:@"swreveal"];
            //UIViewController *navView = [mainstoryboard instantiateViewControllerWithIdentifier:@"homeViewController"];
            
            //attach instruction ui view controler to root
            [self.window setRootViewController:navView];
        }else{
            UIViewController *navView = [mainstoryboard instantiateViewControllerWithIdentifier:@"loginViewController"];
            //attach instruction ui view controler to root
            [self.window setRootViewController:navView];
        }
        //display it.
        [self.window makeKeyAndVisible];
        
   // }
    
    [[UINavigationBar appearance]setBarTintColor: [UIColor colorWithRed:1/255.0f green:89/255.0f blue:128/255.0f alpha:1.0]];
    
    [[UINavigationBar appearance]setTintColor:[UIColor whiteColor]];


    [UINavigationBar appearance].titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    if (@available(iOS 11.0, *)) {
        [UINavigationBar appearance].largeTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    } else {
    }
    [[UITabBar appearance] setUnselectedItemTintColor:[UIColor whiteColor]];
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
    [[UITabBar appearance]  setTranslucent:NO] ;

    // Let the device know we want to receive push notifications
    //-- Set Notification
    if ([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)]){
        // iOS 8 Notifications
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        
        [application registerForRemoteNotifications];
    }else {
        // iOS < 8 Notifications
        [application registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
    }
    
    return  [[FBSDKApplicationDelegate sharedInstance] application:application
                                     didFinishLaunchingWithOptions:launchOptions];;
    

   //return YES;
}
- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    
    
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [self connectToFCM];

     [FBSDKAppEvents activateApp];
    
    // Do the following if you use Mobile App Engagement Ads to get the deferred
    // app link after your app is installed.
    [FBSDKAppLinkUtility fetchDeferredAppLink:^(NSURL *url, NSError *error) {
        if (error) {
            NSLog(@"Received error while fetching deferred app link %@", error);
        }
        if (url) {
            [[UIApplication sharedApplication] openURL:url];
        }
    }];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [userDefaults synchronize];
}

#pragma mark - FireBase 

-(void)connectToFCM
{
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        if(!error)
        {
            NSLog(@"Connected To FCM");
            NSString *fireBaseToken = [[FIRInstanceID instanceID] token];
            NSLog(@"InstanceID token: %@", fireBaseToken);
            if(fireBaseToken != nil)
            {
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                [userDefaults setValue:fireBaseToken forKey:@"FirebaseToken"];
                [userDefaults synchronize];
                
                // NSString *token = [[NSString alloc] initWithData:deviceToken encoding:NSUTF8StringEncoding];
                //NSLog(@"My token is: %@", deviceToken);
               // NSString *tokenStr = [NSString stringWithFormat:@"%@", fireBaseToken];
                //NSLog(@"Token String %@", tokenStr);
              //  NSString *lessThan = [tokenStr stringByReplacingOccurrencesOfString:@"<" withString:@""];
                //NSLog(@"Less Than String %@", lessThan);
                //NSString *greaterThan = [lessThan stringByReplacingOccurrencesOfString:@">" withString:@""];
                //NSLog(@"Greater Than String %@", greaterThan);
                //NSString *token = [greaterThan stringByReplacingOccurrencesOfString:@" " withString:@""];
                NSString *userID = [userDefaults objectForKey:kUserID];
                NSLog(@"My token is: %@", fireBaseToken);
                NSLog(@"My ID is: %@", userID);
                
                if (![userID isEqualToString:@"(null)"]  || userID.length != 0) {
                    //store it to database
                    //compile the URL
                    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
                    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
                    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
                    // manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
                    NSDictionary *params =  [[NSDictionary alloc] initWithObjectsAndKeys:
                                             fireBaseToken, @"token",
                                             @"iOS", @"platform",
                                             userID, @"uid",
                                             nil];
                    [manager POST:[NSString stringWithFormat:@"%@token.php",BaseURL] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                        NSLog(@"JSON: %@", [responseObject description]);
                    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                        NSLog(@"Error: %@", error.localizedDescription);
                    }];
                }
            }
        }
    }];
}


- (void)tokenRefreshNotification:(NSNotification *)notification {
    NSString *fireBaseToken = [[FIRInstanceID instanceID] token];
    NSLog(@"InstanceID token: %@", fireBaseToken);
    if(fireBaseToken != nil)
    {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setValue:fireBaseToken forKey:@"FirebaseToken"];
        [userDefaults synchronize];
        // NSString *token = [[NSString alloc] initWithData:deviceToken encoding:NSUTF8StringEncoding];
        //NSLog(@"My token is: %@", deviceToken);
        //NSString *tokenStr = [NSString stringWithFormat:@"%@", fireBaseToken];
        //NSLog(@"Token String %@", tokenStr);
      // NSString *lessThan = [tokenStr stringByReplacingOccurrencesOfString:@"<" withString:@""];
        //NSLog(@"Less Than String %@", lessThan);
        //NSString *greaterThan = [lessThan stringByReplacingOccurrencesOfString:@">" withString:@""];
        //NSLog(@"Greater Than String %@", greaterThan);
      //  NSString *token = [greaterThan stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *userID = [userDefaults objectForKey:kUserID];
        NSLog(@"My token is: %@", fireBaseToken);
        NSLog(@"My ID is: %@", userID);
        if (![userID isEqualToString:@"(null)"]  || userID.length != 0) {
            //store it to database
            //compile the URL
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            manager.requestSerializer = [AFHTTPRequestSerializer serializer];
            manager.responseSerializer = [AFHTTPResponseSerializer serializer];
            // manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
            NSDictionary *params =  [[NSDictionary alloc] initWithObjectsAndKeys:
                                     fireBaseToken, @"token",
                                     @"iOS", @"platform",
                                     userID, @"uid",
                                     nil] ;
            [manager POST:[NSString stringWithFormat:@"%@token.php",BaseURL] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                NSLog(@"JSON: %@", responseObject);
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                NSLog(@"Error: %@", error.localizedDescription);
            }];
        }
        [self connectToFCM];
    }
}

#pragma mark - Facebook SDK
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
            
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}
@end
