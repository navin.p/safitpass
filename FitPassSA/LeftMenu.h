//
//  LeftMenu.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/23/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"
#import "AppSetting.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface LeftMenu : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITextField *searchFld;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, assign) BOOL slideOutAnimationEnabled;


@property (strong, nonatomic) IBOutlet FBSDKLoginManager *loginManager;

- (IBAction)search:(id)sender;

@end
