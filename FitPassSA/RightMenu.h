//
//  RightMenu.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/23/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"
#import "AppSetting.h"

@interface RightMenu : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UILabel *sortBy;

@property (nonatomic, strong) NSMutableArray *catList;
@property (nonatomic, assign) BOOL slideOutAnimationEnabled;

@property (nonatomic, strong) id Delegate;



@end
