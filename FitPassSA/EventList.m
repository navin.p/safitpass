//
//  EventList.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/19/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import "EventList.h"
#import "AppSetting.h"
#import "Event.h"
#import <CoreLocation/CoreLocation.h>
#import <CoreLocation/CLLocationManagerDelegate.h>
#import "MBProgressHUD.h"
#import "EventCellView.h"


@interface EventList ()

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic) BOOL useCustomCells;
@property (nonatomic, weak) UIRefreshControl *refreshControl;

@end

@implementation EventList

@end