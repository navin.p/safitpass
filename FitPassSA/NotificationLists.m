//
//  NotificationLists.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/31/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import "NotificationLists.h"
#import "AppSetting.h"
#import "SWRevealViewController.h"
#import "UserEventCellVeiw.h"
#import "Event.h"
#import "EventDetail.h"
#import "SWTableViewCell.h"
#import "NSObject+setNotification.h"
#import "EventList.h"
#import "Profile.h"
#import "GAIDictionaryBuilder.h"
#import "GAI.h"
#import "GAIFields.h"
#import "Header.h"

@interface NotificationLists (){
    NSUserDefaults *userDefaults;
}

@end

@implementation NotificationLists

@synthesize userProfile;
@synthesize tableView;


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSString *str = @"Notification List";
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:str];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    tracker.allowIDFACollection = YES;
}

- (void)viewDidLoad {
    
    //Layout adjustment
    self.tabBar.autoresizesSubviews = NO;
    self.tabBar.clipsToBounds = YES;
    
    
    //add logo to navigation bar
    //set your image frame
    
    //UIImageView *image=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,58,33)] ;
    //set your image logo replace to the main-logo
    //[image setImage:[UIImage imageNamed:@"fiplogo_small"]];
    //[self.navigationController.navigationBar.topItem setTitleView:image];

      // Updated by Navin
    //    UIColor *navColor =  [UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:1.0];
//    self.navigationController.navigationBar.barTintColor = navColor;
//    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    [self createMenu];
    
    //first get users
    userDefaults = [[NSUserDefaults alloc] init];
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString *userID = [userDefaults objectForKey:kUserID];
    
    
    [self loadNotifications:userID];
    
}

- (void)loadNotifications:(NSString*)userID{
        
    //then get the data
    NSString *url = [NSString stringWithFormat: @"%@profile.php?uid=%@",BaseURL, userID];
    NSURL *eventURL = [NSURL URLWithString: url];
    NSLog(@"%@", eventURL);
    NSData *jsonData = [NSData dataWithContentsOfURL: eventURL];
    NSError *error = nil;
    NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error: &error];
    
    NSDictionary *userInfo = [dataDictionary objectForKey:@"fitpass"];
    
    //load events
    NSString *eventfound = [userInfo objectForKey:@"notificationfound"];
    if ([eventfound isEqualToString:@"no"]){
        tableView.hidden = YES;
        self.noNotification.hidden = NO;
    }else{
        tableView.hidden = NO;
        self.noNotification.hidden = YES;
        
        //load events
        NSArray *eventRegistered = [[NSArray alloc] init];
        eventRegistered = [userInfo objectForKey:@"notifications"];
        NSLog(@"%@", eventRegistered);
        self.notificationList = [NSMutableArray array];
        for(NSDictionary *eventDictionary in eventRegistered){
            //NSLog(@"%@", eventDictionary);
            Event *event = [Event blogPostWithTitle:[eventDictionary objectForKey:@"title"]];
            //   NSLog(@"%@", event.eventTitle);
            
            event.eventID = [eventDictionary objectForKey:@"ID"];
            event.eventTitle = [eventDictionary objectForKey:@"title"];
            event.startDate = [eventDictionary objectForKey:@"sdate"];
            event.startTime = [eventDictionary objectForKey:@"stime"];
            event.endTime = [eventDictionary objectForKey:@"etime"];
            event.ckTime = [eventDictionary objectForKey:@"ctime"];
            event.eventDescription = [eventDictionary objectForKey:@"description"];
            event.points = [eventDictionary objectForKey:@"point"];
            event.eventPhoto = [eventDictionary objectForKey:@"photo"];
            event.locationInfo = [eventDictionary objectForKey:@"location"];
            event.address = [eventDictionary objectForKey:@"address"];
            event.eventTime = [eventDictionary objectForKey:@"event_time"];
            event.loclat = [eventDictionary objectForKey:@"loc_lat"];
            event.loclong = [eventDictionary objectForKey:@"loc_long"];
            event.locName = [eventDictionary objectForKey:@"loc_name"];
            event.iCalID = [eventDictionary objectForKey:@"iCalID"];
            event.userPt = [eventDictionary objectForKey:@"userPt"];
            NSLog(@"%@", event.eventTitle);
            
            [self.notificationList addObject:event];
            // [self.tableView reloadData];
        }
        [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
    }

    
    
    
}


#pragma mark - Table View Setup
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    // Return the number of rows in the section.
    if (self.notificationList){
        return [self.notificationList count];
    }else{
        return 1;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
     return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 8.f; // you can have your own choice, of course
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}
-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
#pragma mark - Table Cell Delegation
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UserEventCellVeiw *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    //set right buttons
    [cell setRightUtilityButtons:[self rightButtons] WithButtonWidth:58.0f];
    cell.delegate = self;
    
    cell.layer.cornerRadius = 10;
    cell.layer.masksToBounds = YES;
    
    if (self.notificationList){
        //use custom class
        Event *event = [self.notificationList objectAtIndex:indexPath.section];
        NSString *eventTitlewDate = [NSString stringWithFormat: @"%@ on %@",  event.eventTitle, event.startDate];
        cell.event_points.text = event.points;
        cell.event_title.text = eventTitlewDate;
    }
    //cell.hospital_address.text = [NSString stringWithFormat:@"%@",hospital.address];
    return cell;
}

//on direct press, trigger segue
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.savedSelectedIndexPath = [self.tableView indexPathForSelectedRow];
    [self performSegueWithIdentifier:@"notificationDetail" sender:self];
}


#pragma mark - Segue Process Navigation
-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSLog(@"Preparing for seque %@", segue.identifier);
    
    if ([segue.identifier isEqualToString:@"notificationDetail"]){
        
        NSIndexPath *indexPath =  self.savedSelectedIndexPath; //get the index path of the row selected
        
        Event *event = [self.notificationList objectAtIndex:indexPath.section]; // now get the content of that row
        EventDetail *eventDetailView = (EventDetail *)segue.destinationViewController;
        eventDetailView.eventDetail = event;
        
    }
}


- (IBAction)goHome:(id)sender {    
    
    [userDefaults setObject:nil forKey:kEventCat];
    [userDefaults setObject:nil forKey:kEventDate];
    [userDefaults synchronize];
    
    SWRevealViewController *revealController = [self revealViewController];
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    UIViewController *newFrontController = nil;
    EventList *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"eventListViewController"];
    newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    [revealController pushFrontViewController:newFrontController animated:YES];
    
}

#pragma mark - SlideNavigationController Methods -
- (void)createMenu{
    
    //create menu
    SWRevealViewController *revealController = [self revealViewController];
    //create the left menu button
    UIButton *button  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [button setImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
    [button addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
    
    UITapGestureRecognizer *viewTapped=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewTapped:)];
    [self.view addGestureRecognizer:viewTapped];
    viewTapped.delegate = self;
    [viewTapped setCancelsTouchesInView:NO];
    
    self.revealViewController.delegate = self;
    [revealController.frontViewController.revealViewController tapGestureRecognizer];
    
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
       shouldReceiveTouch:(UITouch *)touch
{
    NSLog(@"%@", [touch.view class]);
    if([touch.view class] == self.tableView.class || [touch.view class] ==[UITableViewCell class]){
        NSLog(@"touched");
        return NO;
    }
    return YES;
}
-(void)viewTapped:(UITapGestureRecognizer*)gestureRecognizer
{
    //Do what you want here
    SWRevealViewController *revealController = self.revealViewController;
    if (revealController.frontViewPosition == FrontViewPositionRight) {
        [revealController revealToggleAnimated:YES];
    }
    
}
-(void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position {
    if(position == FrontViewPositionLeft) {
        self.view.userInteractionEnabled = YES;
    } else {
        self.view.userInteractionEnabled = NO;
    }
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position {
    if(position == FrontViewPositionLeft) {
        self.view.userInteractionEnabled = YES;
    } else {
        self.view.userInteractionEnabled = NO;
    }
}

#pragma mark - Swipe Button on Cell

#pragma mark - Swipe Utility Buttons
- (NSArray *)rightButtons
{
    
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:191.0/255.0 green:16.0/255.0 blue:45.0/255.0 alpha:1.0]
                                                 icon:[UIImage imageNamed:@"delete_white"]];
    
    return rightUtilityButtons;
}
#pragma mark - SWTableViewDelegate
- (void)swipeableTableViewCell:(SWTableViewCell *)cell scrollingToState:(SWCellState)state
{
    switch (state) {
        case 0:
            NSLog(@"utility buttons closed");
            break;
        case 1:
            NSLog(@"left utility buttons open");
            break;
        case 2:
            NSLog(@"right utility buttons open");
            break;
        default:
            break;
    }
}
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    switch (index) {
        case 0:
        {
            //get directions, first get the row
            NSIndexPath *cellIndexPath = [self.tableView indexPathForCell:cell];
            self.savedSelectedIndexPath = cellIndexPath;
            Event *event = [self.notificationList objectAtIndex:cellIndexPath.section];
             NSString *userID = [userDefaults objectForKey:kUserID];
            NSString *eventNotify = [self removeNotified:event userID:userID icalID:event.iCalID];
            
            [self loadNotifications:userID];

            if ([eventNotify isEqualToString:@"Removed"]){
                //Save to iCal
                [self loadNotifications:userID];
                
                [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
              
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Notification Center" message:@"Your notification has been removed." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alertView show];
                
                
                [self loadNotifications:userID];
                [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
               
            }
            
            
             [cell hideUtilityButtonsAnimated:YES];
            break;
        }
        case 1:
        {
            break;
        }
        default:
            break;
    }
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell
{
    // allow just one cell's utility button to be open at once
    return YES;
}

- (BOOL)swipeableTableViewCell:(SWTableViewCell *)cell canSwipeToState:(SWCellState)state
{
  
    // NSLog(@"%@", state);
    switch (state) {
        case 1:
            return YES;
            break;
        default:
            break;
    }
    
    return YES;
}

@end
