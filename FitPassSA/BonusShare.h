//
//  BonusShare.h
//  FitPassSA
//
//  Created by Ping-Jung Tsai on 5/13/17.
//  Copyright © 2017 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"
#import "AppSetting.h"
#import "SlideNavigationController.h"


@interface BonusShare : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *closeBtn;
@property (nonatomic, strong) Event *eventDetail;
@property (nonatomic, assign) BOOL slideOutAnimationEnabled;
@property (strong, nonatomic) IBOutlet UILabel *eventPoint;

@property (nonatomic, strong) NSString *eventPointV;
@property (nonatomic, strong) NSString *photoName;
@property (nonatomic, strong) NSMutableArray *eventList;
@property (nonatomic, strong) NSString *eventTitle;
@property (nonatomic, strong) NSString *shareBonus;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;

- (IBAction)closeWindow:(id)sender;
- (IBAction)viewProfile:(id)sender;
- (IBAction)sharePoint:(id)sender;

@end
