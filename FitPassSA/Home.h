//
//  Home.h
//  FitPassSA
//
//  Created by Ping-Jung Tsai on 4/21/17.
//  Copyright © 2017 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"

@interface Home : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UIGestureRecognizerDelegate, UITabBarDelegate, SWRevealViewControllerDelegate>


@property (weak, nonatomic) IBOutlet UIImageView *profileImg;
@property (weak, nonatomic) IBOutlet UILabel *userPTs;
@property (weak, nonatomic) IBOutlet UIButton *totalFPEvents;
@property (weak, nonatomic) IBOutlet UIButton *totalFIPEvents;
@property (weak, nonatomic) IBOutlet UIImageView *FourthBadge;
@property (weak, nonatomic) IBOutlet UIImageView *thirdBadge;
@property (weak, nonatomic) IBOutlet UIImageView *firstBadge;
@property (weak, nonatomic) IBOutlet UIImageView *secondBadge;
@property (weak, nonatomic) IBOutlet UIImageView *badgeBkgd;
@property (weak, nonatomic) IBOutlet UILabel *pillarText;
@property (weak, nonatomic) IBOutlet UILabel *fitPassTxt;
@property (weak, nonatomic) IBOutlet UIImageView *fipLogo;
@property (weak, nonatomic) IBOutlet UICollectionView *sponsorGrid;
@property (strong, nonatomic) IBOutlet UICollectionViewFlowLayout *collectionLayout;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *viewTapped;
@property (weak, nonatomic) IBOutlet UITabBar *tapBar;
@property (strong, nonatomic) IBOutlet UITabBarItem *aboutSAPR;
@property (strong, nonatomic) IBOutlet UITabBarItem *viewAcct;
@property (strong, nonatomic) IBOutlet UITabBarItem *viewFP;
@property (strong, nonatomic) IBOutlet UITabBarItem *viewFIP;

@property (strong, nonatomic) IBOutlet UIImageView *blurImage;

//Updated by navin Patidar
@property (weak, nonatomic) IBOutlet UIImageView *imgMeter;
@property (weak, nonatomic) IBOutlet UIScrollView *scroll;
@property (weak, nonatomic) IBOutlet UITextField *txtNumber;

#pragma Variables
@property (nonatomic) UIImage *image;
@property (nonatomic) int imageIndex;
@property (nonatomic, strong) NSMutableArray *sponsorList;
@property (nonatomic, strong) NSMutableArray *profileInfo;
@property (nonatomic, strong) NSMutableArray *eventsInfo;
- (IBAction)actionOnOK:(id)sender;

- (IBAction)gotoFPEvent:(id)sender;
- (IBAction)gotoFIPEvent:(id)sender;

@end
