//
//  RightMenu.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/23/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "RightMenu.h"
#import "SWRevealViewController.h"
#import "UITextField+TextFld.h"
#import "MBProgressHUD.h"
#import "AppSetting.h"
#import "EventLists.h"
#import "GAIDictionaryBuilder.h"
#import "GAI.h"
#import "GAIFields.h"
#import "Header.h"
@interface RightMenu (){
    NSUserDefaults *userDefaults;
}

@end

@implementation RightMenu

#pragma mark - UIViewController Methods -
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self.slideOutAnimationEnabled = YES;
    
    return [super initWithCoder:aDecoder];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSString *str = @"Event Category Filter";
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:str];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    tracker.allowIDFACollection = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self killHUD];
    
    userDefaults = [[NSUserDefaults alloc] init];
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    // Do any additional setup after loading the view.
    self.tableView.separatorColor = [UIColor whiteColor];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.backgroundColor = [UIColor clearColor];
    
    //load the data
    NSString *calendar = [userDefaults objectForKey:kCalendar];
    NSString *eventType = nil;
    NSString *url = nil;
    if ([calendar isEqualToString:@"FitPass"]){
        eventType = @"fitpass";
    }else if ([calendar isEqualToString:@"Park"]){
        eventType = @"park";
    }
    url = [NSString stringWithFormat: @"%@catList.php?type=%@",BaseURL,eventType];
    NSLog(@"%@", url);
    NSURL *eventURL = [NSURL URLWithString: url];
    NSData *jsonData = [NSData dataWithContentsOfURL: eventURL];
    NSError *error = nil;
    NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error: &error];
    //  NSLog(@"%@", dataDictionary);
    //self.catList = [NSMutableArray array];
    self.catList = [dataDictionary objectForKey:@"fitpass"];
    self.sortBy.text = [NSString stringWithFormat:@"    %@", self.sortBy.text];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View Setup
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (self.catList){
        return [self.catList count];
    }else{
        return 1;
    }
}

#pragma mark - Table Cell Delegation
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    NSDictionary *cellItem = [self.catList objectAtIndex:indexPath.row];
    cell.textLabel.text = [cellItem objectForKey:@"name"];
    
    //disable the line at the last one
    if (indexPath.row == ([self.catList count]-1)){
        cell.separatorInset =  UIEdgeInsetsMake(0.f, 0.f , 0.f, cell.bounds.size.width-85);
       // cell.separatorInset = UIEdgeInsetsMake(0.f, 0.f, 0.f, cell.bounds.size.width + 30);
        
       // [cell.textLabel setFrame:CGRectMake(15.0f, cell.textLabel.frame.origin.y, cell.textLabel.frame.size.width, cell.textLabel.frame.size.height)];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1];
    //cell.hospital_address.text = [NSString stringWithFormat:@"%@",hospital.address];
    return cell;
}
//on direct press, trigger segue
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    progressHUD.labelText = @"Loading...";
    progressHUD.mode = MBProgressHUDAnimationFade;
   
    //get current value and set the eventCategory ID in userDefault
    NSDictionary *catPicked =  [self.catList objectAtIndex:indexPath.row];
    NSString *eventCat =[catPicked objectForKey:@"ID"];
    
    [userDefaults setObject:eventCat forKey:kEventCat];
    [userDefaults synchronize];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    SWRevealViewController *revealController = [self revealViewController];
    
    UIViewController *newFrontController = nil;
    
    
    NSString *calendar = [userDefaults objectForKey:kCalendar];
    NSLog(@"Calendar: %@",calendar);
   /* if ([calendar isEqualToString:@"FitPass"]){
        EventLists *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"eventListViewController"];
    newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    }else{
        EventLists *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"parkEventListViewController"];
        newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    }*/
    EventLists *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"FilterViewController"];
    newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [revealController pushFrontViewController:newFrontController animated:YES];
    
    [self killHUD];
    return;
}
- (void)killHUD
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
