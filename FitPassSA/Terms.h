//
//  Terms.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 5/7/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Terms : UIViewController

@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *dismissBack;
- (IBAction)windowDismiss:(id)sender;

@end
