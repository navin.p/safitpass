//
//  EventLists.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/19/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "Event.h"
#import "EventCellView.h"
#import <CoreLocation/CoreLocation.h>
#import <CoreLocation/CLLocationManagerDelegate.h>
#import "SWRevealViewController.h"
#import "RMDateSelectionViewController.h"


@interface EventLists : UIViewController <CLLocationManagerDelegate,UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, UITabBarDelegate, SWRevealViewControllerDelegate>

//@property (strong, nonatomic) IBOutlet UIBarButtonItem *filterBar;
//@property (strong, nonatomic) IBOutlet UIBarButtonItem *menuBar;
@property (strong, nonatomic) IBOutlet UIButton *dateForward;
@property (strong, nonatomic) IBOutlet UIButton *dateSelection;
@property (strong, nonatomic) IBOutlet UIButton *dateBackward;
@property (strong, nonatomic) IBOutlet UIButton *byPoints;
@property (strong, nonatomic) IBOutlet UIButton *byLocation;
@property (strong, nonatomic) IBOutlet UIButton *byTime;

@property (weak, nonatomic) IBOutlet UILabel *currentUserName;
@property (weak, nonatomic) IBOutlet UIImageView *currentUserPhoto;
@property (weak, nonatomic) IBOutlet UIView *currentUser;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *currentUserHeight;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIImageView *noEvent;
@property (strong, nonatomic) IBOutlet UITabBar *tabBar;

@property (strong, nonatomic) IBOutlet UITabBarItem *aboutFP;

@property (strong, nonatomic) IBOutlet UITabBarItem *viewProfile;
@property (strong, nonatomic) IBOutlet UITabBarItem *viewEvents;

@property (nonatomic, strong) NSMutableArray *eventList;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property(strong, nonatomic) NSIndexPath *savedSelectedIndexPath;
@property (strong, nonatomic) IBOutlet UISwipeGestureRecognizer *swipeLeft;
@property (strong, nonatomic) IBOutlet UISwipeGestureRecognizer *swipeRight;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *viewTap;

- (IBAction)swipeRightAction:(id)sender;

- (IBAction)sortByPt:(id)sender;
- (IBAction)sortByLoc:(id)sender;
- (IBAction)sortByTime:(id)sender;
- (IBAction)viewNextDay:(id)sender;
- (IBAction)viewPrevDay:(id)sender;
- (IBAction)pickDate:(id)sender;

- (NSString *) currentDate;
- (NSString *) previousDate;

- (void) loadEvents:(NSString*)searchType sortEvent:(NSString*)sortType searchDate:(NSString *)dateSelect;
@end
