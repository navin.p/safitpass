//
//  Profile.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/27/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import "Profile.h"
#import "AppSetting.h"
#import "SWRevealViewController.h"
#import "UserEventCellVeiw.h"
#import "UserInfo.h"
#import "Event.h"
#import "EventList.h"
#import "FitPassInfo.h"
#import "NotificationLists.h"
#import "GAIDictionaryBuilder.h"
#import "GAI.h"
#import "GAIFields.h"
#import "MBProgressHUD.h"
#import "Header.h"
@interface Profile (){
    NSUserDefaults *userDefaults;
}

@end

@implementation Profile
@synthesize userProfile;
@synthesize tableView = _tableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated{
    //load current user
    NSString *currentUser = [userDefaults objectForKey:kCurrentUser];
    [self loadProfile:currentUser];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSString *str = @"Point Tracking";
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:str];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    tracker.allowIDFACollection = YES;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Layout adjustment
    self.tabBar.autoresizesSubviews = NO;
    self.tabBar.clipsToBounds = YES;
    
    //add logo to navigation bar
    UIImageView *image=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,105,36)] ;
    //set your image logo replace to the main-logo
    [image setImage:[UIImage imageNamed:@"fp-newlogo"]];
    [self.navigationController.navigationBar.topItem setTitleView:image];
 
    
      // Updated by Navin
//    UIColor *navColor =  [UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:1.0];
//    self.navigationController.navigationBar.barTintColor = navColor;
    
    [self createMenu];
    userDefaults = [[NSUserDefaults alloc] init];
    userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *currentUser = [userDefaults objectForKey:kCurrentUser];
    
    //show progress bar
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self loadProfile:currentUser];
        [self killHUD];
        //[MBProgressHUD hideHUDForView:self.view animated:YES];
    });
    
}
- (void)loadProfile:(NSString*)uid{
    NSLog(@"Load Profile");
  //  NSString *userID = [userDefaults objectForKey:kUserID];
    //then get the data
    NSString *url = [NSString stringWithFormat: @"%@profile.php?uid=%@",BaseURL, uid];
    NSURL *eventURL = [NSURL URLWithString: url];
    NSLog(@"%@", url);
    NSData *jsonData = [NSData dataWithContentsOfURL: eventURL];
    NSError *error = nil;
    NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error: &error];
    
    NSDictionary *userInfo = [dataDictionary objectForKey:@"fitpass"];
    // UserInfo *user = [UserInfo blogPostWithID:userID];
    //user.userPhoto = [userInfo objectForKey:@"photo"];
    //  NSNumber *userPoints = [userInfo objectForKey:@"points"];
    self.userPoint.text = [userInfo objectForKey:@"points"];
    self.totalPT.text = [userInfo objectForKey:@"points"];
    self.weightLost.text = [userInfo objectForKey:@"weight_lost"];
    self.featuredCT.text= [userInfo objectForKey:@"featured_ct"];
    self.nutritionCT.text= [userInfo objectForKey:@"nutrition_ct"];
    self.fitnessCT.text= [userInfo objectForKey:@"fitness_ct"];
    self.volunteeringCT.text= [userInfo objectForKey:@"volunteer_ct"];
    
   // NSNumber *weeklyPoints = [userInfo objectForKey:@"wkypts"];
   // self.weeklyPt.text = [weeklyPoints stringValue];
    NSString *FirstName = [NSString stringWithFormat: @"%@",  [userInfo objectForKey:@"fname"]];
    
    NSString *lastName = [NSString stringWithFormat: @"%@", [userInfo objectForKey:@"lname"]];

    if (FirstName.length == 0 || [FirstName isEqualToString:@"null"]) {
        FirstName = @"";
    }
    if (lastName.length == 0 || [lastName isEqualToString:@"null"]) {
        lastName = @"";

    }
    
    
    self.userName.text = [NSString stringWithFormat:@"%@ %@", FirstName,lastName ];
  
    //load events
    NSString *eventfound = [userInfo objectForKey:@"eventfound"];
    if ([eventfound isEqualToString:@"no"]){
        _tableView.hidden = YES;
        //self.addPhotoLabel.hidden = NO;
        self.completedEvents.text=@"No event added.";
    }else{
        NSArray *eventRegistered = [[NSArray alloc] init];
        eventRegistered = [userInfo objectForKey:@"events"];
        // user.userNotification = [userInfo objectForKey:@"notifications"];
       // self.addPhotoLabel.hidden = YES;
        self.completedEvents.text=@"Completed Events";
        _tableView.hidden = NO;
        [self loadEvents:eventRegistered];
    }
    
    //see if there is facebook login
    NSString *fbID = [userDefaults objectForKey:kFBID];
    NSString *currentUID = [userDefaults objectForKey:kCurrentUser];
    NSString *mainUserID = [userDefaults objectForKey:kUserID];
                            
    //NSLog(@"%@", [userInfo objectForKey:@"photofd"]);
    //NSLog(@"%@", [userDefaults objectForKey:kFBID]);
    if ([[userInfo objectForKey:@"photofd"] isEqualToString:@"not found"] && (![userDefaults objectForKey:kFBID] ||  mainUserID != currentUID)){
        //create round image
        self.profileImg.image = [UIImage imageNamed:@"add_photo"];
        self.profileImg.layer.cornerRadius = self.profileImg.frame.size.width / 2;
        self.profileImg.clipsToBounds = YES;
        //create round image border
        self.profileImg.layer.borderWidth = 3.0f;
        self.profileImg.layer.borderColor = [UIColor whiteColor].CGColor;
    }else{
        if ([[userInfo objectForKey:@"photofd"] isEqualToString:@"not found"] && [userDefaults objectForKey:kFBID]){
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=normal", fbID]];
            NSLog(@"%@", url);
            NSData  *data = [NSData dataWithContentsOfURL:url];
            self.profileImg.image = [UIImage imageWithData:data];
            self.image = [UIImage imageWithData:data];
        }else{
            NSString *userphoto = [NSString stringWithFormat: @"%@users/%@",BaseURLFOR_IMAGE, [userInfo objectForKey:@"photo"]];
            NSURL *photoURL = [NSURL URLWithString:userphoto];
            NSData *imageData = [NSData dataWithContentsOfURL:photoURL];
            
            self.image = [UIImage imageWithData:imageData];
            self.profileImg.image = [UIImage imageWithData:imageData];
        }
        //blur image
        [self updateImage];
        self.addPhotoLabel.text=@"Change Photo";
        //create round image
        self.profileImg.layer.cornerRadius = self.profileImg.frame.size.width / 2;
        self.profileImg.clipsToBounds = YES;
        self.profileImg.layer.borderWidth = 3.0f;
        self.profileImg.layer.borderColor = [UIColor whiteColor].CGColor;
    }
}
- (void)loadEvents:(NSArray*)eventRegistered{
    
    self.eventList = [NSMutableArray array];
    for(NSDictionary *eventDictionary in eventRegistered){
        // NSLog(@"%@", eventDictionary);
        Event *event = [Event blogPostWithTitle:[eventDictionary objectForKey:@"title"]];
        //   NSLog(@"%@", event.eventTitle);
        
        event.eventID = [eventDictionary objectForKey:@"ID"];
        event.eventTitle = [eventDictionary objectForKey:@"title"];
        event.startDate = [eventDictionary objectForKey:@"sdate"];
        event.points = [eventDictionary objectForKey:@"point"];
        [self.eventList addObject:event];
        // [self.tableView reloadData];
    }
    //NSLog(@"%@", self.eventList);
    [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
    
}


- (void)updateImage{
    UIImage *effectImage = nil;
    effectImage = [self.image applyDarkEffect];
    self.blurImage.image = effectImage;
}

#pragma mark - Table View Setup
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (self.eventList){
        return [self.eventList count];
    }else{
        return 1;
    }
}
-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
#pragma mark - Table Cell Delegation
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UserEventCellVeiw *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (self.eventList){
        //use custom class
        Event *event = [self.eventList objectAtIndex:indexPath.row];
        NSLog(@"%@", event.startDate);
        NSString *eventTitlewDate = [NSString stringWithFormat: @"%@ on %@",  event.eventTitle, event.startDate];
        cell.event_points.text = event.points;
        cell.event_title.text = eventTitlewDate;
    }
    //cell.hospital_address.text = [NSString stringWithFormat:@"%@",hospital.address];
    return cell;
}

//on direct press, trigger segue
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}
- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

#pragma mark - Tab Bar
-(void)tabBarController:(UITabBarController *)tabBar didSelectViewController:(UIViewController *)viewController
{
    
    NSLog(@"Selected index: %lu", (unsigned long)tabBar.selectedIndex);
}


- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
   
    SWRevealViewController *revealController = [self revealViewController];
    if(item.tag == 0){
        UIViewController *newFrontController = nil;
        FitPassInfo *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"fitpassInfoViewController"];
        newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
        [revealController pushFrontViewController:newFrontController animated:YES];

    }else if (item.tag == 2){
        [userDefaults setObject:nil forKey:kEventCat];
        [userDefaults setObject:nil forKey:kEventDate];
        [userDefaults synchronize];
        UIViewController *newFrontController = nil;
        EventList *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"eventListViewController"];
        newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
        [revealController pushFrontViewController:newFrontController animated:YES];
    }else if (item.tag == 3){
        
        UIViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"SwitchAcctViewController"];
        [self presentViewController:vc animated:YES completion:nil];
        
    }
}
#pragma mark - Change Picture
- (IBAction)addPicture:(id)sender {
    
    NSString *actionSheetTitle = @"Add Photo";
    NSString *other1 = @"Take Photo";
    NSString *other2 = @"Choose Photo";
    NSString *cancelTitle = @"Cancel Button";
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:actionSheetTitle
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:other1, other2, nil];
    
    [actionSheet showInView:self.view];
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //Get the name of the current pressed button
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    
    if ([buttonTitle isEqualToString:@"Take Photo"])
    {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:NULL];
    }
    
    if ([buttonTitle isEqualToString:@"Choose Photo"])
    {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:NULL];
    }
    
    if ([buttonTitle isEqualToString:@"Cancel"])
    {
        [actionSheet dismissWithClickedButtonIndex:3 animated:YES];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //[userDefaults setObject:NULL forKey:kProfilePic];
    //[userDefaults synchronize];
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    
    //
   // NSLog(@"Resize");
    UIImage *image = chosenImage;
    UIImage *tempImage = nil;
    CGSize targetSize = CGSizeMake(230,230);
    UIGraphicsBeginImageContext(targetSize);
    
    CGRect thumbnailRect = CGRectMake(0, 0, 0, 0);
    thumbnailRect.origin = CGPointMake(0.0,0.0);
    thumbnailRect.size.width  = targetSize.width;
    thumbnailRect.size.height = targetSize.height;
    
    [image drawInRect:thumbnailRect];
    tempImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    //NSLog(@"Start Upload");
    
    NSData *imageData = UIImageJPEGRepresentation(tempImage, 50);
    NSString *urlString = [NSString stringWithFormat:@"%@photo.php?ID=%@",BaseURL, [userDefaults objectForKey:kCurrentUser]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    //  NSLog(@"%@", urlString);
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"userfile\"; filename=\".jpg\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:imageData]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    
    //NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    //NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
   // NSLog(@"Return String %@", returnString);
    
    //NSLog(@"UPLOADING");
    
    
    [picker dismissViewControllerAnimated:YES completion:^{
        //update background image
        self.image = chosenImage;
        self.profileImg.image = chosenImage;

        [self updateImage];
    }];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}


- (IBAction)goHome:(id)sender {
    
    [userDefaults setObject:nil forKey:kEventCat];
    [userDefaults setObject:nil forKey:kEventDate];    
    [userDefaults synchronize];
    
    SWRevealViewController *revealController = [self revealViewController];
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    UIViewController *newFrontController = nil;
    EventList *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"eventListViewController"];
    newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    [revealController pushFrontViewController:newFrontController animated:YES];
    
}

#pragma mark - SlideNavigationController Methods -
- (void)createMenu{
    
    //create menu
    SWRevealViewController *revealController = [self revealViewController];
    //create the left menu button
    UIButton *button  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [button setImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
    [button addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
    
    UITapGestureRecognizer *viewTapped=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewTapped:)];
    [self.view addGestureRecognizer:viewTapped];
    viewTapped.delegate = self;
    [viewTapped setCancelsTouchesInView:NO];
    
    self.revealViewController.delegate = self;
    [revealController.frontViewController.revealViewController tapGestureRecognizer];
    
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
       shouldReceiveTouch:(UITouch *)touch
{
    if([touch.view class] == self.tableView.class || touch.view == self.addPictureBtn || [touch.view class] == self.tabBar.class){
        return NO;
    }
    return YES;
}
-(void)viewTapped:(UITapGestureRecognizer*)gestureRecognizer
{
    //Do what you want here
    SWRevealViewController *revealController = self.revealViewController;
    if (revealController.frontViewPosition == FrontViewPositionRight) {
        [revealController revealToggleAnimated:YES];
    }
    
}
-(void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position {
    if(position == FrontViewPositionLeft) {
        self.view.userInteractionEnabled = YES;
    } else {
        self.view.userInteractionEnabled = NO;
    }
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position {
    if(position == FrontViewPositionLeft) {
        self.view.userInteractionEnabled = YES;
    } else {
        self.view.userInteractionEnabled = NO;
    }
}
- (void)killHUD
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}
@end
