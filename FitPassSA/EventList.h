//
//  EventList.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/19/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import "ViewController.h"
#import "AppSetting.h"
#import "Event.h"
#import <CoreLocation/CoreLocation.h>
#import <CoreLocation/CLLocationManagerDelegate.h>

@interface EventList : ViewController

@property (strong, nonatomic) IBOutlet UIBarButtonItem *menuBar;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *filterBar;
@property (strong, nonatomic) IBOutlet UIButton *dateForward;
@property (strong, nonatomic) IBOutlet UIButton *dateBackward;
@property (strong, nonatomic) IBOutlet UIButton *dateSelection;
@property (strong, nonatomic) IBOutlet UIButton *byTime;
@property (strong, nonatomic) IBOutlet UIButton *byLocation;
@property (strong, nonatomic) IBOutlet UIButton *byPoints;


@property (nonatomic, strong) NSMutableArray *eventList;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property(strong, nonatomic) NSIndexPath *savedSelectedIndexPath;

@end
