
//
//  Sponsor.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 4/15/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import "SponsorList.h"
#import "Sponsor.h"
#import "SponsorCell.h"
#import "MBProgressHUD.h"
#import "SWRevealViewController.h"
#import "AppSetting.h"

#import "GAIDictionaryBuilder.h"
#import "GAI.h"
#import "GAIFields.h"

#import "SAParknRec.h"
#import "EventLists.h"
#import "Profile.h"
#import "ParkEventList.h"
#import "Header.h"
@interface SponsorList (){
    NSUserDefaults *userDefaults;
}

@end

@implementation SponsorList

@synthesize collectionView = _collectionView;
static NSString * const reuseIdentifier = @"Cell";

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidAppear{
    
    //GA Log
    NSString *str = @"Sponsors";
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:str];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    tracker.allowIDFACollection = YES;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    userDefaults = [[NSUserDefaults alloc] init];
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    
    UIImageView *image=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,105,36)] ;
    //set your image logo replace to the main-logo
    [image setImage:[UIImage imageNamed:@"fp-newlogo"]];
    [self.navigationController.navigationBar.topItem setTitleView:image];
  
    
      // Updated by Navin
//    UIColor *navColor =  [UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:1.0];
//    self.navigationController.navigationBar.barTintColor = navColor;
    
    
    // Register cell classes
   // [self.collectionView registerClass:[SponsorCell class] forCellWithReuseIdentifier:@"Cell"];
    
    [self createMenu];
    
    //define a saved selected IndexPath so we can pass the index between swipe button and direct row press
   // self.savedSelectedIndexPath = nil;
    
    self.collectionLayout = [[UICollectionViewFlowLayout alloc] init];
    [self.collectionLayout setItemSize:CGSizeMake(100, 150)];
    [self.collectionLayout setSectionInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    self.collectionLayout.minimumLineSpacing = 0;
    self.collectionLayout.minimumInteritemSpacing = 0;
    [self.collectionView setCollectionViewLayout:self.collectionLayout];
    //testing to see if the collection view is loading
    self.collectionView.backgroundColor = [UIColor whiteColor];
    
    
    [self loadSponsors];
    
    // Akshay 5 July 2019
    NSMutableArray *newTabs = [NSMutableArray arrayWithArray:self.tapBar.items];
    [newTabs removeObjectAtIndex: 0];
    self.tapBar.items = newTabs;
    
    //////
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) loadSponsors{
    //construct URL
    
    //show progress bar
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        NSString *url = [NSString stringWithFormat: @"%@sponsor.php",BaseURL];
        NSURL *eventURL = [NSURL URLWithString: url];
        
        NSData *jsonData = [NSData dataWithContentsOfURL: eventURL];
        NSError *error = nil;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error: &error];
        // NSLog(@"%@", dataDictionary);
        self.sponsorList = [NSMutableArray array];
        NSArray *sponsorArray = [dataDictionary objectForKey:@"fitpass"];
        // NSString *eventFound = [dataDictionary objectForKey:@"notfound"];
        //if nothing is found
        if ([dataDictionary objectForKey:@"notfound"]){
            
        }else{
            for(NSDictionary *sponsorDictionary in sponsorArray){
                Sponsor *sponsor = [Sponsor blogPostWithTitle:[sponsorDictionary objectForKey:@"name"]];
                
                sponsor.sponsorID = [sponsorDictionary objectForKey:@"ID"];
                sponsor.sponsorTitle = [sponsorDictionary objectForKey:@"name"];
                sponsor.sponsorURL = [sponsorDictionary objectForKey:@"url"];
                sponsor.sponsorPhoto = [sponsorDictionary objectForKey:@"image"];
                sponsor.sponsorOrder = [sponsorDictionary objectForKey:@"order"];
                [self.sponsorList addObject:sponsor];
                [_collectionView reloadData];
            }
        }
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });
   
    

}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
   
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.sponsorList count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    SponsorCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    // Configure the cell
    Sponsor *sponsor = [self.sponsorList objectAtIndex:indexPath.item];
    cell.sTitle.text = sponsor.sponsorTitle;
   // NSLog(@"title: %@",  sponsor.sponsorTitle);
    //get image
    if ([sponsor.sponsorPhoto isEqualToString:@""]){
        cell.sImg.hidden = YES;
    }else{
        NSData *imageData = [NSData dataWithContentsOfURL:sponsor.thumbnailURL];
        cell.sImg.image = [UIImage imageWithData:imageData];
        cell.sImg.hidden = NO;
    }
    return cell;
}

/*
 // Uncomment this method to specify if the specified item should be highlighted during tracking
 - (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
 }
 */

/*
 // Uncomment this method to specify if the specified item should be selected
 - (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
 return YES;
 }
 */
- (IBAction)goHome:(id)sender {
    
    [userDefaults setObject:nil forKey:kEventCat];
    [userDefaults setObject:nil forKey:kEventDate];    
    [userDefaults synchronize];
    
    SWRevealViewController *revealController = [self revealViewController];
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    UIViewController *newFrontController = nil;
    EventLists *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"eventListViewController"];
    newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    [revealController pushFrontViewController:newFrontController animated:YES];
    
}

#pragma mark - SlideNavigationController Methods -
- (void)createMenu{
    
    //create menu
    SWRevealViewController *revealController = [self revealViewController];
    //create the left menu button
    UIButton *button  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [button setImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
    [button addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
    UITapGestureRecognizer *viewTapped=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewTapped:)];
    [self.view addGestureRecognizer:viewTapped];
    viewTapped.delegate = self;
    [viewTapped setCancelsTouchesInView:NO];
    
    self.revealViewController.delegate = self;
    [revealController.frontViewController.revealViewController tapGestureRecognizer];
    
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
        shouldReceiveTouch:(UITouch *)touch
{
    if([touch.view class] == self.tapBar.class || [touch.view class] ==[self.collectionView class]){
        return NO;
    }
    return YES;
}
-(void)viewTapped:(UITapGestureRecognizer*)gestureRecognizer
{
    //Do what you want here
    SWRevealViewController *revealController = self.revealViewController;
    if (revealController.frontViewPosition == FrontViewPositionRight) {
        [revealController revealToggleAnimated:YES];
    }
}
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    SWRevealViewController *revealController = [self revealViewController];
    UIViewController *newFrontController = nil;
    if(item.tag == 0){
        EventLists *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"eventListViewController"];
        newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    }else if (item.tag == 1){
        ParkEventList *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"parkEventListViewController"];
        newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    }else if (item.tag == 2){
        SAParknRec *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"saparkViewController"];
        newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    }else if (item.tag == 3){
        UIViewController *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"accountViewController"];
        newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    }
    
    [revealController pushFrontViewController:newFrontController animated:YES];
    // NSLog(@"Selected index: %lu", item.tag);
}
@end
