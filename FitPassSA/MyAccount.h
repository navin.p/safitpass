//
//  MyAccount.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 5/5/17.
//  Copyright © 2017 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImage+ImageEffects.h"
#import "Event.h"
#import "SWRevealViewController.h"
#import "UINavigation+NavBar.h"

#import "SWRevealViewController.h"

@interface MyAccount :  UIViewController <UITableViewDelegate, UITableViewDataSource, UITabBarDelegate, UIGestureRecognizerDelegate, SWRevealViewControllerDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *blurImage;
@property (strong, nonatomic) IBOutlet UIImageView *profileImg;
@property (strong, nonatomic) IBOutlet UILabel *userPoint;
@property (strong, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *featuredCT;
@property (weak, nonatomic) IBOutlet UILabel *fitnessCT;
@property (weak, nonatomic) IBOutlet UILabel *volunteeringCT;
@property (weak, nonatomic) IBOutlet UILabel *nutritionCT;
@property (weak, nonatomic) IBOutlet UILabel *weightLost;

#pragma tab bar
@property (strong, nonatomic) IBOutlet UITabBar *tabBar;
@property (strong, nonatomic) IBOutlet UITabBarItem *aboutSAPR;
@property (strong, nonatomic) IBOutlet UITabBarItem *viewAcct;
@property (strong, nonatomic) IBOutlet UITabBarItem *viewFP;
@property (strong, nonatomic) IBOutlet UITabBarItem *viewFIP;

#pragma event outlets
@property (strong, nonatomic) IBOutlet UITableView *familyView;
@property (strong, nonatomic) IBOutlet UIButton *addPictureBtn;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *viewTapped;
@property (weak, nonatomic) IBOutlet UIButton *editProfile;

@property(strong, nonatomic) NSIndexPath *savedSelectedIndexPath;

#pragma Variables
@property (nonatomic) UIImage *image;
@property (nonatomic) int imageIndex;
@property (nonatomic, strong) NSMutableArray *userProfile;
@property (nonatomic, strong) NSMutableArray *familyList;


#pragma actions
- (IBAction)addPicture:(id)sender;
- (IBAction)editProfileAct:(id)sender;
- (IBAction)addFamily:(id)sender;
- (IBAction)viewPoints:(id)sender;

@end
