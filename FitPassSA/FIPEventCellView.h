//
//  FIPEventCellView.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 6/6/16.
//  Copyright © 2016 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FIPEventCellView : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *event_title;
@property (strong, nonatomic) IBOutlet UILabel *event_address;
@property (strong, nonatomic) IBOutlet UILabel *event_time;
@property (strong, nonatomic) IBOutlet UIImageView *event_photo;
@property (strong, nonatomic) IBOutlet UIView *cellView;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *imageHeight;


@end
