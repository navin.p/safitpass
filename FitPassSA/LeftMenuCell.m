//
//  LeftMenuCell.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/23/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import "LeftMenuCell.h"

@implementation LeftMenuCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
