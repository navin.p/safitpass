//
//  Login.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/10/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface Login : UIViewController <UITextFieldDelegate> {
}

@property (strong, nonatomic) IBOutlet UITextField *emailAddressFd;
@property (strong, nonatomic) IBOutlet UITextField *passwordFd;

@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (strong, nonatomic) IBOutlet UIButton *pwResetBtn;
@property (weak, nonatomic) IBOutlet UIButton *needAccountBtn;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *viewTap;
@property (strong, nonatomic) IBOutlet FBSDKLoginButton *fbLoginBtn;
@property (strong, nonatomic) IBOutlet UIButton *terms;

- (IBAction)login:(id)sender;
- (IBAction)needAcctAction:(id)sender;
- (IBAction)passwordReset:(id)sender;

@property (nonatomic, assign) BOOL slideOutAnimationEnabled;

@end
