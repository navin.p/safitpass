//
//  EventSearch.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 4/7/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import "EventSearch.h"
#import "AppSetting.h"
#import "SWRevealViewController.h"
#import "Event.h"
#import "EventCellView.h"
#import "EventDetail.h"
#import "SWTableViewCell.h"
#import "EventList.h"
#import "GAIDictionaryBuilder.h"
#import "GAI.h"
#import "GAIFields.h"
#import "Header.h"
@interface EventSearch (){
    NSUserDefaults *userDefaults;
}

@end

@implementation EventSearch
@synthesize searchFld;
@synthesize tableView;

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    //GA Log
    NSString *str = @"Event Search";
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:str];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    tracker.allowIDFACollection = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    userDefaults = [[NSUserDefaults alloc] init];
    userDefaults = [NSUserDefaults standardUserDefaults];
    //searchFld.delegate = self;
    
    // Updated by Navin
//    UIColor *navColor =  [UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:1.0];
//    self.navigationController.navigationBar.barTintColor = navColor;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
//
    tableView.hidden = YES;
    self.instructionTxt.hidden = NO;
    self.noResultTxt.hidden = YES;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [searchFld resignFirstResponder];
}
#pragma mark - Event Search
//load events
- (void)loadEvents:(NSString*)searchKeyword{
    
    //construct URL
    NSString *userID = [userDefaults objectForKey:kUserID];
    NSString *url = [NSString stringWithFormat: @"%@searchEvents.php?keyword=%@&uID=%@",BaseURL, searchKeyword,  userID];
    NSURL *eventURL = [NSURL URLWithString: url];
    NSLog(@"%@", eventURL);
    NSData *jsonData = [NSData dataWithContentsOfURL: eventURL];
    NSError *error = nil;
    NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error: &error];
   // NSLog(@"%@", dataDictionary);
    
    self.eventList = [NSMutableArray array];
    NSArray *eventArray = [dataDictionary objectForKey:@"fitpass"];
    // NSString *eventFound = [dataDictionary objectForKey:@"notfound"];
    //if nothing is found
    if ([dataDictionary objectForKey:@"notfound"]){
        tableView.hidden = YES;
        self.instructionTxt.hidden = YES;
        self.noResultTxt.hidden = NO;
    }else{
        tableView.hidden = NO;
        self.instructionTxt.hidden = YES;
        self.noResultTxt.hidden = YES;
        for(NSDictionary *eventDictionary in eventArray){
            Event *event = [Event blogPostWithTitle:[eventDictionary objectForKey:@"name"]];
            
            event.eventID = [eventDictionary objectForKey:@"ID"];
            event.eventTitle = [eventDictionary objectForKey:@"title"];
            event.startDate = [eventDictionary objectForKey:@"sdate"];
            event.startTime = [eventDictionary objectForKey:@"stime"];
            event.endTime = [eventDictionary objectForKey:@"etime"];
            event.ckTime = [eventDictionary objectForKey:@"ctime"];
            event.eventDescription = [eventDictionary objectForKey:@"description"];
            event.points = [eventDictionary objectForKey:@"point"];
            event.fitLevel = [eventDictionary objectForKey:@"fitLevel"];
            event.eventPhoto = [eventDictionary objectForKey:@"photo"];
            event.categories = [eventDictionary objectForKey:@"category"];
            event.locationInfo = [eventDictionary objectForKey:@"location"];
            event.address = [eventDictionary objectForKey:@"address"];
            event.eventTime = [eventDictionary objectForKey:@"event_time"];
            event.loclat = [eventDictionary objectForKey:@"loc_lat"];
            event.loclong = [eventDictionary objectForKey:@"loc_long"];
            event.locName = [eventDictionary objectForKey:@"loc_name"];
            event.iCalID = [eventDictionary objectForKey:@"iCalID"];
            event.userPt = [eventDictionary objectForKey:@"user_pt"];
            
            [self.eventList addObject:event];
            [self.tableView reloadData];
        }
        NSLog(@"%@", self.eventList);
    }
  //  [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
}

#pragma mark - Table View Setup
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
   // NSLog(@"Event Count %lu", (unsigned long)[self.eventList count]);
    if (self.eventList){
        return [self.eventList count];
    }else{
        return 1;
    }
}

#pragma mark - Table Cell Delegation
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    EventCellView *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
   
    //convert date format
    if (self.eventList){
        //use custom class
        Event *event = [self.eventList objectAtIndex:indexPath.row];
        cell.event_points.text = [NSString stringWithFormat:@"%@ points",event.points];
        
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *date = [dateFormatter dateFromString:event.startDate];
        [dateFormatter setDateFormat:@"EEEE, MMMM dd"];
        NSString *newDateString = [dateFormatter stringFromDate:date];
        
        NSString *eventtime;
        if ([event.startTime isEqualToString: event.endTime]){
            eventtime = @"All Day Event";
        }else{
            NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
            [dateFormatter2 setDateFormat:@"HH:mm:ss"];
            NSDate *date2 = [dateFormatter2 dateFromString:event.startTime];
            [dateFormatter2 setDateFormat:@"hh:mm a"];
            eventtime = [dateFormatter2 stringFromDate:date2];
            
            //NSLog(@"%@", event.endTime);
            if (![event.endTime isEqualToString:@"00:00:00"]){
                NSDateFormatter *dateFormatter3 = [[NSDateFormatter alloc] init];
                [dateFormatter3 setDateFormat:@"HH:mm:ss"];
                NSDate *date3 = [dateFormatter3 dateFromString:event.endTime];
                [dateFormatter3 setDateFormat:@"hh:mm a"];
                eventtime =  [NSString stringWithFormat:@"%@ - %@",eventtime, [dateFormatter3 stringFromDate:date3]];
            }
        }
        cell.event_time.text = [NSString stringWithFormat:@"%@, %@",newDateString, eventtime];
        cell.event_title.text = event.eventTitle;
        
        if ([event.points isEqualToString:@"0"]){
            cell.event_points.hidden = YES;
        }
        
    }
    //cell.hospital_address.text = [NSString stringWithFormat:@"%@",hospital.address];
    return cell;
}


//on direct press, trigger segue
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.savedSelectedIndexPath = [self.tableView indexPathForSelectedRow];
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
  //  [self performSegueWithIdentifier:@"searchDetail" sender:self];
}

#pragma mark - Segue Process Navigation
-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSLog(@"Preparing for seque %@", segue.identifier);
    
    if ([segue.identifier isEqualToString:@"searchDetail"]){
        
        self.savedSelectedIndexPath = [self.tableView indexPathForSelectedRow];
        
        NSLog(@"SavedPath %@", _savedSelectedIndexPath);
        //self.savedSelectedIndexPath = [self.tableView indexPathForSelectedRow];
        NSIndexPath *indexPath =  self.savedSelectedIndexPath; //get the index path of the row selected
        
        Event *event = [self.eventList objectAtIndex:indexPath.row]; // now get the content of that row
        EventDetail *eventDetailView = (EventDetail *)segue.destinationViewController;
        eventDetailView.eventDetail = event;
        
    }
}

#pragma mark - Text Field Editing


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    

}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [textView becomeFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    return YES;
}

- (IBAction)goHome:(id)sender {
    
    UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SWRevealViewController *revealVC = [main instantiateViewControllerWithIdentifier:@"swreveal"];
    
    [self presentViewController:revealVC animated:YES completion:nil];

}

- (IBAction)submitSearch:(id)sender {
    int myLength = [searchFld.text length];
    NSLog(@"Count: %d", myLength);
    //if ([searchFld.text isEqualToString:@""]){
    if (myLength < 5){
        return;
    }else{
        NSString *keywordClean = [searchFld.text stringByReplacingOccurrencesOfString:@" " withString:@"_"];
        NSLog(@"Keywords: %@",searchFld.text);
        NSLog(@"Keyword Clean: %@",keywordClean);
        [self loadEvents:keywordClean];
    }
}
@end
