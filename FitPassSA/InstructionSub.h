//
//  InstructionSub.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 4/17/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InstructionSub : UIViewController

@property (assign, nonatomic) NSInteger index;
@property (strong, nonatomic) IBOutlet UIImageView *stepImage;

@end
