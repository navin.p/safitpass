//
//  LeftMenuCell.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/23/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftMenuCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *leftMenuLabel;
@property (strong, nonatomic) IBOutlet UIImageView *leftMenuIcon;
@property (strong, nonatomic) IBOutlet UIImageView *leftMenuArrow;

@end
