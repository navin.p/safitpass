//
//  MyAccount.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 5/5/17.
//  Copyright © 2017 Ping-jung Tsai. All rights reserved.
//

#import "MyAccount.h"
#import "AppSetting.h"
#import "SWRevealViewController.h"
#import "Family.h"
#import "FamilyCell.h"
#import "AddFamily.h"
#import "GAIDictionaryBuilder.h"
#import "GAI.h"
#import "GAIFields.h"
#import "MBProgressHUD.h"

#import "SWTableViewCell.h"
#import "UINavigation+NavBar.h"
#import "AFNetworking.h"
#import "Header.h"

@interface MyAccount (){
    NSUserDefaults *userDefaults;
}
@end
    
@implementation MyAccount

@synthesize userProfile;
@synthesize familyView = _familyView;

- (void)viewWillAppear{
    
    NSString *str = @"My Account";
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:str];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    tracker.allowIDFACollection = YES;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //Layout adjustment
    self.tabBar.autoresizesSubviews = NO;
    self.tabBar.clipsToBounds = YES;
    
    //add logo to navigation bar
    UIImageView *image=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,105,36)] ;
    //set your image logo replace to the main-logo
    [image setImage:[UIImage imageNamed:@"fp-newlogo"]];
    [self.navigationController.navigationBar.topItem setTitleView:image];
 
      // Updated by Navin
//    UIColor *navColor =  [UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:1.0];
//    self.navigationController.navigationBar.barTintColor = navColor;
    _familyView.backgroundColor = [UIColor clearColor];
    [self createMenu];
    
    //first get users
    userDefaults = [[NSUserDefaults alloc] init];
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    //show progress bar
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self loadProfile];
        [self killHUD];
        //[MBProgressHUD hideHUDForView:self.view animated:YES];
    });
    
    // Akshay 5 July 2019
    NSMutableArray *newTabs = [NSMutableArray arrayWithArray:self.tabBar.items];
    [newTabs removeObjectAtIndex: 0];
    self.tabBar.items = newTabs;
    
    //////

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Load Profile
- (void)loadProfile{
    NSString *userID = [userDefaults objectForKey:kUserID];
    //then get the data
    NSString *url = [NSString stringWithFormat: @"%@myAccount.php?uid=%@",BaseURL, userID];
    NSURL *eventURL = [NSURL URLWithString: url];
  //  NSLog(@"%@", url);
    NSData *jsonData = [NSData dataWithContentsOfURL: eventURL];
    NSError *error = nil;
    NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error: &error];
    
    NSDictionary *userInfo = [dataDictionary objectForKey:@"fitpass"];
   // NSLog(@"%@", userInfo);
    // UserInfo *user = [UserInfo blogPostWithID:userID];
    //user.userPhoto = [userInfo objectForKey:@"photo"];
    //  NSNumber *userPoints = [userInfo objectForKey:@"points"];
//    self.userPoint.text = [userInfo objectForKey:@"points"];
    self.userPoint.text = @"0";
    self.weightLost.text = [userInfo objectForKey:@"weight_lost"];
    self.featuredCT.text= [userInfo objectForKey:@"featured_ct"];
    self.nutritionCT.text= [userInfo objectForKey:@"nutrition_ct"];
    self.fitnessCT.text= [userInfo objectForKey:@"fitness_ct"];
    self.volunteeringCT.text= [userInfo objectForKey:@"volunteer_ct"];

    
    // NSNumber *weeklyPoints = [userInfo objectForKey:@"wkypts"];
    // self.weeklyPt.text = [weeklyPoints stringValue];
    NSString *fullname = [NSString stringWithFormat: @"%@ %@",  [userInfo objectForKey:@"fname"],  [userInfo objectForKey:@"lname"]];
    self.userName.text = fullname;
    //load events
    
    //see if there is facebook login
    NSString *fbID = [userDefaults objectForKey:kFBID];
    //NSLog(@"%@", [userInfo objectForKey:@"photofd"]);
    //NSLog(@"%@", [userDefaults objectForKey:kFBID]);
    if ([[userInfo objectForKey:@"photofd"] isEqualToString:@"not found"] && ![userDefaults objectForKey:kFBID]){
        //create round image
        self.profileImg.image = [UIImage imageNamed:@"add_photo"];
        self.profileImg.layer.cornerRadius = self.profileImg.frame.size.width / 2;
        self.profileImg.clipsToBounds = YES;
        //create round image border
        self.profileImg.layer.borderWidth = 3.0f;
        self.profileImg.layer.borderColor = [UIColor whiteColor].CGColor;
    }else{
        if ([[userInfo objectForKey:@"photofd"] isEqualToString:@"not found"]){
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=normal", fbID]];
           // NSLog(@"%@", url);
            NSData  *data = [NSData dataWithContentsOfURL:url];
            self.profileImg.image = [UIImage imageWithData:data];
            self.image = [UIImage imageWithData:data];
        }else{
            NSString *userphoto = [NSString stringWithFormat: @"%@users/%@",BaseURLFOR_IMAGE, [userInfo objectForKey:@"photo"]];
            NSURL *photoURL = [NSURL URLWithString:userphoto];
            NSData *imageData = [NSData dataWithContentsOfURL:photoURL];
            
            self.image = [UIImage imageWithData:imageData];
            self.profileImg.image = [UIImage imageWithData:imageData];
        }
        //blur image
        [self updateImage];
        //create round image
        self.profileImg.layer.cornerRadius = self.profileImg.frame.size.width / 2;
        self.profileImg.clipsToBounds = YES;
        self.profileImg.layer.borderWidth = 3.0f;
        self.profileImg.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    
    //load family
    if([[userInfo objectForKey:@"kidsfd"] isEqualToString:@"No"])  {
        self.familyView.hidden = YES;
    }else{
        NSArray *familyMember = [[NSArray alloc] init];
        familyMember = [userInfo objectForKey:@"kids"];
        // user.userNotification = [userInfo objectForKey:@"notifications"];
        // self.addPhotoLabel.hidden = YES;
        self.familyView.hidden = NO;
        [self loadFamily:familyMember];
    }
}
- (void)loadFamily:(NSArray*)familyMember{
    
    self.familyList = [NSMutableArray array];
    for(NSDictionary *memberInfo in familyMember){
        // NSLog(@"%@", eventDictionary);
        Family *member = [Family blogPostWithID:[memberInfo objectForKey:@"ID"]];
        //   NSLog(@"%@", event.eventTitle);
        member.userFirstName = [memberInfo objectForKey:@"fname"];
        member.userLastName = [memberInfo objectForKey:@"lname"];
        member.userPoints = [memberInfo objectForKey:@"Total_Pt"];
        member.userFirstName = [memberInfo objectForKey:@"fname"];
        member.weightLost = [memberInfo objectForKey:@"weight_lost"];
        member.userPhoto = [memberInfo objectForKey:@"photo"];
        [self.familyList addObject:member];
        // [self.tableView reloadData];
    }
    //NSLog(@"%@", self.eventList);
    [self.familyView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
    
}
#pragma mark - Table View Setup
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
     if (self.familyList){
        return [self.familyList count];
    }else{
        return 1;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 8.f; // you can have your own choice, of course
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}
-(void)viewDidLayoutSubviews
{
    if ([self.familyView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.familyView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.familyView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.familyView setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
#pragma mark - Table Cell Delegation
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    FamilyCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    //set right buttons
    [cell setRightUtilityButtons:[self rightButtons] WithButtonWidth:58.0f];
    cell.delegate = self;
    
    cell.layer.cornerRadius = 10;
    cell.layer.masksToBounds = YES;
    
    if (self.familyView){
        //use custom class
        Family *family = [self.familyList objectAtIndex:indexPath.section];
        cell.memberName.text = [NSString stringWithFormat: @"%@ %@", family.userFirstName, family.userLastName];
        cell.weightPoint.text = [NSString stringWithFormat: @"%@ points | %@ lbs lost",  family.userPoints, family.weightLost];
        
        NSLog(@"%@", family.userPhoto);
        if (![family.userPhoto isEqualToString:@""]){
            NSData *imageData = [NSData dataWithContentsOfURL:family.thumbnailURL];
            cell.memberPhoto.image = [UIImage imageWithData:imageData];
        }else if([family.userPhoto isEqualToString:@""]){
            cell.memberPhoto.image = [UIImage imageNamed:@"default-photo"];
        }
        cell.memberPhoto.layer.cornerRadius = cell.memberPhoto.frame.size.width / 2;
        cell.memberPhoto.clipsToBounds = YES;
      
    }
    //cell.hospital_address.text = [NSString stringWithFormat:@"%@",hospital.address];
    return cell;
}

//on direct press, trigger segue
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    /*  MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
     progressHUD.labelText = @"Loading...";
     progressHUD.mode = MBProgressHUDAnimationFade;
     */
    
    self.savedSelectedIndexPath = [self.familyView indexPathForSelectedRow];
    
    //NSLog(@"Saved Path %@", _savedSelectedIndexPath);
    
    [self.familyView deselectRowAtIndexPath:indexPath animated:YES];
    [self performSegueWithIdentifier:@"showDetail" sender:self];
}

#pragma mark - Segue Process Navigation
-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    
    if ([segue.identifier isEqualToString:@"showDetail"]){
        
        [self killHUD];
        
        //self.savedSelectedIndexPath = [self.familyView indexPathForSelectedRow];
        
        //NSLog(@"SavedPath %@", _savedSelectedIndexPath);
        //self.savedSelectedIndexPath = [self.tableView indexPathForSelectedRow];
        NSIndexPath *indexPath =  self.savedSelectedIndexPath; //get the index path of the row selected
        
        Family *familyUpdate = [self.familyList objectAtIndex:indexPath.section]; // now get the content of that row
        AddFamily *familyDetailView = (AddFamily *)segue.destinationViewController;
        
        familyDetailView.userInfo = familyUpdate;
        
    }
}


- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Add Picture
- (IBAction)addPicture:(id)sender {
    
    NSString *actionSheetTitle = @"Add Photo";
    NSString *other1 = @"Take Photo";
    NSString *other2 = @"Choose Photo";
    NSString *cancelTitle = @"Cancel Button";
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:actionSheetTitle
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:other1, other2, nil];
    
    [actionSheet showInView:self.view];
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //Get the name of the current pressed button
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    
    if ([buttonTitle isEqualToString:@"Take Photo"])
    {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:NULL];
    }
    
    if ([buttonTitle isEqualToString:@"Choose Photo"])
    {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:NULL];
    }
    
    if ([buttonTitle isEqualToString:@"Cancel"])
    {
        [actionSheet dismissWithClickedButtonIndex:3 animated:YES];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //[userDefaults setObject:NULL forKey:kProfilePic];
    //[userDefaults synchronize];
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    
    self.profileImg.image = chosenImage;
    
    UIImage *image = chosenImage;
    UIImage *tempImage = nil;
    CGSize targetSize = CGSizeMake(230,230);
    UIGraphicsBeginImageContext(targetSize);
    
    CGRect thumbnailRect = CGRectMake(0, 0, 0, 0);
    thumbnailRect.origin = CGPointMake(0.0,0.0);
    thumbnailRect.size.width  = targetSize.width;
    thumbnailRect.size.height = targetSize.height;
    
    [image drawInRect:thumbnailRect];
    
    tempImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    NSData *imageData = UIImageJPEGRepresentation(tempImage, 50);
    NSString *urlString = [NSString stringWithFormat:@"%@photo.php?ID=%@",BaseURL, [userDefaults objectForKey:kUserID]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    //  NSLog(@"%@", urlString);
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"userfile\"; filename=\".jpg\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:imageData]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    NSLog(@"Return String %@", returnString);
    
  //  NSLog(@"UPLOADING");
    
    [picker dismissViewControllerAnimated:YES completion:^{
        //update background image
        self.image =chosenImage;
        [self updateImage];
    }];

}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
- (void)updateImage{
    UIImage *effectImage = nil;
    effectImage = [self.image applyDarkEffect];
    self.blurImage.image = effectImage;
}


#pragma mark - Button Functions

- (IBAction)editProfileAct:(id)sender {
    SWRevealViewController *revealController = [self revealViewController];
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    UIViewController *newFrontController = nil;
    UIViewController *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"UserdataViewController"];
    newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    [revealController pushFrontViewController:newFrontController animated:YES];
}

- (IBAction)addFamily:(id)sender {
    NSString *userID = [userDefaults objectForKey:kUserID];
    [userDefaults setObject:@"No" forKey:kISParent];
    [userDefaults setObject:userID forKey:kParentID];
    [userDefaults setObject:@"Yes" forKey:kNewKid];
    [userDefaults synchronize];
    
    //[self performSegueWithIdentifier:@"showDetail" sender:self];
    
    SWRevealViewController *revealController = [self revealViewController];
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    UIViewController *newFrontController = nil;
    AddFamily *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"addFamilyViewController"];
    newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    [revealController pushFrontViewController:newFrontController animated:YES];
   
}

- (IBAction)viewPoints:(id)sender {
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    SWRevealViewController *revealController = [self revealViewController];
    UIViewController *newFrontController = nil;
    UIViewController *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    [revealController pushFrontViewController:newFrontController animated:YES];
}

#pragma mark - SlideNavigationController Methods -
- (void)createMenu{
    
    //create menu
    SWRevealViewController *revealController = [self revealViewController];
    //create the left menu button
    UIButton *button  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [button setImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
    [button addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
    
    UITapGestureRecognizer *viewTapped=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewTapped:)];
    [self.view addGestureRecognizer:viewTapped];
    viewTapped.delegate = self;
    [viewTapped setCancelsTouchesInView:NO];
    
    self.revealViewController.delegate = self;
    [revealController.frontViewController.revealViewController tapGestureRecognizer];
    
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
       shouldReceiveTouch:(UITouch *)touch
{
    //NSLog(@"%@", [touch.view class]);
    if([touch.view class] == self.familyView.class || touch.view == self.addPictureBtn || [touch.view class] == self.tabBar.class || [touch.view class] == [UITableViewCell class] ){
       // NSLog(@"TOUCHED");
        return NO;
    }else{
       // NSLog(@"nottouched");
    }
    return YES;
}
-(void)viewTapped:(UITapGestureRecognizer*)gestureRecognizer
{
    //Do what you want here
    SWRevealViewController *revealController = self.revealViewController;
    if (revealController.frontViewPosition == FrontViewPositionRight) {
        [revealController revealToggleAnimated:YES];
    }
    
}
-(void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position {
    if(position == FrontViewPositionLeft) {
        self.view.userInteractionEnabled = YES;
    } else {
        self.view.userInteractionEnabled = NO;
    }
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position {
    if(position == FrontViewPositionLeft) {
        self.view.userInteractionEnabled = YES;
    } else {
        self.view.userInteractionEnabled = NO;
    }
}


#pragma mark - Swipe Button on Cell

#pragma mark - Swipe Utility Buttons
- (NSArray *)rightButtons
{
    
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:191.0/255.0 green:16.0/255.0 blue:45.0/255.0 alpha:1.0]
                                                 icon:[UIImage imageNamed:@"delete_white"]];
    
    return rightUtilityButtons;
}
#pragma mark - SWTableViewDelegate
- (void)swipeableTableViewCell:(SWTableViewCell *)cell scrollingToState:(SWCellState)state
{
    switch (state) {
        case 0:
            NSLog(@"utility buttons closed");
            break;
        case 1:
            NSLog(@"left utility buttons open");
            break;
        case 2:
            NSLog(@"right utility buttons open");
            break;
        default:
            break;
    }
}
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    switch (index) {
        case 0:
        {
            //get directions, first get the row
            NSIndexPath *cellIndexPath = [self.familyView indexPathForCell:cell];
            self.savedSelectedIndexPath = cellIndexPath;
            Family *family = [self.familyList objectAtIndex:cellIndexPath.section];
            NSString *userID = family.userID;
            
            //compile the URL
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            manager.requestSerializer = [AFHTTPRequestSerializer serializer];
            manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
            NSDictionary *params =  [[NSDictionary alloc] initWithObjectsAndKeys:
                                      userID, @"uid",
                                     @"Remove", @"func",
                                     nil] ;
            
            //PROCESS IT
            
            
            
            
            [manager POST:[NSString stringWithFormat:@"%@addFamily.php",BaseURL] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                //is this successful or is it a duplicate entries
                NSString *returnvalue = [NSString stringWithFormat:@"%@",[responseObject objectAtIndex:0]];
                if ([returnvalue isEqualToString:@"Removed"]){
                    NSLog(@"%@", returnvalue);
                    
                    [self loadProfile];
                    
                    [self.familyView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
                    
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Updated" message:@"This family member has been removed." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alertView show];
                    [cell hideUtilityButtonsAnimated:YES];
                }
                return;
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                NSLog(@"Error: %@", error.localizedDescription);
            }];
            break;
        }
        case 1:
        {
            break;
        }
        default:
            break;
    }
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell
{
    // allow just one cell's utility button to be open at once
    return YES;
}

- (BOOL)swipeableTableViewCell:(SWTableViewCell *)cell canSwipeToState:(SWCellState)state
{
    
    // NSLog(@"%@", state);
    switch (state) {
        case 1:
            return YES;
            break;
        default:
            break;
    }
    
    return YES;
}
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    SWRevealViewController *revealController = [self revealViewController];
    UIViewController *newFrontController = nil;
    if(item.tag == 0){
        UIViewController *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"eventListViewController"];
        newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    }else if (item.tag == 1){
        UIViewController *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"parkEventListViewController"];
        newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    }else if (item.tag == 2){
        UIViewController *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"saparkViewController"];
        newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    }else if (item.tag == 3){
        return;
    }
    
    [revealController pushFrontViewController:newFrontController animated:YES];
    // NSLog(@"Selected index: %lu", item.tag);
}
- (void)killHUD
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

@end
