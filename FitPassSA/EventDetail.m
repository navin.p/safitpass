//
//  EventDetail.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/24/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import "EventDetail.h"
#import <MapKit/MapKit.h>
#import "Event.h"
#import "JPSThumbnailAnnotation.h"
#import "UIViewController+NavigationBack.h"
#import "LogPoint.h"
#import "SWRevealViewController.h"
#import "AppSetting.h"
#import "NSObject+setNotification.h"
#import "LogPoint.h"

#import "GAIDictionaryBuilder.h"
#import "GAI.h"
#import "GAIFields.h"
#import "Header.h"

@interface EventDetail ()<MKMapViewDelegate>{
    
    NSUserDefaults *userDefaults;
}
@end
@implementation EventDetail
@synthesize eventDetail;
@synthesize eventMapIB;

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    //GA Log    
    NSString *str = [NSString stringWithFormat:@"Event Detail - %@", eventDetail.eventTitle];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:str];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    tracker.allowIDFACollection = YES;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    userDefaults = [[NSUserDefaults alloc] init];
    userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:eventDetail.eventID forKey:kEventID];
    [userDefaults synchronize];
    
    // Do any additional setup after loading the view.
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:eventDetail.startDate];
    [dateFormatter setDateFormat:@"EEEE, MMMM dd"];
    
    self.eventLocationName.text = [NSString stringWithFormat:@"%@ at %@",[dateFormatter stringFromDate:date], eventDetail.locName];
    self.eventTitleIB.text = eventDetail.eventTitle;
    self.eventLocationIB.text =  eventDetail.address;
    self.eventTimeIB.text = eventDetail.eventTime;
    self.eventPointsIB.text = eventDetail.points;
    self.pointRuleIB.text = [NSString stringWithFormat: @"Single Event is worth %@ points", eventDetail.points];
    //self.eventDetailIB.text = eventDetail.eventDescription;
    
    NSMutableParagraphStyle *style  = [[NSMutableParagraphStyle alloc] init];
    style.minimumLineHeight = 18.f;
    style.maximumLineHeight = 18.f;
    NSDictionary *attributtes = @{NSParagraphStyleAttributeName : style,};
    self.eventDetailIB .attributedText = [[NSAttributedString alloc] initWithString:eventDetail.eventDescription
                                                                         attributes:attributtes];
    
    //if the event is already logged, then hide log button
    NSLog(@"Event Points: %@", eventDetail.points);
    self.ptLogged.hidden = YES;
    if (![eventDetail.userPt isEqualToString:@"NA"]){
        self.pointBtn.hidden = YES;
        self.getPtHeight.constant = 20;
       self.ptLogged.hidden = NO;
    }else{
        self.pointBtn.hidden = NO;
        self.ptLogged.hidden = YES;
    }
    //hide get point if this is a FIP event
    if ([eventDetail.points isEqualToString:@"0"]){
        self.pointBtn.hidden = YES;
        self.ptLogged.hidden = YES;
        self.pointBkbd.hidden = YES;
        self.pointRule.hidden = YES;
        self.pointRuleIB.hidden = YES;
        self.pointTXT.hidden = YES;
        self.getPtHeight.constant = 0;

        //self.noPtHeight.constant = -150.f;
    }
    //if there is a badge
    if ([eventDetail.badge isEqualToString:@""]){
        self.badgeView.hidden = YES;
        self.badgeIcon.hidden = YES;
    }else if ([eventDetail.badge isEqualToString:@"1"]){
        self.badgeView.hidden = NO;
        self.badgeIcon.image = [UIImage imageNamed:@"badge_featured_circle"];
    }else if ([eventDetail.badge isEqualToString:@"2"]){
        self.badgeView.hidden = NO;
        self.badgeIcon.image = [UIImage imageNamed:@"badge_nutrition_circle"];
    }else if ([eventDetail.badge isEqualToString:@"3"]){
        self.badgeView.hidden = NO;
        self.badgeIcon.image = [UIImage imageNamed:@"badge_fitness_circle"];
    }else if ([eventDetail.badge isEqualToString:@"4"]){
        self.badgeView.hidden = NO;
        self.badgeIcon.image = [UIImage imageNamed:@"badge_volunteer_circle"];
    }else{
        self.badgeView.hidden = YES;
        self.badgeIcon.hidden = YES;
    }
    //is there notification set up?
 //   NSString *iCalID = eventDetail.iCalID;
    if (![eventDetail.iCalID isEqualToString:@"NA"]){
        self.notificationBtn.hidden = YES;
    }
    
    //customize back button
    /*
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
     UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrow_left"]
                                                                    style:UIBarButtonItemStylePlain
                                                                   target:self.navigationController
                                                                   action:@selector(popViewControllerAnimated:)];
     self.navigationItem.leftBarButtonItem = backButton;
     self.navigationItem.backBarButtonItem.title = @"Back";
    }*/
    
    
    //pass coordinate
    if (![eventDetail.loclat isEqualToString:@""]){
        double latdouble = [eventDetail.loclat doubleValue];
        double longdouble = [eventDetail.loclong doubleValue];
        CLLocation *LocationAtual = [[CLLocation alloc] initWithLatitude:latdouble longitude:longdouble];
    
        //NSLog(@"%f", longdouble);
        self.coordinates = LocationAtual;
        //get location
        eventMapIB.showsUserLocation = YES;
        eventMapIB.delegate = self;
        eventMapIB.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    
        CLLocationCoordinate2D location = eventMapIB.userLocation.coordinate;
        MKCoordinateRegion region;
        MKCoordinateSpan span;
    
        location.latitude  = latdouble;
        location.longitude = longdouble;
        
        
        span.latitudeDelta = 0.05;
        span.longitudeDelta = 0.05;
    
        region.span = span;
        region.center = location;
    
        [eventMapIB setRegion:region animated:YES];
        [eventMapIB regionThatFits:region];
       
        // Annotations
        [eventMapIB addAnnotations:[self annotations]];
    
        // Add an annotation
        /*MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
        point.coordinate = self.coordinates.coordinate;
        point.title = [NSString stringWithFormat: @"%@", eventDetail.locName];
        [eventMapIB addAnnotation:point];
      */
    }else{
        self.mapHeightLimit.constant = 0;
    }
    
    
    //get data from JSON
    if ([eventDetail.eventPhoto isEqualToString:@""]){
         self.photoHeightLimit.constant = 0;
        self.eventPhotoIB.hidden = YES;
    }else{
        NSString *imageurl = [NSString stringWithFormat: @"%@events/%@",BaseURLFOR_IMAGE, eventDetail.eventPhoto];
        NSURL *thumbnailURL = [NSURL URLWithString: imageurl];
        NSData *imageData = [NSData dataWithContentsOfURL: thumbnailURL];
        UIImage *image = [UIImage imageWithData:imageData];
        self.eventPhotoIB.image = image;
        self.photoHeightLimit.constant = 180;
        self.eventPhotoIB.contentMode = UIViewContentModeScaleAspectFill;
        self.eventPhotoIB.clipsToBounds = YES;
        self.eventPhotoIB.hidden = NO;
    }
    
    //remove logo goint into detail view
    //  NSLog(@"Hospital %@", hospital.hospitalIntro);
    //return;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews{
    self.eventDetailIB.numberOfLines = 0;
    [self.eventDetailIB sizeToFit];
    self.eventTitleIB.numberOfLines = 0;
    [self.eventTitleIB sizeToFit];
    self.eventTimeIB.numberOfLines = 0;
    [self.eventTimeIB sizeToFit];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Map
//get the right location
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation: (MKUserLocation *)userLocation
{
    mapView.centerCoordinate =  self.coordinates.coordinate;
    
}
//map annotations
- (NSArray *)annotations {
    // Empire State Building
    JPSThumbnail *locationdot = [[JPSThumbnail alloc] init];
    locationdot.image = [UIImage imageNamed:@"marker"];
    locationdot.title = @"Directions";
    NSLog(@"%@", locationdot.title);
   // locationdot.subtitle = [NSString stringWithFormat:@"%@ miles", eventDetail.dis];
    locationdot.coordinate = CLLocationCoordinate2DMake(self.coordinates.coordinate.latitude, self.coordinates.coordinate.longitude);
    locationdot.disclosureBlock = ^{ [self getDirections]; };
    
    return @[[JPSThumbnailAnnotation annotationWithThumbnail:locationdot]];
}

#pragma mark - MKMapViewDelegate

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    if ([view conformsToProtocol:@protocol(JPSThumbnailAnnotationViewProtocol)]) {
        [((NSObject<JPSThumbnailAnnotationViewProtocol> *)view) didSelectAnnotationViewInMap:mapView];
    }
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
    if ([view conformsToProtocol:@protocol(JPSThumbnailAnnotationViewProtocol)]) {
        [((NSObject<JPSThumbnailAnnotationViewProtocol> *)view) didDeselectAnnotationViewInMap:mapView];
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    if ([annotation conformsToProtocol:@protocol(JPSThumbnailAnnotationProtocol)]) {
        return [((NSObject<JPSThumbnailAnnotationProtocol> *)annotation) annotationViewInMap:mapView];
    }
    return nil;
}

- (void)getDirections{
    
    NSString *addressURL = [NSString stringWithFormat:@"https://maps.google.com/maps?saddr=%f,%f&daddr=%f,%f",self.eventMapIB.userLocation.coordinate.latitude, self.eventMapIB.userLocation.coordinate.longitude, self.coordinates.coordinate.latitude, self.coordinates.coordinate.longitude];
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: addressURL]];
    
    NSLog(@"%@",addressURL);
    
}
- (IBAction)getPoint:(id)sender {
    //[self performSegueWithIdentifier:@"logPoint" sender:self];
    NSLog(@"clickced");
    //switch view
    UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *newFrontController = nil;
    SWRevealViewController *revealController = [self revealViewController];
    LogPoint *detailVC = [main instantiateViewControllerWithIdentifier:@"logPointViewController"];
    newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    [revealController pushFrontViewController:newFrontController animated:YES];

}

- (IBAction)setNotification:(id)sender {
    //SAVE TO Database
    NSString *userID = [userDefaults objectForKey:kUserID];
    NSString *eventNotify = [self getNotified:eventDetail userID:userID];
    if ([eventNotify isEqualToString:@"Added"]){
        //Save to iCal
    
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Notification Center" message:@"Your notification has been added." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        
        //hide set notification button
        self.notificationBtn.hidden = YES;
    }
}

#pragma mark - Segue Process Navigation
-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"logPoint"]){
      //  LogPoint *logDetail = (LogPoint *)segue.destinationViewController;
        LogPoint *logDetail = segue.destinationViewController;
        logDetail.eventDetail = eventDetail;
    }
}

@end
