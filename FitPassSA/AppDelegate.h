//
//  AppDelegate.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/9/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"
#import "LeftMenu.h"
#import "RightMenu.h"
#import <FirebaseCore/FirebaseCore.h>
#import <FirebaseInstanceID/FirebaseInstanceID.h>
#import <UserNotifications/UserNotifications.h>
#import <UserNotificationsUI/UserNotificationsUI.h>

@class Instruction;
@class SWRevealViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    NSUserDefaults *userDefaults;
}
@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) Instruction *viewController;
@property (strong, nonatomic) SWRevealViewController *viewControllerReveal;

@end

