//
//  UserEventCellVeiw.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/30/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"

@interface UserEventCellVeiw : SWTableViewCell

@property (strong, nonatomic) IBOutlet UILabel *event_title;
@property (strong, nonatomic) IBOutlet UILabel *event_points;
@property (strong, nonatomic) IBOutlet UIView *notificationRow;
@end
