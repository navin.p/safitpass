//
//  BonusShare.m
//  FitPassSA
//
//  Created by Ping-Jung Tsai on 5/13/17.
//  Copyright © 2017 Ping-jung Tsai. All rights reserved.
//

#import "BonusShare.h"
#import "Event.h"
#import "AppSetting.h"
#import "Profile.h"
#import "SWRevealViewController.h"
#import "EventList.h"
#import "MBProgressHUD.h"
#import "AFNetworking.h"

#import "GAIDictionaryBuilder.h"
#import "GAI.h"
#import "GAIFields.h"
#import "Header.h"
@interface BonusShare (){
    NSUserDefaults *userDefaults;
}

@end

@implementation BonusShare
@synthesize eventDetail;

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    //GA Log
    NSString *str = @"Bonus Share Point Activity";
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:str];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    tracker.allowIDFACollection = YES;
}

#pragma mark - Object lifecycle
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self.slideOutAnimationEnabled = YES;
    
    return [super initWithCoder:aDecoder];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.[super viewDidLoad];
    
    userDefaults = [[NSUserDefaults alloc] init];
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    self.navigationController.navigationBarHidden = YES;
    
    NSString *eID = [userDefaults objectForKey:kEventID];
    //then get the data
    NSString *url = [NSString stringWithFormat: @"%@eventDetail.php?eid=%@",BaseURL, eID];
    NSURL *eventURL = [NSURL URLWithString: url];
    NSLog(@"%@", eventURL);
    NSData *jsonData = [NSData dataWithContentsOfURL: eventURL];
    NSError *error = nil;
    NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error: &error];
    
    NSDictionary *userInfo = [dataDictionary objectForKey:@"fitpass"];
    
    self.eventPoint.text = [userInfo objectForKey:@"point"];
    self.eventTitle = [userInfo objectForKey:@"title"];
    self.eventPointV =[userInfo objectForKey:@"point"];
    self.shareBonus = [userInfo objectForKey:@"shareBonus"];
    self.photoName = [userInfo objectForKey:@"photo"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Clsoe window
- (IBAction)closeWindow:(id)sender {
    
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    SWRevealViewController *revealController = [self revealViewController];
    UIViewController *newFrontController = nil;
    EventList *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"eventListViewController"];
    newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    [revealController pushFrontViewController:newFrontController animated:YES];
    
}

- (IBAction)viewProfile:(id)sender {
    //redirect back to eventList;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    SWRevealViewController *revealController = [self revealViewController];
    UIViewController *newFrontController = nil;
    Profile *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    [revealController pushFrontViewController:newFrontController animated:YES];
}

#pragma mark - Share Point
- (IBAction)sharePoint:(id)sender {
    //first log it
    
    //show progress bar
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    progressHUD.labelText = @"Logging Point...";
    progressHUD.mode = MBProgressHUDAnimationFade;
    
  //  NSString *uID = [userDefaults objectForKey:kUserID];
    NSString *eID =   [userDefaults objectForKey:kEventID];
    NSString *uID = [userDefaults objectForKey:kCurrentUser];
    
    //compose the url parameters
    //compile the URL
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    //  NSURL *url = [NSURL URLWithString:@"http://www.safitpass.com.php53-22.ord1-1.websitetestlink.com/json/logPoint.php"];
    
    NSDictionary *params =  [[NSDictionary alloc] initWithObjectsAndKeys:
                             uID, @"uid",
                             eID, @"eid",
                             @"1", @"share",
                             nil] ;
    NSLog(@"%@", params);
    [manager POST:[NSString stringWithFormat:@"%@logPoint.php",BaseURL] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSString *userPT = [NSString stringWithFormat:@"%@",[responseObject objectAtIndex:1]];
        //is this successful or is it a duplicate entries
        [userDefaults setObject:userPT forKey:kUserPoints];
        [userDefaults synchronize];
            
        //NSLog(@"%@", [userDefaults objectForKey:kUserPoints]);
        [self killHUD];
        
        self.shareButton.hidden = YES;
        //then show the social share action
        //compose the item to share
        NSString *textToShare = [NSString stringWithFormat: @"I just earned %@ points on FitPass for: %@", self.eventPoint, self.eventTitle];
        NSLog(@"Photo: %@",self.photoName);
        if (![self.photoName isEqualToString:@""]){
            NSString *imageurl = [NSString stringWithFormat: @"%@events/%@",BaseURLFOR_IMAGE, self.photoName];
            NSURL *thumbnailURL = [NSURL URLWithString: imageurl];
            NSData *imageData = [NSData dataWithContentsOfURL: thumbnailURL];
            UIImage *imageToShare = [UIImage imageWithData:imageData];
            
            NSArray *objectsToShare = @[textToShare, imageToShare];
            NSLog(@"%@", objectsToShare);
            UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
            [self presentViewController:activityVC animated:YES completion:nil];
        }else{
            UIImage *imageToShare = [UIImage imageNamed:@"fitpas_info"];
            NSArray *objectsToShare = @[textToShare, imageToShare];
            NSLog(@"%@", objectsToShare);
            UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
            [self presentViewController:activityVC animated:YES completion:nil];
        }
            
        return;
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error.localizedDescription);
        [self killHUD];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"No Connection!" message:@"It seems there is no internet connection. Please try again later." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alertView show];
        return;
    }];
}

#pragma mark - Misc
- (void)killHUD
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}
@end
