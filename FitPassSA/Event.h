//
//  Event.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/19/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Event : NSObject

@property (nonatomic, strong) NSString *eventID;
@property (nonatomic, strong) NSString *eventTitle;
@property (nonatomic, strong) NSString *startDate;
@property (nonatomic, strong) NSString *startTime;
@property (nonatomic, strong) NSString *endTime;
@property (nonatomic, strong) NSString *eventTime;
@property (nonatomic, strong) NSString *ckTime;
@property (nonatomic, strong) NSString *eventDescription;
@property (nonatomic, strong) NSString *points;
@property (nonatomic, strong) NSDictionary *locationInfo;
@property (nonatomic, strong) NSString *locName;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *fitLevel;
@property (nonatomic, strong) NSString *eventPhoto;
@property (nonatomic, strong) NSString *loclat;;
@property (nonatomic, strong) NSString *badge;
@property (nonatomic, strong) NSString *loclong;
@property (nonatomic, strong) NSMutableArray *categories;
@property (nonatomic, strong) NSString *iCalID;
@property (nonatomic, strong) NSString *userPt;
@property (nonatomic, strong) NSString *shareBonus;

- (id) initWithTitle:(NSString *)title;
+(id) blogPostWithTitle:(NSString *)title; //convenient constructor

//implementation to conver to URL
- (NSURL *) thumbnailURL;
- (NSDate *) eventDate;
- (NSDate *) eventStime;
- (NSDate *) eventEtime;
- (NSDate *) eventCtime;
- (NSDictionary *)notification:(NSString *)eID userID:(NSString *)uID;

@end
