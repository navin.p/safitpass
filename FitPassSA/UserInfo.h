//
//  UserInfo.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/27/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserInfo : NSObject
@property (nonatomic, strong) NSString *userID;
@property (nonatomic, strong) NSString *userPhoto;
@property (nonatomic, strong) NSString *userPoints;
@property (nonatomic, strong) NSString *userWkyPoints;
@property (nonatomic, strong) NSString *userFirstName;
@property (nonatomic, strong) NSString *userLastName;
@property (nonatomic, strong) NSString *userEmail;
@property (nonatomic, strong) NSString *userGender;
@property (nonatomic, strong) NSString *userAge;
@property (nonatomic, strong) NSString *userActLvl;
@property (nonatomic, strong) NSString *userTeam;
@property (nonatomic, strong) NSMutableArray *userNotification;
@property (nonatomic, strong) NSMutableArray *userEvents;
@property (nonatomic, strong) NSString *eventFound;
@property (nonatomic, strong) NSString *weight;

- (id) initWithID:(NSString *)userID;
+(id) blogPostWithID:(NSString *)userID; //convenient constructor

//implementation to conver to URL
- (NSURL *) thumbnailURL;

@end
