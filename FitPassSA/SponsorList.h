//
//  Sponsor.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 4/15/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
@interface SponsorList : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UITabBarDelegate, UIGestureRecognizerDelegate, SWRevealViewControllerDelegate>


@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UICollectionViewFlowLayout *collectionLayout;


@property (weak, nonatomic) IBOutlet UITabBar *tapBar;
@property (strong, nonatomic) IBOutlet UITabBarItem *aboutSAPR;
@property (strong, nonatomic) IBOutlet UITabBarItem *viewAcct;
@property (strong, nonatomic) IBOutlet UITabBarItem *viewFP;
@property (strong, nonatomic) IBOutlet UITabBarItem *viewFIP;

@property (nonatomic, strong) NSMutableArray *sponsorList;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *viewTapped;

- (IBAction)goHome:(id)sender;
@end
