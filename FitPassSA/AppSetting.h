//
//  AppSetting.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/17/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#ifndef FitPassSA_AppSetting_h
#define FitPassSA_AppSetting_h

#define kUserData @"kUSER_DATA"
#define kFirstName @"kFIRST_NAME"
#define kLastName @"kLAST_NAME"
#define kEmail @"kEMAIL"
#define kPassword @"kPASSWORD"
#define kUsername @"kUSERNAME"
#define kLoggedIn @"kLoggedIn"
#define kUserID @"kUSER_ID"
#define kDeviceToken @"kDeviceToken"
#define kCurrentDate @"kCurrentDate"
#define kProfilePic @"kProfilePic"
#define kMyID @"kMyID"
#define kEventCat @"kEventCat"
#define kUserPoints @"kUserPoints"
#define kEventID @"kEventID"
#define kEventDate @"kEventDate"
#define kFBID @"kFBID"
#define kISParent @"kISParent"
#define kParentID @"kParentID"
#define kCurrentUser @"kCurrentUser"
#define kCalendar @"kCalendar"
#define kNewKid @"kNewKid"
#endif
