//
//  UserInfo.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/27/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import "UserInfo.h"

@implementation UserInfo

- (id) initWithID:(NSString *)userID{
    self = [super init];
    if (self){
        //[self setTitle:title];
        self.userID = userID;
        self.userFirstName = nil;
        self.userPhoto = nil;
        self.userPoints = nil;
        self.userFirstName = nil;
        self.userLastName = nil;
        self.userEmail = nil;
        self.userNotification = nil;
        self.userEvents = nil;
        self.eventFound = nil;
        self.userTeam = nil;
        self.weight = nil;
    }
    return self;
}
+(id) blogPostWithID:(NSString *)userID{
    return [[self alloc]initWithID:userID];
}
//implementation to conver to URL
- (NSURL *) thumbnailURL{
    return [NSURL URLWithString:self.userPhoto];
}
@end
