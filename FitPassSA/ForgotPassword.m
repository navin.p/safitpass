//
//  ForgotPassword.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 4/16/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import "ForgotPassword.h"
#import "MBProgressHUD.h"
#import "UITextField+TextFld.h"
#import "UIButton+BtnStyle.h"
#import <QuartzCore/QuartzCore.h>
#import "AppSetting.h"
#import "AFNetworking.h"
#import "URBAlertView.h"
#import "SWRevealViewController.h"
#import "GAIDictionaryBuilder.h"
#import "GAI.h"
#import "GAIFields.h"
#import "Header.h"

@interface ForgotPassword (){
    NSUserDefaults *userDefaults;
}

@end

@implementation ForgotPassword
@synthesize emailField;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self.slideOutAnimationEnabled = YES;
    
    return [super initWithCoder:aDecoder];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    //GA Log
    NSString *str = @"Forgot Password";
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:str];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    tracker.allowIDFACollection = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    [self.emailField configureTextFieldWithFrame:self.emailField.frame];
    
    UIColor *color = [UIColor whiteColor];
    self.emailField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email Address" attributes:@{NSForegroundColorAttributeName: color}];
    
    //add tap for keybord dismiss
    UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc]
                                       initWithTarget:self
                                       action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:viewTap];
    
    //style button
  //  [self.resetBtn configureBtn:self.resetBtn];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)resetAction:(id)sender {
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    progressHUD.labelText = @"Processing...";
    progressHUD.mode = MBProgressHUDAnimationFade;
    
    //check for email
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    if  ([emailTest evaluateWithObject:emailField.text] != YES && [emailField.text length]!=0)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention!" message:@"Please enter a valid email address." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alertView show];
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        return;
    }
    //check for required fields
    if ([emailField.text isEqualToString:@""]) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention!" message:@"Please enter the email address you used to register." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alertView show];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        return;
    }else{
        //compile the URL
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        NSDictionary *params =  [[NSDictionary alloc] initWithObjectsAndKeys:
                                 emailField.text, @"email",
                                 nil] ;
        //PROCESS IT
        NSLog(@"%@", emailField.text);
        [manager POST:[NSString stringWithFormat:@"%@password.php",BaseURL] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
           
            //is this successful or is it a duplicate entries
            NSString *dupUser = [NSString stringWithFormat:@"%@",[responseObject objectAtIndex:0]];
            NSString *userID = [NSString stringWithFormat:@"%@",[responseObject objectAtIndex:1]];
            if ([dupUser isEqualToString:@"1"]){
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"No Account Found!" message:@"There is no account set up with this email. Please try again or sign up a new account." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                
                [alertView show];
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            }else if ([dupUser isEqualToString:@"2"]){
                NSString *messagetxt = [NSString stringWithFormat: @"There is a problem emailing your new password to %@.",  emailField.text, userID];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Unable to Deliver Email!" message:messagetxt delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                
                [alertView show];
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                
                UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                //assign instruction to new uiviewcontroller
                UIViewController *instructionViewController = [mainstoryboard instantiateViewControllerWithIdentifier:@"loginViewController"];
                [self presentViewController:instructionViewController animated:YES completion:nil];
                
            }else{
                //assign value to system default
                NSString *messagetxt = [NSString stringWithFormat: @"Your new password has been sent to %@. Please login with your new password.",  emailField.text, userID];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Password Reset!" message:messagetxt delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                
                [alertView show];
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                
                //redirect to home page to login                
                UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                //assign instruction to new uiviewcontroller
                UIViewController *instructionViewController = [mainstoryboard instantiateViewControllerWithIdentifier:@"loginViewController"];
                [self presentViewController:instructionViewController animated:YES completion:nil];
            }
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
            return;
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error.localizedDescription);
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        }];
    }
}
/*
 #pragma mark - Navigation
*/
- (IBAction)loginAction:(id)sender {
    
    
    UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //assign instruction to new uiviewcontroller
    UIViewController *instructionViewController = [mainstoryboard instantiateViewControllerWithIdentifier:@"loginViewController"];
    [self presentViewController:instructionViewController animated:YES completion:nil];
}

- (IBAction)signUpAction:(id)sender {
    
    
    UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //assign instruction to new uiviewcontroller
    UIViewController *instructionViewController = [mainstoryboard instantiateViewControllerWithIdentifier:@"SignUpViewContorller"];
    [self presentViewController:instructionViewController animated:YES completion:nil];
}




 #pragma mark - Text Field Editing
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [self.view endEditing:YES];
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return YES;
}
 #pragma mark - Text Field Drop Down
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
}
-(void)dismissKeyboard {
    [self.emailField resignFirstResponder];
}


@end
