//
//  EventCategory.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 4/30/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"
#import "SWTableViewCell.h"
#import <CoreLocation/CoreLocation.h>
#import <CoreLocation/CLLocationManagerDelegate.h>

@interface EventCategory : UIViewController<UITableViewDelegate, UITableViewDataSource, SWTableViewCellDelegate>


@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *byPoints;
@property (strong, nonatomic) IBOutlet UIButton *byLocation;
@property (strong, nonatomic) IBOutlet UIButton *byTime;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (strong, nonatomic) IBOutlet UIImageView *noEvent;

@property (nonatomic, strong) NSMutableArray *eventList;
@property(strong, nonatomic) NSIndexPath *savedSelectedIndexPath;

@property (weak, nonatomic) IBOutlet UILabel *currentUserName;
@property (weak, nonatomic) IBOutlet UIImageView *currentUserPhoto;
@property (weak, nonatomic) IBOutlet UIView *currentUser;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *currentUserHeight;

- (IBAction)sortByPt:(id)sender;
- (IBAction)sortByLoc:(id)sender;
- (IBAction)sortByTime:(id)sender;
@end
