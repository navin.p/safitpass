//
//  SignUp.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/10/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignUp.h"
#import <QuartzCore/QuartzCore.h>
#import "UITextField+TextFld.h"
#import "UIButton+BtnStyle.h"
#import "DropDownListView.h"
#import "MBProgressHUD.h"
#import "CommonCrypto.h"
#import "AFNetworking.h"
#import "URBAlertView.h"
#import "AppSetting.h"
#import "SWRevealViewController.h"
#import "EventList.h"
#import "Login.h"
#import "GAIDictionaryBuilder.h"
#import "GAI.h"
#import "GAIFields.h"
#import "Header.h"

#define kOFFSET_FOR_KEYBOARD 80.0

@interface SignUp ()
{
    NSArray *genderArray;
    NSArray *booleanArray;
    NSArray *activityArray;
    NSUserDefaults *userDefaults;
}

@end


@implementation SignUp

@synthesize scrollView;
@synthesize contentView;
@synthesize lblSelectedNames;
@synthesize firstName;
@synthesize lastName;
@synthesize emailAddress;
@synthesize password;
@synthesize age;
@synthesize textOK;
@synthesize fitLevel;
@synthesize gender;
@synthesize zipCode;
@synthesize teamName;
@synthesize weight;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(void)viewDidAppear:(BOOL)animated
{
    //GA Log
    NSString *str = @"Account Sign Up";
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:str];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    tracker.allowIDFACollection = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    //** attach scroll view **/
    self.scrollView.delegate = self;
    [self.scrollView setScrollEnabled:YES];
    self.scrollView.frame = CGRectMake(0, 80, self.view.frame.size.width, self.scrollView.frame.size.height);
    [self.scrollView setContentSize:CGSizeMake(self.view.frame.size.width, 1000)];
    self.contentView.frame = CGRectMake(0, 80, self.view.frame.size.width, self.contentView.frame.size.height);

    UITapGestureRecognizer *viewTapped=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewTapped:)];
    [self.view addGestureRecognizer:viewTapped];
    viewTapped.delegate = self;
    [viewTapped setCancelsTouchesInView:NO];
    
    //add tap for keybord dismiss
    /*UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc]
                                       initWithTarget:self
                                       action:@selector(dismissKeyboard)];
   
    [self.view addGestureRecognizer:viewTap];
     */
    userDefaults = [[NSUserDefaults alloc] init];
    userDefaults = [NSUserDefaults standardUserDefaults];
    
     //change style of the fields
    [self.firstName configureTextFieldWithFrame:self.firstName.frame];
    [self.lastName configureTextFieldWithFrame:self.lastName.frame];
    [self.emailAddress configureTextFieldWithFrame:self.emailAddress.frame];
    [self.password configureTextFieldWithFrame:self.password.frame];
    [self.age configureTextFieldWithFrame:self.age.frame];
    [self.textOK configureTextFieldWithFrame:self.textOK.frame];
    [self.fitLevel configureTextFieldWithFrame:self.fitLevel.frame];
    [self.gender configureTextFieldWithFrame:self.gender.frame];
    [self.zipCode configureTextFieldWithFrame:self.zipCode.frame];
    [self.teamName configureTextFieldWithFrame:self.teamName.frame];
    [self.weight configureTextFieldWithFrame:self.weight.frame];
    
    //self.firstName.frame=CGRectMake(0, 0, 400, 44);
    
    //change placeholder of the fieldsw.
    UIColor *color = [UIColor whiteColor];
    self.firstName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"First Name" attributes:@{NSForegroundColorAttributeName: color}];
    self.lastName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Last Name" attributes:@{NSForegroundColorAttributeName: color}];
    self.emailAddress.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email Address" attributes:@{NSForegroundColorAttributeName: color}];
    self.password.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];
    self.weight.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Weight" attributes:@{NSForegroundColorAttributeName: color}];
    self.age.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Age" attributes:@{NSForegroundColorAttributeName: color}];
    self.textOK.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Can we text you?" attributes:@{NSForegroundColorAttributeName: color}];
    self.fitLevel.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Fitness Level" attributes:@{NSForegroundColorAttributeName: color}];
    self.gender.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Gender" attributes:@{NSForegroundColorAttributeName: color}];
    self.zipCode.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Zip Code" attributes:@{NSForegroundColorAttributeName: color}];
    self.teamName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Team Name (if any)" attributes:@{NSForegroundColorAttributeName: color}];

    //style button
   // [self.registerBtn configureBtn:self.registerBtn];
    
    
    //set picker options
    
    genderArray = @[@"Male", @"Female"];
    booleanArray = @[@"Yes", @"No"];
    activityArray = @[@"Light", @"Moderate", @"Advanced"];

    self.gender.delegate = self;
    self.textOK.delegate = self;
    self.fitLevel.delegate = self;

}

/*
 #pragma mark - Sign Up Submission
 **/

- (IBAction)signUp:(id)sender
{
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    progressHUD.labelText = @"Registering...";
    progressHUD.mode = MBProgressHUDAnimationFade;
    
    //check for email
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    if  ([emailTest evaluateWithObject:emailAddress.text] != YES && [emailAddress.text length]!=0)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention!" message:@"Please enter a valid email address." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alertView show];
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        return;
    }
    
    //check for required fields
    if ([firstName.text isEqualToString:@""] ||
        [lastName.text isEqualToString:@""] ||
        [emailAddress.text isEqualToString:@""] ||
        [password.text isEqualToString:@""]) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention!" message:@"Please enter first name, last name, email and password!" delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alertView show];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        return;
    }else{
        //salt the password
        /*const char *cStr = [password.text UTF8String];
        unsigned char digest[16];
        CC_MD5( cStr, strlen(cStr), digest ); // This is the md5 call
        
        NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
       
        for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++){
            [output appendFormat:@"%02x", digest[i]];
        }
         */
        
        //compile the URL
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
       // NSURL *url = [NSURL URLWithString:@"http://www.safitpass.com.php53-22.ord1-1.websitetestlink.com/json/signup.php"];
        NSDictionary *params =  [[NSDictionary alloc] initWithObjectsAndKeys:
                                 firstName.text, @"first_name",
                                 lastName.text, @"last_name",
                                 fitLevel.text, @"fit_level",
                                 zipCode.text, @"zip_code",
                                 gender.text, @"gender",
                                 age.text, @"age",
                                 emailAddress.text, @"email",
                                 password.text, @"password",
                                 textOK.text, @"text_ok",
                                 teamName.text, @"team",
                                 weight.text, @"weight",
                                 nil] ;
        //PROCESS IT
        [manager POST:[NSString stringWithFormat:@"%@signup.php",BaseURL] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSLog(@"JSON: %@", responseObject);
            
            //is this successful or is it a duplicate entries
            NSString *dupUser = [NSString stringWithFormat:@"%@",[responseObject objectAtIndex:0]];
            NSString *userID = [NSString stringWithFormat:@"%@",[responseObject objectAtIndex:1]];
            if ([dupUser isEqualToString:@"1"]){
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"User Already Exist!" message:@"This email address is already registered." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                
                [alertView show];
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            }else{
                //assign value to system default
                [userDefaults setObject:@"1" forKey:kLoggedIn];
                [userDefaults setObject:userID forKey:kUserID];
                [userDefaults setObject:emailAddress.text forKey:kEmail];
                [userDefaults setObject:password.text forKey:kPassword];
                [userDefaults setObject:firstName.text forKey:kFirstName];
                [userDefaults synchronize];
                
                UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                SWRevealViewController *revealVC = [main instantiateViewControllerWithIdentifier:@"swreveal"];
                
                [self presentViewController:revealVC animated:YES completion:nil];
                
            }
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
            return;
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error.localizedDescription);
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        }];
    }
}

- (IBAction)alreadyHasAcctAction:(id)sender {
    
    NSLog(@"ALREADY HAS ACCOUNT");
    UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //assign instruction to new uiviewcontroller
    UIViewController *instructionViewController = [mainstoryboard instantiateViewControllerWithIdentifier:@"loginViewController"];
    [self presentViewController:instructionViewController animated:YES completion:nil];
    
}


 #pragma mark - Text Field Editing
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    return YES;
 
}

#pragma mark - Text Field Drop Down
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
   
    //see if I can get the y position of the text field checked
    //double positionX = textField.frame.origin.x;
    //double positionY = textField.frame.origin.y + 100;
    //double positionX= 0;
    //double fieldwidth = textField.frame.size.width;
    double fieldwidth = self.view.frame.size.width;
    [self.lblSelectedNames setHidden:YES];
    
    [self.gender resignFirstResponder];
    [self.textOK resignFirstResponder];
    [self.fitLevel resignFirstResponder];
  
    if (textField == self.gender) {
        int optionCt = [genderArray count];
        double viewHeight = optionCt * 88;
        double positionY = (self.view.frame.size.height - viewHeight)/2;
        /*[self showPopUpWithTitle:@"" withOption:genderArray xy:CGPointMake(positionX, positionY) size:CGSizeMake(fieldwidth, viewHeight) isMultiple:NO];*/
        [self showPopUpWithTitle:@"" withOption:genderArray xy:CGPointMake(0, positionY) size:CGSizeMake(fieldwidth, viewHeight) isMultiple:NO];
        //set the label market to gender so we know what we are clicking
        self.lblSelectedNames.text = @"Gender";
    }else if (textField == self.textOK){        int optionCt = [booleanArray count];
        double viewHeight = optionCt * 88;
        double positionY = (self.view.frame.size.height - viewHeight)/2;
        /*[self showPopUpWithTitle:@"" withOption:booleanArray xy:CGPointMake(positionX, positionY) size:CGSizeMake(fieldwidth, viewHeight) isMultiple:NO];*/
        [self showPopUpWithTitle:@"" withOption:booleanArray xy:CGPointMake(0, positionY) size:CGSizeMake(fieldwidth, viewHeight) isMultiple:NO];
        self.lblSelectedNames.text = @"Text";
    }else if (textField == self.fitLevel) {
        int optionCt = [activityArray count];
        double viewHeight = optionCt * 88;
        double positionY = (self.view.frame.size.height - viewHeight)/2;
        /*[self showPopUpWithTitle:@"" withOption:activityArray xy:CGPointMake(positionX, positionY) size:CGSizeMake(fieldwidth, viewHeight) isMultiple:NO];*/
        [self showPopUpWithTitle:@"" withOption:activityArray xy:CGPointMake(0, positionY) size:CGSizeMake(fieldwidth, viewHeight) isMultiple:NO];
        self.lblSelectedNames.text = @"Level";
    }
}

-(void)showPopUpWithTitle:(NSString*)popupTitle withOption:(NSArray*)arrOptions xy:(CGPoint)point size:(CGSize)size isMultiple:(BOOL)isMultiple{
    
    
    Dropobj = [[DropDownListView alloc] initWithTitle:popupTitle options:arrOptions xy:point size:size isMultiple:isMultiple];
    Dropobj.delegate = self;
    [Dropobj showInView:self.view animated:YES];
    
    /*----------------Set DropDown backGroundColor-----------------*/
    [Dropobj SetBackGroundDropDwon_R:0.0 G:108.0 B:194.0 alpha:0.70];
    
}

- (void)DropDownListView:(DropDownListView *)dropdownListView Datalist:(NSMutableArray*)ArryData{
    
    if (ArryData.count>0) {
        lblSelectedNames.text=[ArryData componentsJoinedByString:@"\n"];
        CGSize size=[self GetHeightDyanamic:lblSelectedNames];
        lblSelectedNames.frame=CGRectMake(0, 100, size.width, size.height);
    }
    else{
        lblSelectedNames.text=@"";
    }
    
}
- (void)DropDownListViewDidCancel{
    
}


-(CGSize)GetHeightDyanamic:(UILabel*)lbl
{
    NSRange range = NSMakeRange(0, [lbl.text length]);
    CGSize constraint;
    constraint= CGSizeMake(288 ,MAXFLOAT);
    CGSize size;
    
    if (([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)) {
        NSDictionary *attributes = [lbl.attributedText attributesAtIndex:0 effectiveRange:&range];
        CGSize boundingBox = [lbl.text boundingRectWithSize:constraint options: NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size;
        
        size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    }
    else{
        size = [lbl.text sizeWithFont:[UIFont fontWithName:@"Helvetica" size:14] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    }
    return size;
}

-(void)DropDownListView:(DropDownListView *)dropdownListView didSelectedIndex:(NSInteger)anIndex{
    if (self.gender && [self.lblSelectedNames.text isEqual: @"Gender"]) {
        self.gender.text = [genderArray objectAtIndex:anIndex];
    }
    if (self.textOK && [self.lblSelectedNames.text  isEqual: @"Text"]) {
         self.textOK.text = [booleanArray objectAtIndex:anIndex];
    }
    if (self.fitLevel && [self.lblSelectedNames.text  isEqual: @"Level"]) {
        self.fitLevel.text = [activityArray objectAtIndex:anIndex];
    }
    [Dropobj fadeOut];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
-(void)dismissKeyboard {
    [self.firstName resignFirstResponder];
    [self.lastName resignFirstResponder];
    [self.emailAddress resignFirstResponder];
    [self.password resignFirstResponder];
    [self.age resignFirstResponder];
    [self.zipCode resignFirstResponder];
}*/
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
       shouldReceiveTouch:(UITouch *)touch
{
    
    [self dismissKeyboard];
    return YES;
}
-(void)viewTapped:(UITapGestureRecognizer*)gestureRecognizer
{
    
    [self dismissKeyboard];
    //Do what you want here
    
}
-(void)dismissKeyboard {
    NSLog(@"HIDE KEYBOARD");
    
    [Dropobj fadeOut];
    [self.firstName resignFirstResponder];
    [self.lastName resignFirstResponder];
    [self.emailAddress resignFirstResponder];
    [self.password resignFirstResponder];
    [self.gender resignFirstResponder];
    [self.age resignFirstResponder];
    [self.textOK resignFirstResponder];
    [self.fitLevel resignFirstResponder];
    [self.zipCode resignFirstResponder];
    [self.teamName resignFirstResponder];
    [self.weight resignFirstResponder];
}
@end
