//
//  LogPoint.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/26/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"
#import "AppSetting.h"

@interface LogPoint : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIButton *closeBtn;
@property (strong, nonatomic) IBOutlet UIImageView *eventPhoto;
@property (strong, nonatomic) IBOutlet UILabel *eventTitle;
@property (strong, nonatomic) IBOutlet UILabel *eventPoint;
@property (strong, nonatomic) IBOutlet UITextField *codeField;
@property (strong, nonatomic) IBOutlet UIButton *logBtn;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *photoHeightLimit;
@property (strong, nonatomic) IBOutlet UIScrollView *scrowView;
@property (strong, nonatomic) IBOutlet UIView *contentView;


#pragma Variables
@property (nonatomic, strong) Event *eventDetail;
@property (nonatomic, assign) BOOL slideOutAnimationEnabled;

@property (nonatomic, strong) NSString *eventTitleV;
@property (nonatomic, strong) NSString *eventPointV;
@property (nonatomic, strong) NSString *photoNameV;
@property (nonatomic, strong) NSString *socialBonus;

#pragma Actions
- (IBAction)closeWindow:(id)sender;
- (IBAction)logPoints:(id)sender;
- (IBAction)hideKeyBoard:(id)sender;

@end
