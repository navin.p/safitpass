//
//  EventCellView.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/19/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import "FIPEventCellView.h"
//#import "NIDropDown.h"


@interface FIPEventCellView ()
@property(nonatomic, strong) UITableView *table;
@end

@implementation FIPEventCellView

@synthesize table;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    // [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setBounds:(CGRect)bounds
{
    //[super setBounds:bounds];
    
    self.cellView.frame = self.bounds;
    self.cellView.clipsToBounds = NO;
    
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self.cellView layoutIfNeeded];
}


- (void)dealloc {
    //    [btnSelect release];
    //    [super dealloc];
}


@end
