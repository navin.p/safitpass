//
//  FitPassInfo.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 4/1/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FitPassInfo : UIViewController<UIGestureRecognizerDelegate, UITabBarDelegate>

@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *viewTapped;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *webviewTapped;
@property (strong, nonatomic) IBOutlet UITabBarItem *aboutFP;
@property (strong, nonatomic) IBOutlet UITabBarItem *viewProfile;
@property (strong, nonatomic) IBOutlet UITabBarItem *viewEvents;
@property (strong, nonatomic) IBOutlet UITabBar *tabBar;

- (IBAction)goHome:(id)sender;

@end
