//
//  SwitchAcct.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 5/15/17.
//  Copyright © 2017 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
@interface SwitchAcct : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, SWRevealViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UICollectionView *memberCollection;
@property (strong, nonatomic) IBOutlet UICollectionViewFlowLayout *memberCollectionLayout;

@property (nonatomic, strong) NSMutableArray *memberList;

@property(strong, nonatomic) NSIndexPath *savedSelectedIndexPath;
@property(strong, nonatomic) NSIndexSet *selectedIndex;

@end
