//
//  SwitchAcctCell.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 5/15/17.
//  Copyright © 2017 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SwitchAcctCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *userPhoto;
@property (weak, nonatomic) IBOutlet UILabel *userName;

@end
