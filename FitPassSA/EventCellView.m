//
//  EventCellView.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 6/6/16.
//  Copyright © 2016 Ping-jung Tsai. All rights reserved.
//

#import "EventCellView.h"

@interface EventCellView ()
@property(nonatomic, strong) UITableView *table;
@end


@implementation EventCellView

@synthesize table;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    // [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setBounds:(CGRect)bounds
{
    //[super setBounds:bounds];
    
    self.cellView.frame = self.bounds;
    self.cellView.clipsToBounds = NO;
    
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self.cellView layoutIfNeeded];
}


- (void)dealloc {
    //    [btnSelect release];
    //    [super dealloc];
}


@end
