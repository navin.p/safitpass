//
//  SharePoint.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/26/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"
#import "AppSetting.h"
#import "SlideNavigationController.h"

@interface SharePoint : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *closeBtn;
@property (strong, nonatomic) IBOutlet UIImageView *eventPhoto;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *photoHeightLimit;

#pragma Variables
@property (nonatomic, strong) Event *eventDetail;
@property (nonatomic, assign) BOOL slideOutAnimationEnabled;
@property (nonatomic, strong) NSMutableArray *eventList;
@property (nonatomic, strong) NSString *eventTitle;
@property (nonatomic, strong) NSString *eventPoint;
@property (nonatomic, strong) NSString *photoName;


- (IBAction)closeWindow:(id)sender;
- (IBAction)viewProfile:(id)sender;
- (IBAction)sharePoint:(id)sender;

@end
