//
//  Sponsor.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 4/16/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import "Sponsor.h"
#import "Header.h"
@implementation Sponsor


- (id) initWithTitle:(NSString *)title{
    self = [super init];
    if (self){
        //[self setTitle:title];
        self.sponsorTitle = title;
        self.sponsorID = nil;
        self.sponsorURL = nil;
        self.sponsorPhoto = nil;
        self.sponsorOrder = nil;
    }
    return self;
}

+(id) blogPostWithTitle:(NSString *)title{
    return [[self alloc]initWithTitle:title];
}

//implementation to conver to URL
- (NSURL *) thumbnailURL{
    NSString *url = [NSString stringWithFormat: @"%@sponsors/%@",BaseURLFOR_IMAGE, self.sponsorPhoto];
    return [NSURL URLWithString:url];
}
- (NSURL *) siteURL{
    NSString *url = [NSString stringWithFormat: @"%@events/%@",BaseURLFOR_IMAGE, self.sponsorURL];
    return [NSURL URLWithString:url];
}
@end
