//
//  EventCategory.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 4/30/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import "EventCategory.h"
#import "EventLists.h"
#import "Event.h"
#import "EventCellView.h"
#import "AppSetting.h"
#import "CoreLocation/CLLocationManagerDelegate.h"
#import "MBProgressHUD.h"
#import "RMDateSelectionViewController.h"
#import "EventDetail.h"
#import "SWRevealViewController.h"
#import "GAIDictionaryBuilder.h"
#import "GAI.h"
#import "GAIFields.h"
#import "Header.h"
@interface EventCategory (){
    NSString *eventCat;
    NSUserDefaults *userDefaults;
}
@property (nonatomic) BOOL useCustomCells;
@property (nonatomic, weak) UIRefreshControl *refreshControl;
@property (nonatomic, strong) NSArray *configs;

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)
#define IS_IOS8_OR_ABOVE                            (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
@end

@implementation EventCategory
@synthesize noEvent;
@synthesize tableView;

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    
    userDefaults = [[NSUserDefaults alloc] init];
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    //get the category
    eventCat = [userDefaults objectForKey:kEventCat];
    NSString *catname;
    /*change navigation title
    if ([eventCat isEqualToString:@"1"]){
        catname = @"Fitness";
    }else if ([eventCat isEqualToString:@"2"]){
        catname =  @"Nutrition";
    }else if ([eventCat isEqualToString:@"3"]){
        catname = @"Wellness";
    }else if ([eventCat isEqualToString:@"4"]){
        catname =  @"Physical Activity";
    }else if ([eventCat isEqualToString:@"5"]){
        catname =  @"Fitness in Park";
    }else if ([eventCat isEqualToString:@"6"]){
        catname =  @"Featured";
    }else{
        catname =  @"All Events";
    }*/
    if ([eventCat isEqualToString:@"1"]){
        catname = @"Featured";
    }else if ([eventCat isEqualToString:@"2"]){
        catname =  @"Fitness";
    }else if ([eventCat isEqualToString:@"3"]){
        catname = @"Wellness/Nutrition";
    }else if ([eventCat isEqualToString:@"4"]){
        catname =  @"Volunteering";
    }else if ([eventCat isEqualToString:@"5"]){
        catname =  @"Fitness in Park";
    }else{
        catname =  @"All Events";
    }
    NSString *str = [NSString stringWithFormat:@"%@ Events",  catname];
    self.navigationController.topViewController.title = str;
    
     //GA Log
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:str];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    tracker.allowIDFACollection = YES;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTableWithNotification:) name:@"RefreshTable" object:nil];
    
}
- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Updated by Navin
    //UIColor *navColor =  [UIColor redColor];
    //self.navigationController.navigationBar.barTintColor = navColor;
    //self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    
    
    
    userDefaults = [[NSUserDefaults alloc] init];
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString *userID = [userDefaults objectForKey:kUserID];
    if ([userDefaults objectForKey:kCurrentUser] == nil){
        [userDefaults setObject:userID forKey:kCurrentUser];
        [userDefaults synchronize];
    }
    //load current user
    NSString *currentUser = [userDefaults objectForKey:kCurrentUser];
    [self loadCurrentUser:currentUser];
    
    [self createMenu];
    
    self.edgesForExtendedLayout=UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars=NO;
    self.automaticallyAdjustsScrollViewInsets=NO;
    
    //default hide everything
    tableView.hidden = YES;
    noEvent.hidden = YES;
    
    //define a saved selected IndexPath so we can pass the index between swipe button and direct row press
    self.savedSelectedIndexPath = nil;
    
    //get current location
    self.locationManager = [[CLLocationManager alloc] init];
    [self.locationManager setDelegate:self];
    self.locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
    [self.locationManager startUpdatingLocation];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        [self.locationManager requestAlwaysAuthorization];
    }
    //CLLocation *currentLoc = self.locationManager.location;

    //get the category
    eventCat = [userDefaults objectForKey:kEventCat];
    
    [self loadEvents:eventCat sortEvent:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didChangePreferredContentSize:)
                                                 name:UIContentSizeCategoryDidChangeNotification object:nil];
    self.tableView.estimatedRowHeight = 100.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    
    
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleBlackTranslucent];

    
    UINavigationBar *bar = [self.navigationController navigationBar];
    [bar setTintColor:[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1.0]];
    
    
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIContentSizeCategoryDidChangeNotification
                                                  object:nil];
}

- (void)didChangePreferredContentSize:(NSNotification *)notification
{
    [self.tableView reloadData];
}
- (void)refreshTableWithNotification:(NSNotification *)notification
{
    eventCat = [userDefaults objectForKey:kEventCat];
    [self loadEvents:[userDefaults objectForKey:kEventCat] sortEvent:nil];
}
- (void)loadCurrentUser:(NSString*)uid{
    //then get the data
    NSString *url = [NSString stringWithFormat: @"%@userBrief.php?uid=%@",BaseURL, uid];
    NSLog(@"User URL: %@", url);
    NSURL *eventURL = [NSURL URLWithString: url];
    NSData *jsonData = [NSData dataWithContentsOfURL: eventURL];
    NSError *error = nil;
    NSDictionary *userInfo = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error: &error];
    
    NSLog(@"%@", userInfo);
    self.currentUserName.text = [userInfo objectForKey:@"full_name"];
    NSString *userphoto = [NSString stringWithFormat: @"%@users/%@",BaseURLFOR_IMAGE, [userInfo objectForKey:@"photo"]];
    NSURL *photoURL = [NSURL URLWithString:userphoto];
    NSData *imageData = [NSData dataWithContentsOfURL:photoURL];
    self.currentUserPhoto.image = [UIImage imageWithData:imageData];
    
    self.currentUserPhoto.layer.cornerRadius = self.currentUserPhoto.frame.size.width / 2;
    self.currentUserPhoto.clipsToBounds = YES;
    self.currentUserPhoto.layer.borderWidth = 3.0f;
    self.currentUserPhoto.layer.borderColor = [UIColor whiteColor].CGColor;
}
#pragma mark - Event Gathering
//load events
- (void)loadEvents:(NSString*)searchType sortEvent:(NSString*)sortType{
    
    
    NSString *userID = [userDefaults objectForKey:kUserID];
    
    //show progress bar
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    progressHUD.labelText = @"Updating...";
    progressHUD.mode = MBProgressHUDAnimationFade;
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        NSString *calendar = [userDefaults objectForKey:kCalendar];
        //construct URL
        NSString *url = nil;
        NSString *eventType = nil;
        if ([calendar isEqualToString:@"FitPass"]){
                eventType = @"fitpass";
        }else if ([calendar isEqualToString:@"Park"]){
            eventType = @"park";
        }
        url= [NSString stringWithFormat: @"%@searchEvents.php?cat=%@&sortby=%@&lat=%f&long=%f&uID=%@&type=%@",BaseURL, searchType, sortType, self.locationManager.location.coordinate.latitude, self.locationManager.location.coordinate.longitude, userID, eventType];
        
        NSURL *eventURL = [NSURL URLWithString: url];
        NSLog(@"%@", eventURL);
        NSData *jsonData = [NSData dataWithContentsOfURL: eventURL];
        NSError *error = nil;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error: &error];
        self.eventList = [NSMutableArray array];
        NSArray *eventArray = [dataDictionary objectForKey:@"fitpass"];
        // NSString *eventFound = [dataDictionary objectForKey:@"notfound"];
        //if nothing is found
        if ([dataDictionary objectForKey:@"notfound"]){
            tableView.hidden = YES;
            noEvent.hidden = NO;
        }else{
            noEvent.hidden = YES;
            tableView.hidden = NO;
            for(NSDictionary *eventDictionary in eventArray){
                Event *event = [Event blogPostWithTitle:[eventDictionary objectForKey:@"name"]];
                
                event.eventID = [eventDictionary objectForKey:@"ID"];
                event.eventTitle = [eventDictionary objectForKey:@"title"];
                event.startDate = [eventDictionary objectForKey:@"sdate"];
                event.startTime = [eventDictionary objectForKey:@"stime"];
                event.endTime = [eventDictionary objectForKey:@"etime"];
                event.ckTime = [eventDictionary objectForKey:@"ctime"];
                event.eventDescription = [eventDictionary objectForKey:@"description"];
                event.points = [eventDictionary objectForKey:@"point"];
                event.fitLevel = [eventDictionary objectForKey:@"fitLevel"];
                event.eventPhoto = [eventDictionary objectForKey:@"photo"];
                event.categories = [eventDictionary objectForKey:@"category"];
                event.locationInfo = [eventDictionary objectForKey:@"location"];
                event.address = [eventDictionary objectForKey:@"address"];
                event.eventTime = [eventDictionary objectForKey:@"event_time"];
                event.loclat = [eventDictionary objectForKey:@"loc_lat"];
                event.loclong = [eventDictionary objectForKey:@"loc_long"];
                event.locName = [eventDictionary objectForKey:@"loc_name"];
                event.iCalID = [eventDictionary objectForKey:@"iCalID"];
                event.userPt = [eventDictionary objectForKey:@"user_pt"];
                
                [self.eventList addObject:event];
                [self.tableView reloadData];
            }
        }
        
        [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    });

    
}

#pragma mark - GeoLocation
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError");
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch(status) {
        case kCLAuthorizationStatusNotDetermined:
            // User has not yet made a choice with regards to this application
            NSLog(@"Not Determined");
            break;
        case kCLAuthorizationStatusRestricted:
            // This application is not authorized to use location services.  Due
            // to active restrictions on location services, the user cannot change
            // this status, and may not have personally denied authorization
            NSLog(@"Restricted");
            break;
        case kCLAuthorizationStatusDenied:
            // User has explicitly denied authorization for this application, or
            // location services are disabled in Settings
            NSLog(@"Denied");
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            NSLog(@"Allowed to location Access 1");
            //[self loadEvents:nil sortEvent:nil limitEvent:nil];
            break;
        default:
            NSLog(@"Allowed to location Access 2");
            //[self loadEvents:nil sortEvent:nil limitEvent:nil];
    }
    //   NSLog(@"Change in authorization status%@");
}

#pragma mark - Table View Setup
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    NSLog(@"Event Count %lu", (unsigned long)[self.eventList count]);
    if (self.eventList){
        return [self.eventList count];
    }else{
        return 1;
    }
}

#pragma mark - Table Cell Delegation
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    EventCellView *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    //convert date format
    if (self.eventList){
        //use custom class
        Event *event = [self.eventList objectAtIndex:indexPath.row];
        cell.event_points.text = [NSString stringWithFormat:@"%@ points",event.points];
        
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *date = [dateFormatter dateFromString:event.startDate];
        [dateFormatter setDateFormat:@"EEEE, MMMM dd"];
        NSString *newDateString = [dateFormatter stringFromDate:date];
        
        NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
        [dateFormatter2 setDateFormat:@"HH:mm:ss"];
        NSDate *date2 = [dateFormatter2 dateFromString:event.startTime];
        [dateFormatter2 setDateFormat:@"hh:mm a"];
        NSString *newTimeString = [dateFormatter2 stringFromDate:date2];
        
        cell.event_time.text = [NSString stringWithFormat:@"%@ %@",newDateString, newTimeString];
        cell.event_title.text = event.eventTitle;
        
    }
    //cell.hospital_address.text = [NSString stringWithFormat:@"%@",hospital.address];
    return cell;
}

//on direct press, trigger segue
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.savedSelectedIndexPath = [self.tableView indexPathForSelectedRow];
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    //  [self performSegueWithIdentifier:@"searchDetail" sender:self];
}

#pragma mark - Segue Process Navigation
-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSLog(@"Preparing for seque %@", segue.identifier);
    
    if ([segue.identifier isEqualToString:@"categoryDetail"]){
        
        self.savedSelectedIndexPath = [self.tableView indexPathForSelectedRow];
        
        NSLog(@"SavedPath %@", _savedSelectedIndexPath);
        //self.savedSelectedIndexPath = [self.tableView indexPathForSelectedRow];
        NSIndexPath *indexPath =  self.savedSelectedIndexPath; //get the index path of the row selected
        
        Event *event = [self.eventList objectAtIndex:indexPath.row]; // now get the content of that row
        EventDetail *eventDetailView = (EventDetail *)segue.destinationViewController;
        eventDetailView.eventDetail = event;
        
    }
}


- (IBAction)sortByPt:(id)sender {
    [self loadEvents:eventCat sortEvent:@"point"];
    [self.byPoints setTitleColor:[UIColor colorWithRed:4/255.0f green:121/255.0f blue:144/255.0f alpha:1.0] forState:UIControlStateNormal];
    [self.byLocation setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.byTime setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
}

- (IBAction)sortByLoc:(id)sender {
    [self loadEvents:eventCat sortEvent:@"location"];
    [self.byLocation setTitleColor:[UIColor colorWithRed:4/255.0f green:121/255.0f blue:144/255.0f alpha:1.0] forState:UIControlStateNormal];
    [self.byPoints setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.byTime setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
}

- (IBAction)sortByTime:(id)sender {
    [self loadEvents:eventCat sortEvent:@"time"];
    [self.byTime setTitleColor:[UIColor colorWithRed:4/255.0f green:121/255.0f blue:144/255.0f alpha:1.0] forState:UIControlStateNormal];
    [self.byLocation setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.byPoints setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
}


#pragma mark - SlideNavigationController Methods -
- (void)createMenu{
    
    //create menu
    SWRevealViewController *revealController = [self revealViewController];
    //create the left menu button
    UIButton *button  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [button setImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
    [button addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
    
    //create the left menu button
    UIButton *rightbutton  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [rightbutton setImage:[UIImage imageNamed:@"filter"] forState:UIControlStateNormal];
    [rightbutton addTarget:revealController action:@selector(rightRevealToggle:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightbutton];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
