//
//  FIPInfo.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 5/7/16.
//  Copyright © 2016 Ping-jung Tsai. All rights reserved.
//

#import "FIPInfo.h"
#import "AppSetting.h"
#import "MBProgressHUD.h"
#import "SWRevealViewController.h"
#import "ParkEventList.h"
#import "GAIDictionaryBuilder.h"
#import "GAI.h"
#import "GAIFields.h"
#import "Header.h"
@interface FIPInfo (){
    NSUserDefaults *userDefaults;
}

@end

@implementation FIPInfo

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    //GA Log
    NSString *str = @"Fitness In the Park Information";
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:str];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    tracker.allowIDFACollection = YES;
    

    [self killHUD];
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.webView.delegate = self;
    
    UIImageView *image=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,55,33)] ;
    //set your image logo replace to the main-logo
    [image setImage:[UIImage imageNamed:@"fiplogo_small"]];
    [self.navigationController.navigationBar.topItem setTitleView:image];
    
      // Updated by Navin
//    UIColor *navColor =  [UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:1.0];
//    self.navigationController.navigationBar.barTintColor = navColor;
    
    
    //create menu
    [self createMenu];
    //tapp on webview
    UITapGestureRecognizer *webviewTapped=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewTapped:)];
    webviewTapped.numberOfTapsRequired = 1;
    webviewTapped.delegate = self;
    [self.webView addGestureRecognizer:webviewTapped];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        // Do any additional setup after loading the view.
        // Do any additional setup after loading the view.
        
 

        NSURL *disclaimerURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@fipinfo.php?source=app",BaseURL1]];
        [self.webView loadRequest:[NSURLRequest requestWithURL:disclaimerURL]];
        
        [self.webView setScalesPageToFit:YES];
        self.webView.frame = self.view.frame;
        self.webView.clipsToBounds =YES;
        self.webView.frame =  CGRectMake(0,64,self.webView.frame.size.width,self.webView.frame.size.height-64);
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

- (void)killHUD
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    if(inType != UIWebViewNavigationTypeOther) {
        [[UIApplication sharedApplication] openURL:[inRequest URL]];
        return NO;
    }
    return YES;
}



#pragma mark - Home button on top right
- (IBAction)goHome:(id)sender {
    
    SWRevealViewController *revealController = [self revealViewController];
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    UIViewController *newFrontController = nil;
    ParkEventList *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"parkEventListViewController"];
    newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    [revealController pushFrontViewController:newFrontController animated:YES];
    
}

#pragma mark - SlideNavigationController Methods -
- (void)createMenu{
    
    //create menu
    SWRevealViewController *revealController = [self revealViewController];
    //create the left menu button
    UIButton *button  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [button setImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
    [button addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
    UITapGestureRecognizer *viewTapped=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewTapped:)];
    [self.view addGestureRecognizer:viewTapped];
    viewTapped.delegate = self;
    [viewTapped setCancelsTouchesInView:NO];
    
    self.revealViewController.delegate = self;
    [revealController.frontViewController.revealViewController tapGestureRecognizer];
}

#pragma mark - Tab Bar
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
       shouldReceiveTouch:(UITouch *)touch
{
    if([touch.view class] == self.tabBar.class){
        return NO;
    }
    return YES;
}
-(void)viewTapped:(UITapGestureRecognizer*)gestureRecognizer
{
    //Do what you want here
    SWRevealViewController *revealController = self.revealViewController;
    if (revealController.frontViewPosition == FrontViewPositionRight) {
        [revealController revealToggleAnimated:YES];
    }
}
-(void)tabBarController:(UITabBarController *)tabBar didSelectViewController:(UIViewController *)viewController
{
    
    NSLog(@"Selected index: %lu", (unsigned long)tabBar.selectedIndex);
}


- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    SWRevealViewController *revealController = [self revealViewController];
    
    if (item.tag == 1){
        [userDefaults setObject:nil forKey:kEventCat];
        [userDefaults setObject:nil forKey:kEventDate];
        [userDefaults synchronize];
        
        UIViewController *newFrontController = nil;
        ParkEventList *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"parkEventListViewController"];
        newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
        [revealController pushFrontViewController:newFrontController animated:YES];
    }
     NSLog(@"Selected index: %lu", item.tag);
}
-(void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position {
    if(position == FrontViewPositionLeft) {
        self.view.userInteractionEnabled = YES;
    } else {
        self.view.userInteractionEnabled = NO;
    }
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position {
    if(position == FrontViewPositionLeft) {
        self.view.userInteractionEnabled = YES;
    } else {
        self.view.userInteractionEnabled = NO;
    }
}


@end
