//
//  InstructionSub.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 4/17/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import "InstructionSub.h"

@interface InstructionSub ()

@end

@implementation InstructionSub

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        // Custom initialization
    }
    
    return self;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    NSLog(@"%ld", (long)self.index);
    switch (self.index) {
        case 0:
            [self.stepImage setImage:[UIImage imageNamed:@"step1"]];
            break;
        case 1:
            [self.stepImage setImage:[UIImage imageNamed:@"step2"]];
            break;
        case 2:
            [self.stepImage setImage:[UIImage imageNamed:@"step3"]];
            break;
        case 3:
            [self.stepImage setImage:[UIImage imageNamed:@"step4"]];
            break;
            
        case 4:
            break;
            
        default:
            break;
    }
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
