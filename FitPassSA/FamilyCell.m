//
//  FamilyCell.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 5/5/17.
//  Copyright © 2017 Ping-jung Tsai. All rights reserved.
//

#import "FamilyCell.h"

@implementation FamilyCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
