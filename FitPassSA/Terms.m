//
//  Terms.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 5/7/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import "Terms.h"
#import "MBProgressHUD.h"
#import "SWRevealViewController.h"
#import "AppSetting.h"
#import "GAIDictionaryBuilder.h"
#import "GAI.h"
#import "GAIFields.h"
#import "Header.h"
@interface Terms ()

@end

@implementation Terms

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSString *str = @"Terms and Conditions";
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:str];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    tracker.allowIDFACollection = YES;
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"appbkgd.png"]]];
   
    
    // Updated by Navin
   // UIColor *navColor =  [UIColor colorWithRed:240/255.0f green:140/255.0f blue:38/255.0f alpha:1.0];
//    self.navigationController.navigationBar.barTintColor = navColor;
 //   self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        // Do any additional setup after loading the view.
        
        NSURL *disclaimerURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@terms.php?source=app",BaseURL1]];

        
        [self.webView loadRequest:[NSURLRequest requestWithURL:disclaimerURL]];
        [self.webView setScalesPageToFit:YES];
        self.webView.frame = self.view.frame;
        self.webView.clipsToBounds =YES;
        self.webView.frame =  CGRectMake(0,64,self.webView.frame.size.width,self.webView.frame.size.height-64);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)windowDismiss:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
   // [self dismissModalViewControllerAnimated:YES];
}
@end
