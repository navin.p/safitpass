//
//  EventDetail.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/24/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"
#import <MapKit/MapKit.h>
#import "SlideNavigationController.h"

@interface EventDetail : UIViewController <MKMapViewDelegate, SlideNavigationControllerDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *eventPhotoIB;
@property (strong, nonatomic) IBOutlet UILabel *eventTitleIB;
@property (strong, nonatomic) IBOutlet UILabel *eventLocationIB;
@property (strong, nonatomic) IBOutlet UILabel *eventTimeIB;
@property (strong, nonatomic) IBOutlet MKMapView *eventMapIB;
@property (strong, nonatomic) IBOutlet MKPlacemark *mPlacemark;
@property (strong, nonatomic) IBOutlet UILabel *eventDetailIB;
@property (strong, nonatomic) IBOutlet UILabel *eventPointsIB;
@property (strong, nonatomic) IBOutlet UILabel *pointRuleIB;
@property (strong, nonatomic) IBOutlet UIButton *pointBtn;
@property (strong, nonatomic) IBOutlet UIButton *notificationBtn;
@property (strong, nonatomic) IBOutlet UILabel *eventLocationName;
@property (strong, nonatomic) IBOutlet UILabel *ptLogged;
@property (strong, nonatomic) IBOutlet UILabel *pointRule;
@property (strong, nonatomic) IBOutlet UIImageView *pointBkbd;
@property (strong, nonatomic) IBOutlet UILabel *pointTXT;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *noPtHeight;
@property (weak, nonatomic) IBOutlet UIImageView *badgeIcon;
@property (weak, nonatomic) IBOutlet UIView *badgeView;

/*variables*/
@property (nonatomic, strong) Event *eventDetail;
@property (nonatomic, retain) CLLocation *coordinates;
@property (nonatomic, strong) NSURL *thumbnailURL;
@property (nonatomic, assign) BOOL slideOutAnimationEnabled;

#pragma constraints
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *photoHeightLimit;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *mapHeightLimit;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *getPtHeight;

#pragma Actions
- (IBAction)getPoint:(id)sender;
- (IBAction)setNotification:(id)sender;

@end
