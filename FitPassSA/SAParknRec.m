//
//  SAParknRec.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 4/6/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import "SAParknRec.h"
#import "MBProgressHUD.h"
#import "SWRevealViewController.h"
#import "AppSetting.h"
#import "GAIDictionaryBuilder.h"
#import "GAI.h"
#import "GAIFields.h"
#import "Profile.h"
#import "EventLists.h"
#import "ParkEventList.h"
#import "Header.h"

@implementation SAParknRec{
    NSUserDefaults *userDefaults;
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSString *str = @"SA Park & Rec Page";
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:str];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    tracker.allowIDFACollection = YES;
    
    [self killHUD];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    userDefaults = [[NSUserDefaults alloc] init];
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    UIImageView *image=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,105,32)] ;
    //set your image logo replace to the main-logo
    [image setImage:[UIImage imageNamed:@"sapark"]];
    [self.navigationController.navigationBar.topItem setTitleView:image];
  
      // Updated by Navin
    // UIColor *navColor =  [UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:1.0];
   // self.navigationController.navigationBar.barTintColor = navColor;
    
    //create menu
    [self createMenu];
    
    self.webView.delegate = self;
    //tapp on webview
    UITapGestureRecognizer *webviewTapped=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewTapped:)];
    webviewTapped.numberOfTapsRequired = 1;
    webviewTapped.delegate = self;
    [self.webView addGestureRecognizer:webviewTapped];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        // Do any additional setup after loading the view.
        NSURL *disclaimerURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@parkinfo.php?source=app",BaseURL1]];

        [self.webView loadRequest:[NSURLRequest requestWithURL:disclaimerURL]];
        [self.webView setScalesPageToFit:YES];
        self.webView.frame = self.view.frame;
        self.webView.clipsToBounds =YES;
        self.webView.frame =  CGRectMake(0,64,self.webView.frame.size.width,self.webView.frame.size.height-64);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });
    
    // Akshay 5 July 2019
    NSMutableArray *newTabs = [NSMutableArray arrayWithArray:self.tapBar.items];
    [newTabs removeObjectAtIndex: 0];
    self.tapBar.items = newTabs;
 
    
    //////
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

- (void)killHUD
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}
- (void)webView:(UIWebView *)wv didFailLoadWithError:(NSError *)error {
    // Ignore NSURLErrorDomain error -999.
    if (error.code == NSURLErrorCancelled) return;
    
    // Ignore "Fame Load Interrupted" errors. Seen after app store links.
    if (error.code == 102 && [error.domain isEqual:@"WebKitErrorDomain"]) return;
    
    // Normal error handling…
}
- (BOOL)webView:(UIWebView *)wv shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    // Determine if we want the system to handle it.
    NSURL *url = request.URL;
    if (![url.scheme isEqual:@"http"] && ![url.scheme isEqual:@"https"]) {
        if ([[UIApplication sharedApplication]canOpenURL:url]) {
            [[UIApplication sharedApplication]openURL:url];
            return NO;
        }
    }
    return YES;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)goHome:(id)sender {
    [userDefaults setObject:nil forKey:kEventCat];
    [userDefaults setObject:nil forKey:kEventDate];
    [userDefaults synchronize];
    
    
    SWRevealViewController *revealController = [self revealViewController];
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    UIViewController *newFrontController = nil;
    EventLists *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"eventListViewController"];
    newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    [revealController pushFrontViewController:newFrontController animated:YES];
    
}

#pragma mark - SlideNavigationController Methods -
- (void)createMenu{
    
    //create menu
    SWRevealViewController *revealController = [self revealViewController];
    //create the left menu button
    UIButton *button  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [button setImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
    [button addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
    UITapGestureRecognizer *viewTapped=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewTapped:)];
    [self.view addGestureRecognizer:viewTapped];
    viewTapped.delegate = self;
    [viewTapped setCancelsTouchesInView:NO];
    
    self.revealViewController.delegate = self;
    [revealController.frontViewController.revealViewController tapGestureRecognizer];
    
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
       shouldReceiveTouch:(UITouch *)touch
{
    
    if([touch.view class] == self.tapBar.class){
        return NO;
    }
    return YES;
}
-(void)viewTapped:(UITapGestureRecognizer*)gestureRecognizer
{
    NSLog(@"view tapped");
    //Do what you want here
    SWRevealViewController *revealController = self.revealViewController;
    if (revealController.frontViewPosition == FrontViewPositionRight) {
        [revealController revealToggleAnimated:YES];
    }
    
}

#pragma mark - Tab Bar
-(void)tabBarController:(UITabBarController *)tabBar didSelectViewController:(UIViewController *)viewController
{
    NSLog(@"Selected index: %lu", (unsigned long)tabBar.selectedIndex);
}
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    SWRevealViewController *revealController = [self revealViewController];
    UIViewController *newFrontController = nil;
    if(item.tag == 0){
        EventLists *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"eventListViewController"];
        newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    }else if (item.tag == 1){
        ParkEventList *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"parkEventListViewController"];
        newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    }else if (item.tag == 2){
        SAParknRec *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"saparkViewController"];
        newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    }else if (item.tag == 3){
        UIViewController *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"accountViewController"];
        newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    }
    
    [revealController pushFrontViewController:newFrontController animated:YES];
    // NSLog(@"Selected index: %lu", item.tag);
}
@end
