//
//  FIPInfo.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 5/7/16.
//  Copyright © 2016 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FIPInfo : UIViewController<UIGestureRecognizerDelegate, UITabBarDelegate>

@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *viewTapped;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *webviewTapped;
@property (strong, nonatomic) IBOutlet UITabBarItem *aboutFIP;
@property (strong, nonatomic) IBOutlet UITabBarItem *viewEvents;
@property (strong, nonatomic) IBOutlet UITabBar *tabBar;


- (IBAction)goHome:(id)sender;


@end
