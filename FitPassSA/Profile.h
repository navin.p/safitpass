//
//  Profile.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/27/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImage+ImageEffects.h"
//#import "UserInfo.h"
#import "Event.h"
#import "SWRevealViewController.h"

@interface Profile : UIViewController <UITableViewDelegate, UITableViewDataSource, UITabBarDelegate, UIGestureRecognizerDelegate, SWRevealViewControllerDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *blurImage;
@property (strong, nonatomic) IBOutlet UIImageView *profileImg;
@property (strong, nonatomic) IBOutlet UILabel *userPoint;
//@property (strong, nonatomic) IBOutlet UILabel *weeklyPt;
@property (strong, nonatomic) IBOutlet UITabBar *tabBar;
@property (strong, nonatomic) IBOutlet UITabBarItem *aboutFP;
@property (strong, nonatomic) IBOutlet UITabBarItem *viewProfile;
@property (strong, nonatomic) IBOutlet UITabBarItem *viewEvents;

@property (strong, nonatomic) IBOutlet UILabel *userName;
@property (strong, nonatomic) IBOutlet UILabel *completedEvents;

@property (strong, nonatomic) IBOutlet UILabel *addPhotoLabel;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *viewTapped;
@property (weak, nonatomic) IBOutlet UILabel *weightLost;
@property (weak, nonatomic) IBOutlet UILabel *lostSince;
@property (weak, nonatomic) IBOutlet UILabel *featuredCT;
@property (weak, nonatomic) IBOutlet UILabel *fitnessCT;
@property (weak, nonatomic) IBOutlet UILabel *volunteeringCT;
@property (weak, nonatomic) IBOutlet UILabel *nutritionCT;
@property (weak, nonatomic) IBOutlet UILabel *totalPT;

#pragma event outlets
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *addPictureBtn;


#pragma Variables
@property (nonatomic) UIImage *image;
@property (nonatomic) int imageIndex;
@property (nonatomic, strong) NSMutableArray *userProfile;
@property (nonatomic, strong) NSMutableArray *eventList;


#pragma actions
- (IBAction)goHome:(id)sender;
- (IBAction)addPicture:(id)sender;

@end
