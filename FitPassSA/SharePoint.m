//
//  SharePoint.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/26/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//
#import "Event.h"
#import "AppSetting.h"
#import "SharePoint.h"
#import "Profile.h"
#import "SWRevealViewController.h"
#import "EventList.h"
#import "GAIDictionaryBuilder.h"
#import "GAI.h"
#import "GAIFields.h"
#import "Header.h"

@interface SharePoint (){
    NSUserDefaults *userDefaults;
}
@end

@implementation SharePoint
@synthesize eventDetail;

#pragma mark - Object lifecycle
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self.slideOutAnimationEnabled = YES;
    
    return [super initWithCoder:aDecoder];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    //GA Log
    NSString *str = @"Share Point Activity";
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:str];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    tracker.allowIDFACollection = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    userDefaults = [[NSUserDefaults alloc] init];
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    self.navigationController.navigationBarHidden = YES;
    
    NSString *eID = [userDefaults objectForKey:kEventID];
    //then get the data
    NSString *url = [NSString stringWithFormat: @"%@eventDetail.php?eid=%@",BaseURL, eID];
    NSURL *eventURL = [NSURL URLWithString: url];
    NSLog(@"%@", eventURL);
    NSData *jsonData = [NSData dataWithContentsOfURL: eventURL];
    NSError *error = nil;
    NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error: &error];
    
    NSDictionary *userInfo = [dataDictionary objectForKey:@"fitpass"];
    
    NSLog(@"%@", userInfo);
    self.eventTitle =[userInfo objectForKey:@"title"];
    self.eventPoint = [userInfo objectForKey:@"point"];
    self.photoName = [userInfo objectForKey:@"photo"];
    NSLog(@"%@",self.photoName);
    if ([self.photoName isEqualToString:@""]){
        self.photoHeightLimit.constant = 0;
    }else{
        NSString *imageurl = [NSString stringWithFormat: @"%@events/%@",BaseURLFOR_IMAGE, self.photoName];
        NSURL *thumbnailURL = [NSURL URLWithString: imageurl];
        NSData *imageData = [NSData dataWithContentsOfURL: thumbnailURL];
        UIImage *image = [UIImage imageWithData:imageData];
        self.eventPhoto.image = image;
            
        self.eventPhoto.layer.cornerRadius = self.eventPhoto.frame.size.width / 2;
        self.eventPhoto.clipsToBounds = YES;
        //create round image border
        self.eventPhoto.layer.borderWidth = 3.0f;
        self.eventPhoto.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    
}
- (IBAction)closeWindow:(id)sender {
   
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    SWRevealViewController *revealController = [self revealViewController];
    UIViewController *newFrontController = nil;
    EventList *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"eventListViewController"];
    newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    [revealController pushFrontViewController:newFrontController animated:YES];
    
}

- (IBAction)viewProfile:(id)sender {
    //redirect back to eventList;
    NSLog(@"clicked");
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    SWRevealViewController *revealController = [self revealViewController];
    UIViewController *newFrontController = nil;
    Profile *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    [revealController pushFrontViewController:newFrontController animated:YES];
}

- (IBAction)sharePoint:(id)sender {
    NSLog(@"%@ %@", self.eventPoint, self.eventTitle);
    
    //compose the item to share
    NSString *textToShare = [NSString stringWithFormat: @"I just earned %@ points on FitPass for: %@", self.eventPoint, self.eventTitle];
    NSLog(@"Photo: %@",self.photoName);
    if (![self.photoName isEqualToString:@""]){
        NSString *imageurl = [NSString stringWithFormat: @"%@events/%@",BaseURLFOR_IMAGE, self.photoName];
        NSURL *thumbnailURL = [NSURL URLWithString: imageurl];
        NSData *imageData = [NSData dataWithContentsOfURL: thumbnailURL];
        UIImage *imageToShare = [UIImage imageWithData:imageData];
    
        NSArray *objectsToShare = @[textToShare, imageToShare];
        NSLog(@"%@", objectsToShare);
        UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
        [self presentViewController:activityVC animated:YES completion:nil];
    }else{
        UIImage *imageToShare = [UIImage imageNamed:@"fitpas_info"];
        NSArray *objectsToShare = @[textToShare, imageToShare];
        NSLog(@"%@", objectsToShare);
        UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
        [self presentViewController:activityVC animated:YES completion:nil];
    }
    
    
}

@end
