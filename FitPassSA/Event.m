//
//  Event.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/19/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import "Event.h"
#import "AFNetworking.h"
#import "Header.h"
@implementation Event

- (id) initWithTitle:(NSString *)title{
    self = [super init];
    if (self){
        //[self setTitle:title];
        self.eventTitle = title;
        self.startDate = nil;
        self.startTime = nil;
        self.endTime = nil;
        self.ckTime = nil;
        self.eventDescription = nil;
        self.points = nil;
        self.locationInfo = nil;
        self.loclat = nil;
        self.loclong = nil;
        self.locName = nil;
        self.fitLevel = nil;
        self.badge = nil;
        self.eventPhoto = nil;
        self.categories = nil;
        self.shareBonus = nil;
    }
    return self;
}

+(id) blogPostWithTitle:(NSString *)title{
    return [[self alloc]initWithTitle:title];
}
//implementation to conver to URL
- (NSURL *) thumbnailURL{
    NSString *url = [NSString stringWithFormat: @"%@events/%@",BaseURLFOR_IMAGE, self.eventPhoto];
    return [NSURL URLWithString:url];
}
- (NSDate *) eventDate{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yy"];
    return [dateFormatter dateFromString:self.startDate];
}
- (NSDate *) eventStime{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yy"];
    return [dateFormatter dateFromString:self.startTime];
}
- (NSDate *) eventEtime{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yy"];
    return [dateFormatter dateFromString:self.endTime];
}
- (NSDate *) eventCtime{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yy"];
    return [dateFormatter dateFromString:self.ckTime];
}

- (NSDictionary *)notification:(NSString *)eID userID:(NSString *)uID{
    
    //construct URL
    NSString *url = [NSString stringWithFormat: @"%@eventDetail.php?eid=%@&uid=%@&request=notification",BaseURL, eID, uID];
    NSURL *eventURL = [NSURL URLWithString: url];
    NSData *jsonData = [NSData dataWithContentsOfURL: eventURL];
    NSError *error = nil;
    NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error: &error];

    return dataDictionary;
}

@end
