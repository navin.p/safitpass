//
//  Instruction.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 4/17/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Instruction : UIViewController <UIPageViewControllerDataSource>

@property (strong, nonatomic) UIPageViewController *pageController;

@property (nonatomic, assign) BOOL slideOutAnimationEnabled;

@end
