//
//  Family.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 5/5/17.
//  Copyright © 2017 Ping-jung Tsai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Family : NSObject

@property (nonatomic, strong) NSString *userID;
@property (nonatomic, strong) NSString *userPhoto;
@property (nonatomic, strong) NSString *userPoints;
@property (nonatomic, strong) NSString *userFirstName;
@property (nonatomic, strong) NSString *userLastName;
@property (nonatomic, strong) NSString *userGender;
@property (nonatomic, strong) NSString *userAge;
@property (nonatomic, strong) NSString *userActLvl;
@property (nonatomic, strong) NSMutableArray *userNotification;
@property (nonatomic, strong) NSMutableArray *userEvents;
@property (nonatomic, strong) NSString *eventFound;
@property (nonatomic, strong) NSString *weightLost;
@property (nonatomic, strong) NSString *parent;
@property (nonatomic, strong) NSString *children;
@property (nonatomic, strong) NSString *userFullName;

- (id) initWithID:(NSString *)userID;
+(id) blogPostWithID:(NSString *)userID; //convenient constructor

//implementation to conver to URL
- (NSURL *) thumbnailURL;


@end
