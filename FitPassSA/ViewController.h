//
//  ViewController.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/9/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet FBSDKLoginButton *loginButton;

@property (weak, nonatomic) IBOutlet UIButton *SignUp;
- (IBAction)signUpAction:(id)sender;
- (IBAction)loginAction:(id)sender;

@property (nonatomic, assign) BOOL slideOutAnimationEnabled;
@end

