//
//  Home.m
//  FitPassSA
//
//  Created by Ping-Jung Tsai on 4/21/17.
//  Copyright © 2017 Ping-jung Tsai. All rights reserved.
//

#import "Home.h"
#import "Sponsor.h"
#import "SponsorCell.h"
#import "MBProgressHUD.h"
#import "AppSetting.h"
#import "CoreLocation/CLLocationManagerDelegate.h"
#import "EventLists.h"
#import "ParkEventList.h"
#import "SAParknRec.h"
#import "SWRevealViewController.h"
#import "GAIDictionaryBuilder.h"
#import "GAI.h"
#import "GAIFields.h"
#import "Profile.h"
#import "MyAccount.h"
#import "Header.h"
static NSString * const reuseIdentifier = @"Cell";

@interface Home (){
    NSUserDefaults *userDefaults;
}

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)
#define IS_IOS8_OR_ABOVE                            (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))

@end

@implementation Home
@synthesize sponsorGrid;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidAppear{
    
    //GA Log
    NSString *str = @"Homepage";
   
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:str];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    tracker.allowIDFACollection = YES;
    
    // Do any additional setup after loading the view, typically from a nib.
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    [self createMenu];
    UITapGestureRecognizer *viewTapped=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewTapped:)];
    [self.view addGestureRecognizer:viewTapped];
    
    //define a saved selected IndexPath so we can pass the index between swipe button and direct row press
    // self.savedSelectedIndexPath = nil;
    
    self.collectionLayout = [[UICollectionViewFlowLayout alloc] init];
    [self.collectionLayout setItemSize:CGSizeMake(100, 150)];
    [self.collectionLayout setSectionInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    self.collectionLayout.minimumLineSpacing = 0;
    self.collectionLayout.minimumInteritemSpacing = 0;
    [self.sponsorGrid setCollectionViewLayout:self.collectionLayout];
    //testing to see if the collection view is loading
    self.sponsorGrid.backgroundColor = [UIColor whiteColor];
    
    
}


-(void)viewWillAppear:(BOOL)animated{

    
}
-(void)viewWillLayoutSubviews{
    [self.scroll setContentSize:CGSizeMake(self.view.frame.size.width, 575)];

    
}
- (void)viewDidLoad {
    // Do any additional setup after loading the view.
    
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    userDefaults = [[NSUserDefaults alloc] init];
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString *userID = [userDefaults objectForKey:kUserID];
    //reset current user to the main account everytime you open the app
    [userDefaults setObject:userID forKey:kCurrentUser];
    [userDefaults synchronize];
    
    //construct URL
    NSString *url = [NSString stringWithFormat: @"%@home.php?uid=%@",BaseURL, userID];
    NSURL *homeURL = [NSURL URLWithString: url];
    NSLog(@"%@", homeURL);
    NSData *jsonData = [NSData dataWithContentsOfURL: homeURL];
    NSError *error = nil;
    NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error: &error];
    
    NSArray *eventArray = [dataDictionary objectForKey:@"events"];
    NSArray *profileArray = [dataDictionary objectForKey:@"profile"];
    NSArray *sponsorArray = [dataDictionary objectForKey:@"sponsor"];
    NSString *fitPassOver =[dataDictionary objectForKey:@"fitpassEnded"];
    
    NSLog(@"%@",fitPassOver);
    if ([fitPassOver isEqualToString: @"Yes"]){
        self.firstBadge.hidden = YES;
        self.secondBadge.hidden = YES;
        self.thirdBadge.hidden = YES;
        self.FourthBadge.hidden = YES;
        self.badgeBkgd.hidden = YES;
        self.fitPassTxt.hidden = YES;
        self.totalFPEvents.hidden = YES;
        self.pillarText.hidden = YES;
    }
    
    [self loadEvents:eventArray];
    [self loadProfile:profileArray fitPassOver:fitPassOver ];
    [self loadSponsors:sponsorArray];

}

#pragma mark - load Profile Content

- (void)loadProfile:(NSArray*)profileArray fitPassOver:(NSString*)fitPassOver{
    
    NSString *fbID = [userDefaults objectForKey:kFBID];
    for(NSDictionary *profileDetail in profileArray){
        self.userPTs.text = [profileDetail objectForKey:@"pt"];
        //self.userPTs.text =  @"105";
        if (!(([self.userPTs.text length]) == 0)) {
            int point = [self.userPTs.text intValue];
            if (point <50) {
                _imgMeter.image = [UIImage imageNamed:@"0"];
            }else{
                if (point % 5 == 0 || point % 5 == 1 || point % 5 == 2) {
                    _imgMeter.image = [UIImage imageNamed:[NSString stringWithFormat:@"%d",point - point % 5]];
                }else if (point % 5 == 3 || point % 5 == 4 ){
                    _imgMeter.image = [UIImage imageNamed:[NSString stringWithFormat:@"%d",point  + point % 5]];
                }
            }
            
        }
        
        NSLog(@"%@",profileDetail);
        if ([[profileDetail objectForKey:@"photo"] isEqualToString:@"not found"] || [profileDetail objectForKey:@"photo"] == nil){
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=normal", fbID]];
            NSData  *data = [NSData dataWithContentsOfURL:url];
            self.profileImg.image = [UIImage imageWithData:data];
        }else{
            if ([[profileDetail objectForKey:@"photo"] isEqualToString:@""]){
                self.profileImg.image = [UIImage imageNamed:@"default-photo"];
            }else{
                NSString *userphoto = [NSString stringWithFormat: @"%@users/%@",BaseURLFOR_IMAGE, [profileDetail objectForKey:@"photo"]];
                NSURL *photoURL = [NSURL URLWithString:userphoto];
                NSData *imageData = [NSData dataWithContentsOfURL:photoURL];
                self.image = [UIImage imageWithData:imageData];
                self.profileImg.image = [UIImage imageWithData:imageData];
                // if (self.profileImg.image == nil) {
//                    self.profileImg.image = [UIImage imageNamed:@"default-photo"];
//                }
                
            }
            
            //create round image & blur it
            [self updateImage];
            self.profileImg.layer.cornerRadius = self.profileImg.frame.size.width / 2;
            self.profileImg.clipsToBounds = YES;
            self.profileImg.layer.borderWidth = 3.0f;
            self.profileImg.layer.borderColor = [UIColor whiteColor].CGColor;
        }
        //hide badges
        self.firstBadge.hidden = YES;
        self.secondBadge.hidden = YES;
        self.thirdBadge.hidden = YES;
        self.FourthBadge.hidden = YES;
        
        if ([fitPassOver isEqualToString: @"No"]){
            //pillar count
            NSString *totalComplete = [NSString stringWithFormat:@"%@", [profileDetail objectForKey:@"totalcomplete"]];
            if ([totalComplete isEqualToString:@"0"]){
                self.badgeBkgd.image = [UIImage imageNamed:@"empty-pillar"];
            }else if ([totalComplete isEqualToString:@"1"]){
                self.badgeBkgd.image = [UIImage imageNamed:@"1quarter"];
            }else if ([totalComplete isEqualToString:@"2"]){
                self.badgeBkgd.image = [UIImage imageNamed:@"2quarter"];
            }else if ([totalComplete isEqualToString:@"3"]){
                self.badgeBkgd.image = [UIImage imageNamed:@"3quarter"];
            }else if ([totalComplete isEqualToString:@"4"]){
                self.badgeBkgd.image = [UIImage imageNamed:@"4quarter"];
            }
            
            NSArray *badgeAarray = [profileDetail objectForKey:@"badges"];
            if ([totalComplete isEqualToString:@"0"]){
                NSLog(@"No Pillar");
                //self.badgeBkgd.image = [UIImage imageNamed:@"empty-pillar"];
            }else{
                //load pillar
                //NSLog(@"count: %lu", (unsigned long)badgeAarray.count);
                for (int i = 0; i < badgeAarray.count; i++) {
                    NSString *badge = [badgeAarray objectAtIndex:i];
                    NSLog(@"i: %d", i);
                 //   NSLog(@"badge: %d", badge);
                    if (i==0){
                        self.firstBadge.hidden = NO;
                        if ([badge isEqualToString:@"featured"]){self.firstBadge.image = [UIImage imageNamed:@"badge_featured"]; }
                        if ([badge isEqualToString:@"fitness"]){self.firstBadge.image = [UIImage imageNamed:@"badge_fitness"]; }
                        if ([badge isEqualToString:@"nutrition"]){self.firstBadge.image = [UIImage imageNamed:@"badge_nutrition"]; }
                        if ([badge isEqualToString:@"volunteer"]){self.firstBadge.image = [UIImage imageNamed:@"badge_volunteer"]; }
                    }
                    if (i==1){
                        NSLog(@"abc %@", totalComplete);
                        
                         self.secondBadge.hidden = NO;
                         if ([badge isEqualToString:@"featured"]){self.secondBadge.image = [UIImage imageNamed:@"badge_featured"]; }
                         if ([badge isEqualToString:@"fitness"]){self.secondBadge.image = [UIImage imageNamed:@"badge_fitness"]; }
                         if ([badge isEqualToString:@"nutrition"]){self.secondBadge.image = [UIImage imageNamed:@"badge_nutrition"]; }
                         if ([badge isEqualToString:@"volunteer"]){self.secondBadge.image = [UIImage imageNamed:@"badge_volunteer"]; }
                    }
                     if (i==2){
                         self.thirdBadge.hidden = NO;
                         if ([badge isEqualToString:@"featured"]){self.thirdBadge.image = [UIImage imageNamed:@"badge_featured"]; }
                         if ([badge isEqualToString:@"fitness"]){self.thirdBadge.image = [UIImage imageNamed:@"badge_fitness"]; }
                         if ([badge isEqualToString:@"nutrition"]){self.thirdBadge.image = [UIImage imageNamed:@"badge_nutrition"]; }
                         if ([badge isEqualToString:@"volunteer"]){self.thirdBadge.image = [UIImage imageNamed:@"badge_volunteer"]; }
                    }
                     if (i==3){
                         self.FourthBadge.hidden = NO;
                         if ([badge isEqualToString:@"featured"]){self.FourthBadge.image = [UIImage imageNamed:@"badge_featured"]; }
                         if ([badge isEqualToString:@"fitness"]){self.FourthBadge.image = [UIImage imageNamed:@"badge_fitness"]; }
                         if ([badge isEqualToString:@"nutrition"]){self.FourthBadge.image = [UIImage imageNamed:@"badge_nutrition"]; }
                         if ([badge isEqualToString:@"volunteer"]){self.FourthBadge.image = [UIImage imageNamed:@"badge_volunteer"]; }
                    }
                }//end of looping badge array
            }//end of if badge is not nil
        }//end of if still within fit pass time
    }
}

- (void)updateImage{
    UIImage *effectImage = nil;
    effectImage = [self.image applyDarkEffect];
    self.blurImage.image = effectImage;

}

#pragma mark - loadEvent
- (void)loadEvents:(NSArray*)eventArray{
    
    for(NSDictionary *eventCT in eventArray){
        [self.totalFPEvents setTitle: [eventCT objectForKey:@"FitPass"] forState:UIControlStateNormal];
        [self.totalFIPEvents setTitle: [eventCT objectForKey:@"Park"] forState: UIControlStateNormal];
    }
    self.totalFPEvents.layer.cornerRadius = self.totalFPEvents.bounds.size.width/2;
    self.totalFPEvents.clipsToBounds = YES;
    self.totalFIPEvents.layer.cornerRadius = self.totalFIPEvents.bounds.size.width/2;
    self.totalFIPEvents.clipsToBounds = YES;
}


#pragma mark - loadSponsors
- (void)loadSponsors:(NSArray*)sponsorArray{
    
    self.sponsorList = [NSMutableArray array];
    for(NSDictionary *sponsorDictionary in sponsorArray){
        Sponsor *sponsor = [Sponsor blogPostWithTitle:[sponsorDictionary objectForKey:@"name"]];
        sponsor.sponsorID = [sponsorDictionary objectForKey:@"ID"];
        sponsor.sponsorTitle = [sponsorDictionary objectForKey:@"name"];
        sponsor.sponsorURL = [sponsorDictionary objectForKey:@"url"];
        sponsor.sponsorPhoto = [sponsorDictionary objectForKey:@"image"];
        sponsor.sponsorOrder = [sponsorDictionary objectForKey:@"order"];
        [self.sponsorList addObject:sponsor];
        [sponsorGrid reloadData];
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
 #pragma mark - SponsorList
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.sponsorList count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    SponsorCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    // Configure the cell
    Sponsor *sponsor = [self.sponsorList objectAtIndex:indexPath.item];
    cell.sTitle.text = sponsor.sponsorTitle;
     //get image
    if ([sponsor.sponsorPhoto isEqualToString:@""]){
        cell.sImg.hidden = YES;
    }else{
        NSData *imageData = [NSData dataWithContentsOfURL:sponsor.thumbnailURL];
        cell.sImg.image = [UIImage imageWithData:imageData];
        cell.sImg.hidden = NO;
    }
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)actionOnOK:(id)sender {
    
    if (!(([self.txtNumber.text length]) == 0)) {
        int point = [self.txtNumber.text intValue];
        if (point <50) {
            _imgMeter.image = [UIImage imageNamed:@"0"];
        }else{
            if(point > 300){
                _imgMeter.image = [UIImage imageNamed:[NSString stringWithFormat:@"%d",300]];
            }
           else if (point % 5 == 0 || point % 5 == 1 || point % 5 == 2) {
                _imgMeter.image = [UIImage imageNamed:[NSString stringWithFormat:@"%d",point - point % 5]];
            }else if (point % 5 == 3 || point % 5 == 4 ){
                if (point % 5 == 3){
                    _imgMeter.image = [UIImage imageNamed:[NSString stringWithFormat:@"%d",point  + 2]];
                }else{
                    _imgMeter.image = [UIImage imageNamed:[NSString stringWithFormat:@"%d",point  + 1]];
                }
            }
        }
    }
}

- (IBAction)gotoFPEvent:(id)sender {
    
    [userDefaults setObject:nil forKey:kEventCat];
    [userDefaults setObject:nil forKey:kEventDate];
    [userDefaults synchronize];
    
    SWRevealViewController *revealController = [self revealViewController];
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    UIViewController *newFrontController = nil;
    EventLists *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"eventListViewController"];
    newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    [revealController pushFrontViewController:newFrontController animated:YES];

}

- (IBAction)gotoFIPEvent:(id)sender {
    
    [userDefaults setObject:nil forKey:kEventCat];
    [userDefaults setObject:nil forKey:kEventDate];
    [userDefaults synchronize];
    
    SWRevealViewController *revealController = [self revealViewController];
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    UIViewController *newFrontController = nil;
    EventLists *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"parkEventListViewController"];
    newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    [revealController pushFrontViewController:newFrontController animated:YES];

}
#pragma mark - SlideNavigationController Methods -
- (void)createMenu{
    
    //create menu
    SWRevealViewController *revealController = [self revealViewController];
    //create the left menu button
    UIButton *button  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [button setImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
    [button addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
}
-(void)viewTapped:(UITapGestureRecognizer*)gestureRecognizer
{
    //Do what you want here
    SWRevealViewController *revealController = self.revealViewController;
    if (revealController.frontViewPosition == FrontViewPositionRight) {
        [revealController revealToggleAnimated:YES];
    }
    
}


#pragma mark - Tab Bar
-(void)tabBarController:(UITabBarController *)tabBar didSelectViewController:(UIViewController *)viewController
{
    NSLog(@" index: %lu", (unsigned long)tabBar.selectedIndex);
}
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    SWRevealViewController *revealController = [self revealViewController];
    UIViewController *newFrontController = nil;
    if(item.tag == 0){
        EventLists *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"eventListViewController"];
        newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    }else if (item.tag == 1){
        ParkEventList *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"parkEventListViewController"];
        newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    }else if (item.tag == 2){
        SAParknRec *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"saparkViewController"];
        newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    }else if (item.tag == 3){
        MyAccount *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"accountViewController"];
        newFrontController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    }
    
    [revealController pushFrontViewController:newFrontController animated:YES];
    // NSLog(@"Selected index: %lu", item.tag);
}

@end
