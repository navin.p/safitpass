//
//  Family.m
//  FitPassSA
//
//  Created by Ping-jung Tsai on 5/5/17.
//  Copyright © 2017 Ping-jung Tsai. All rights reserved.
//

#import "Family.h"
#import "Header.h"
@implementation Family

- (id) initWithID:(NSString *)userID{
    self = [super init];
    if (self){
        //[self setTitle:title];
        self.userID = userID;
        self.userPhoto = nil;
        self.userPoints = nil;
        self.userFirstName = nil;
        self.userLastName = nil;
        self.userGender = nil;
        self.userAge = nil;
        self.userActLvl = nil;
        self.userGender = nil;
        self.userNotification = nil;
        self.userEvents = nil;
        self.eventFound = nil;
        self.weightLost = nil;
        self.parent = nil;
        self.children = nil;
        self.userFullName = nil;
    }
    return self;
}
+(id) blogPostWithID:(NSString *)userID{
    return [[self alloc]initWithID:userID];
}
//implementation to conver to URL
- (NSURL *) thumbnailURL{
    NSString *url = [NSString stringWithFormat: @"%@users/%@",BaseURLFOR_IMAGE, self.userPhoto];
    return [NSURL URLWithString:url];
}
@end
