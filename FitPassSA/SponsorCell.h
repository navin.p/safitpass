//
//  SponsorCell.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 4/15/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SponsorCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *sImg;
@property (strong, nonatomic) IBOutlet UILabel *sTitle;

@end
