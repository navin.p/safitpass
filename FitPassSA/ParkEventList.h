//
//  ParkEventList.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 5/14/16.
//  Copyright © 2016 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"
#import <CoreLocation/CoreLocation.h>
#import <CoreLocation/CLLocationManagerDelegate.h>
#import "RMDateSelectionViewController.h"
#import "SWRevealViewController.h"


@interface ParkEventList : UIViewController <CLLocationManagerDelegate, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, UITabBarDelegate, SWRevealViewControllerDelegate>

//@property (strong, nonatomic) IBOutlet UIBarButtonItem *filterBar;
//@property (strong, nonatomic) IBOutlet UIBarButtonItem *menuBar;
@property (strong, nonatomic) IBOutlet UIButton *dateForward;
@property (strong, nonatomic) IBOutlet UIButton *dateSelection;
@property (strong, nonatomic) IBOutlet UIButton *dateBackward;
@property (strong, nonatomic) IBOutlet UIButton *byLocation;
@property (strong, nonatomic) IBOutlet UIButton *byTime;


@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIImageView *noEvent;

@property (strong, nonatomic) IBOutlet UITabBar *tabBar;
@property (strong, nonatomic) IBOutlet UITabBarItem *aboutFIP;
@property (strong, nonatomic) IBOutlet UITabBarItem *viewEvents;

@property (nonatomic, strong) NSMutableArray *eventList;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property(strong, nonatomic) NSIndexPath *savedSelectedIndexPath;
@property (strong, nonatomic) IBOutlet UISwipeGestureRecognizer *swipeLeft;
@property (strong, nonatomic) IBOutlet UISwipeGestureRecognizer *swipeRight;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *viewTap;


- (IBAction)sortByLoc:(id)sender;
- (IBAction)sortByTime:(id)sender;
- (IBAction)viewNextDay:(id)sender;
- (IBAction)viewPrevDay:(id)sender;
- (IBAction)pickDate:(id)sender;

- (NSString *) currentDate;
- (NSString *) previousDate;

- (void) loadEvents:(NSString*)searchType sortEvent:(NSString*)sortType searchDate:(NSString *)dateSelect;
@end
