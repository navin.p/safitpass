//
//  SignUp.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/10/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "UITextField+TextFld.h"
#import "UIButton+BtnStyle.h"
#import "DropDownListView.h"

@interface SignUp : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate, UITextFieldDelegate, kDropDownListViewDelegate, UIGestureRecognizerDelegate, UITextViewDelegate, UIScrollViewDelegate>
{
    DropDownListView *Dropobj;
    NSArray *arryList;
    NSArray *booleanList;
    NSArray *activityList;
}

@property (strong, nonatomic) IBOutlet UITextField *firstName;
@property (strong, nonatomic) IBOutlet UITextField *lastName;
@property (strong, nonatomic) IBOutlet UITextField *emailAddress;
@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UITextField *age;
@property (strong, nonatomic) IBOutlet UITextField *textOK;
@property (strong, nonatomic) IBOutlet UITextField *fitLevel;
@property (strong, nonatomic) IBOutlet UITextField *gender;
@property (strong, nonatomic) IBOutlet UITextField *zipCode;
@property (strong, nonatomic) IBOutlet UITextField *teamName;
@property (strong, nonatomic) IBOutlet UIButton *terms;
@property (weak, nonatomic) IBOutlet UITextField *weight;

@property (strong, nonatomic) IBOutlet UIButton *registerBtn;
@property (strong, nonatomic) IBOutlet UIButton *alreadyHaveAcctBtn;

@property (strong, nonatomic) IBOutlet UILabel *lblSelectedNames;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *viewTapped;


@property (nonatomic, assign) BOOL slideOutAnimationEnabled;
- (IBAction)signUp:(id)sender;
- (IBAction)alreadyHasAcctAction:(id)sender;



@end
