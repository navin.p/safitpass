//
//  NotificationLists.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 3/31/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"
#import "SWRevealViewController.h"

@interface NotificationLists : UIViewController<UITableViewDelegate, UITableViewDataSource, UITabBarDelegate, SWRevealViewControllerDelegate, UIGestureRecognizerDelegate>

@property (strong, nonatomic) IBOutlet UITabBar *tabBar;
@property (strong, nonatomic) IBOutlet UITabBarItem *viewNotificaton;
@property (strong, nonatomic) IBOutlet UITabBarItem *viewProfile;
@property (strong, nonatomic) IBOutlet UITabBarItem *viewEvents;
@property (strong, nonatomic) IBOutlet UILabel *noNotification;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *viewTapped;

#pragma event outlets
@property (strong, nonatomic) IBOutlet UITableView *tableView;


#pragma Variables
@property (nonatomic, strong) NSMutableArray *userProfile;
@property (nonatomic, strong) NSMutableArray *notificationList;
@property(strong, nonatomic) NSIndexPath *savedSelectedIndexPath;



#pragma actions
- (IBAction)goHome:(id)sender;


@end

