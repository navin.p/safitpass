//
//  FamilyCell.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 5/5/17.
//  Copyright © 2017 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"

@interface FamilyCell : SWTableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *memberPhoto;
@property (weak, nonatomic) IBOutlet UILabel *weightPoint;
@property (weak, nonatomic) IBOutlet UILabel *memberName;
@property (weak, nonatomic) IBOutlet UIButton *memberTrack;
@property (weak, nonatomic) IBOutlet UIButton *memberEdit;
@property (weak, nonatomic) IBOutlet UIView *memberView;

@end
