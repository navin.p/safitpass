//
//  SAParknRec.h
//  FitPassSA
//
//  Created by Ping-jung Tsai on 4/6/15.
//  Copyright (c) 2015 Ping-jung Tsai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"

@interface SAParknRec : UIViewController <UITabBarDelegate, UIGestureRecognizerDelegate, SWRevealViewControllerDelegate, UIWebViewDelegate>

@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *viewTapped;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *webviewTapped;

@property (weak, nonatomic) IBOutlet UITabBar *tapBar;
@property (strong, nonatomic) IBOutlet UITabBarItem *aboutSAPR;
@property (strong, nonatomic) IBOutlet UITabBarItem *viewAcct;
@property (strong, nonatomic) IBOutlet UITabBarItem *viewFP;
@property (strong, nonatomic) IBOutlet UITabBarItem *viewFIP;

#pragma mark -  actions

- (IBAction)goHome:(id)sender;

@end
